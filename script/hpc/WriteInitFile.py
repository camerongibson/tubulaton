#!/usr/bin/python
# -*- coding: utf-8 -*-
#NOTE: This script is coded in Python3
#Writes a line to the text file with all the needed parameters for a single simulation
import sys
import linecache



def main():
    """Read correctFile line to file. """
    filename=sys.argv[1]
    WriteNam=sys.argv[2]
    RowNumber=sys.argv[3]
  
    x=linecache.getline(filename,int(RowNumber))

    if x=='':
        raise ValueError('No file read')

    t=x.split(';')
    #print(t)
    t=t[1:-1] #Remove first and last entry which will be RepNum and \n
    f=open(WriteNam,"w+")   
    for j in t:
        j=j.strip()
        f.write(j)
        f.write("\n")
    f.close()
    



if __name__ == '__main__':
    main()
