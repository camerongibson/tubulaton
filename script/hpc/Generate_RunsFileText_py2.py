#!/usr/bin/python
# -*- coding: utf-8 -*-
#NOTE: This script is coded in Python2
#Writes a line to the text file with all the needed parameters for a single simulation
import sys

def WriteFile():
    """ Writes a line to the text file representing a single simulation  """
   
    s="""Kgr_sh_Plus=0.01# /s                              allard et al 2010
    Ksh_gr_Plus=0.04# /s                              allard et al 2010
    V_gr_Plus=0.08# um/s
    V_sh_Plus=0.16# um/s                              Kawamura et al 2008
    V_sh_Moins=0.09# um/s                             Kawamura et al 2008
    K_nobundle_shrink=0.4# um/s
    K_nobundle_cross=0.6#1-m_proprietesD["K_nobundle_shrink
    K_nobundle_catastrophe=0.4#                       Dixit et al 2004
    poids_previous_vect=0.5
    """

    dico={}

    dico["Angle_bundle"]=0.69813170079773179
    dico["Angle_cut"]=0.6;
    dico["Angle_mb_limite"]=0.69813170079773179
    dico["Angle_mb_trajectoire"]=0.69813170079773179
    dico["D_bundle"]=49
    dico["cortical"]=0
    dico["d_mb"]=10
    dico["decision_accrochage"]=0
    dico["decision_cut"]=0
    dico["decision_rencontre"]=0
    dico["details"]=1#parametres destine à gere si on garde le contour ou pas
    dico["epaisseur_corticale"]=1.
    dico["garbage_steps"]=500
    dico["gr_st_moins"]=0
    dico["gr_st_plus"]=1
    dico["nb_max_steps"]=1000   
    dico["nb_microtubules_init"]=10   
    dico["nom_config"]="config.ini"
    dico["nom_folder_input_vtk"]="./"
    dico["nom_folder_output_vtk"]="./"
    dico["nom_input_vtk"]="contourPave.vtk"
    dico["nom_input_vtk_ref"]="contourPave_trou_NO.vtk"
    dico["nom_output_vtk"]="sortie_standart"
    dico["nom_rapport"]=1
    dico["part_alea_alea"]=1
    dico["part_alea_fixe"]=39
    dico["part_influence_influence"]=0
    dico["part_influence_normale"]=1
    dico["proba_detachement_par_step_par_microtubule"]=0.001
    dico["proba_initialisation_par_step"]=0.2
    dico["proba_tocut"]=0.01;
    dico["proba_shrink"]=0;
    dico["proba_crossmicro_shrink"]=0;
    dico["proba_crossmicro_cut"]=0.75;
    dico["cutgroup"]=0;
    dico["save_events"]=100
    dico["stop"]=0
    dico["taille_microtubule"]=8.
    dico["tan_Angle_mb"]=0.83909963117727993
    dico["vtk_steps"]=1000





    if len(sys.argv)==1:
        print("arguments possibles :\n","\n".join([ i.ljust(40)+ " " + str(dico[i]) for i in sorted(dico.keys())]))

    dico2={}
    if len(sys.argv)!=2:
        args=sys.argv[3:]
        for i in args:
            args_s = i.split("=")
            if args_s[0] in list(dico.keys()):
                dico2[args_s[0]]=args_s[1]
            else:
                print("argument", args_s[0]," misunderstood")

    filename=sys.argv[1]
    f=open(filename, "a")

    f.write("RepNum=")
    f.write(sys.argv[2])
    f.write(";")

    if len(sys.argv)>1:
        #print "Your arguments :\n"
        for i in sorted(dico.keys()):
            if i in list(dico2.keys()):
                #print i.ljust(40)+ " " + str(dico2[i]) +  "  (default = "+str(dico[i])+")"
                f.write(str(i))
                f.write("=")
                f.write(str(dico2[i]))
                f.write("#")
                f.write(str(dico[i]))
                f.write(";")
            else:
                print("Value not set by generate file=" + i.ljust(40)+ " " + str(dico[i]))
                f.write(str(i))
                f.write("=")
                f.write(str(dico[i]))
                f.write(";")
        f.write("\n")

def main():
    """Writes line to file. """
    #if len(sys.argv)==3:
    #    print(sys.argv)
      #  if int(sys.argv[2])==1:
      #      filename=sys.argv[1]
      #      f=open(filename, "w") 
      #  else:
      #      filename=sys.argv[1]
      #      close(filename)   
    #else:
    WriteFile()



if __name__ == '__main__':
    main()



