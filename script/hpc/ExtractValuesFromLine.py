#!/usr/bin/python
# -*- coding: utf-8 -*-
#NOTE: This script is coded in Python3
#Writes a line to the text file with all the needed parameters for a single simulation
import sys
import linecache

def extractvalue(Lin):
    if len(Lin)!=1:
        raise ValueError('Should not have multiple reptition data in the file')
    else :    
        Lin=Lin[0]
        if '#' in Lin:
            Lin=Lin.split('#')
            Lin=Lin[0]
        Lin=Lin.split('=')
        Lin=Lin[1]
        Lin=Lin.strip()
    return(Lin)

def main():
    """Read correctFile line to file. """
    filename=sys.argv[1]
    WriteNam=sys.argv[2]
    RowNumber=sys.argv[3]
    Word=sys.argv[4]

    Word=Word+"=" 
    x=linecache.getline(filename,int(RowNumber))

    if x=='':
        raise ValueError('No file read')

    t=x.split(';')
    Temp =[s for s in t if Word in s]
    Temp= extractvalue(Temp)

#    Rep =[s for s in t if "RepNum" in s]
#    Rep= extractvalue(Rep)
#    vtk_steps=[s for s in t if "vtk_steps" in s] 
#    vtk_steps=extractvalue(vtk_steps) 
#    nb_max_steps=nb_max_steps=[s for s in t if "nb_max_steps" in s]
#    nb_max_steps=extractvalue(nb_max_steps)
#    cut=cut[s for s in t if "proba_crossmicro_cut"  in s]   
#    cut=extractvalue(cut)
#    cat=cat[s for s in t if "proba_crossmicro_shrink"  in s]   
#    cat=extractvalue(cat)
#    shrink=[s for s in t if "proba_shrink"  in s]
#    shrink=extractvalue(shrink)
#    nuc_init=[s for s in t if "proba_initialisation_par_step"  in s]
#    nuc_init=extractvalue(nuc_init)
#    Bund=[s for s in t if "Angle_bundle"  in s]
#    Bund=extractvalue(Bund)

    print(Temp)

    return()

if __name__ == '__main__':
    main()
