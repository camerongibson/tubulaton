#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
import matplotlib.pyplot as plt

   
def read_vtk(vtk_path):
    """Read in the data of the vtk mesh"""
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(vtk_path)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()
    data = reader.GetOutput()
    return data


def SphericalStatistic_Forces(data,nam):
    """Calculte spherical statistics on the Point and Force data"""
    LenNum=data.GetNumberOfPoints()
    Forces_All=data.GetPointData().GetArray(nam)
    Forces=[] 
    Points_All=data.GetPointData().GetArray("scalars")
    Points=[]   
    moment=[0,0,0]
    for j in range (0,LenNum):
        Forces.append(Forces_All.GetValue(j))  
        Temp=[Points_All.GetValue(3*j),Points_All.GetValue(3*j+1),Points_All.GetValue(3*j+2)]  
        Points.append(Temp)       
        moment=[moment[0]+Forces_All.GetValue(j)*Temp[0],moment[1]+Forces_All.GetValue(j)*Temp[1],moment[2]+Forces_All.GetValue(j)*Temp[2]]
    Forces=[abs(F) for F in Forces]
    Tot=np.sum(Forces)
    if abs(Tot)<0.00000001:
        ResVect=[]
        ResLength=-10
        Variance=-10
    else :
        ResVect=[moment[0]/Tot,moment[1]/Tot,moment[2]/Tot]
        ResLength=math.sqrt(ResVect[0]**2+ResVect[1]**2+ResVect[2]**2)
        Variance=1-ResLength

    return Tot,moment,ResVect,ResLength,Variance 

def SingleResult():
    vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020031616331584376402/2020031616331584376402_Nucleus_0_repeat_5__Cont_1_10000.vtk"
    RepNum=5
    Tot_All=[]
    Variance_All=[]
    ResVect_x=[]
    ResVect_y=[]
    ResVect_z=[]
    ResLength_All=[]
    for k in range(0,5):
        st=vtk_path.split('_')
        for i,j in enumerate(st):
            if j=="repeat":
                st[i+1]=str(k+1)
        st='_'.join(st)
        data=read_vtk(st)
        Tot,moment,ResVect,ResLength,Variance = SphericalStatistic(data)
        Tot_All.append(Tot)
        Variance_All.append(Variance)
        ResVect_x.append(ResVect[0])
        ResVect_y.append(ResVect[1])
        ResVect_z.append(ResVect[2])
        ResLength_All.append(ResLength)
    print(Tot_All)
    print(ResVect_x)
    print(ResVect_y)
    print(ResVect_z)
    print(ResLength_All)
    print(Variance_All)

def RangePosition():
    """Load given files for the number of repeats given and compare on graphs. Varys position along the cuboidal external mesh. """
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020031719411584474060/2020031719411584474060_" #10 repeats. Vincent cuboid
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032017241584725094/2020032017241584725094_" #30 repeats. Vincent cuboid
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032611531585223613/2020032611531585223613_" #3 repeats. Cylinder. Range position
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032612041585224268/2020032612041585224268_" #3 repeats. Cylinder. Range size
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032613171585228645/2020032613171585228645_" #3 repeats. Cylinder. Range Cylinder Length
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032617371585244277/2020032617371585244277_" #30 repeats Cylinder. Range position
    #RepNum=3
    VTK=10000
    #AllPositions=['100','200','300','400','500','600','700','800','900','1000','1100','1200','1300','1400']
    AllPositions=['400', '600','800','1000','1200','1400','1600','1800','2000','2200','2400','2600','2800','3000','3200','3400']
    
    vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020051513541589547266/2020051513541589547266_"
    RepNum=10
    AllPositions=['1400','2400','3400','4400','5400','6400','7400','8400']

    
    AllTot=[]
    AllxVec=[]
    AllyVec=[]
    AllzVec=[]
    AllResLen=[]
    AllVariance=[]
    for num in AllPositions:
        print(num)
        TempTot=[]
        TempxVec=[]
        TempyVec=[]
        TempzVec=[]
        TempResLen=[]
        TempVariance=[]
        for Rep in range(1,RepNum+1):
            #vtk_fullname=vtk_path+'Nucleus_'+str(num)+'_repeat_'+str(Rep)+'__Cont_2_10000.vtk'
            vtk_fullname=vtk_path+'Sphere_Rad500_500_500_Xpos'+str(num)+'_Mesh2000_repeat_'+str(Rep)+'__Cont_2_'+str(VTK)+'.vtk'
            data=read_vtk(vtk_fullname)
            #nam="Forces"
            #nam="Forces_NN_Direction"
            nam="Forces_NN"
            Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_Forces(data,nam)
            #Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_NNForceDirection(data)
            TempTot.append(Tot)
            if len(ResVect)!=0:
                TempxVec.append(ResVect[0])
                TempyVec.append(ResVect[1])
                TempzVec.append(ResVect[2])
                TempResLen.append(ResLength)
                TempVariance.append(Variance)
        AllTot.append(TempTot)
        AllxVec.append(TempxVec)
        AllyVec.append(TempyVec)
        AllzVec.append(TempzVec)
        AllResLen.append(TempResLen)
        AllVariance.append(TempVariance)
    #AllPos_Labels=[str(int(j)-400) for j in AllPositions]
    AllPos_Labels=[round((float(Tem)-1400)*8/1000,2) for Tem in AllPositions]
    Xlab='Nucleus x-position (micrometers)'
    Plotgraphs(AllTot,AllxVec,AllyVec,AllzVec,AllResLen,AllVariance,AllPos_Labels,Xlab)  
    plt.show()   
 
def RangeSize():
    """Load given files for the number of repeats given and compare on graphs. Varys position along the cuboidal external mesh. """
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020031719411584474060/2020031719411584474060_" #10 repeats. Vincent cuboid
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032017241584725094/2020032017241584725094_" #30 repeats. Vincent cuboid
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032611531585223613/2020032611531585223613_" #3 repeats. Cylinder. Range position
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032612041585224268/2020032612041585224268_" #3 repeats. Cylinder. Range size
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032613171585228645/2020032613171585228645_" #3 repeats. Cylinder. Range Cylinder Length
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032617391585244370/2020032617391585244370_" #3 repeats. Cylinder. Range Cylinder Length
    
    vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020051513471589546850/" #forces on sphere
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020051513481589546888/2020051513481589546888_" #sphere nucleated microtubules
    AllPositions=['100','200','300','400','500']

    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020051513581589547530/2020051513581589547530_" #elipse nucleated microtubules
    #AllPositions=['500','1000','1500','2000','2500']
 
    vtk_path="/home/tamsin/Active/mount/hpc_slcu/tubulaton/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020060415051591279558/2020060415051591279558_"
    AllPositions=['25','50','75','100','125','150','175','200','225','250','275','300','325','350','375','400','425','450','475','500','525','550','575','600','625']
   
    RepNum=30
    VTK=10000
    #AllPositions=['100','200','300','400','500','600','700','800','900','1000','1100','1200','1300','1400']
    #AllPositions=['25','50','75','100','125','150','175','200','225','250','275']
    #Xpos=[400,1150]#[400,1150,1900]
    Xpos=[5425]
    col=['red','blue','green']
    PosBoxplot=[tem for tem in range(1,len(AllPositions)*4+1,4)]
    AllPosLabels=[tem for tem in AllPositions]
    Count=0;
    for NumX,X1 in enumerate(Xpos):
        AllTot=[]
        AllxVec=[]
        AllyVec=[]
        AllzVec=[]
        AllResLen=[]
        AllVariance=[]
        for num in AllPositions:
            print(num)
            TempTot=[]
            TempxVec=[]
            TempyVec=[]
            TempzVec=[]
            TempResLen=[]
            TempVariance=[]
            for Rep in range(1,RepNum+1):
                #vtk_fullname=vtk_path+'Nucleus_'+str(num)+'_repeat_'+str(Rep)+'__Cont_2_10000.vtk'
                #vtk_fullname=vtk_path+'Sphere_Rad'+str(num)+'_Xpos'+str(X1)+'_Mesh1000_repeat_'+str(Rep)+'__Cont_2_'+str(VTK)+'.vtk'
                #if num=='500':
                vtk_fullname=vtk_path+'Sphere_Rad'+str(num)+'_Xpos'+str(X1)+'_repeat_'+str(Rep)+'__Cont_2_'+str(VTK)+'.vtk'
                #else : 
                #    vtk_fullname=vtk_path+'Sphere_Rad'+str(num)+'_500_500_Xpos'+str(X1)+'_Mesh2000_repeat_'+str(Rep)+'__Cont_2_'+str(VTK)+'.vtk'
                vtk_fullname=vtk_path+str(Count)+'__Cont_2_'+str(VTK)+'.vtk'
                Count=Count+1;
                data=read_vtk(vtk_fullname)
                #nam="Forces"
                nam="Forces_NN_Direction"
                #name="Forces_NN"
                Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_Forces(data,nam)
                TempTot.append(Tot)
                if len(ResVect)!=0:
                    TempxVec.append(ResVect[0])
                    TempyVec.append(ResVect[1])
                    TempzVec.append(ResVect[2])
                    TempResLen.append(ResLength)
                    TempVariance.append(Variance)
            AllTot.append(TempTot)
            AllxVec.append(TempxVec)
            AllyVec.append(TempyVec)
            AllzVec.append(TempzVec)
            AllResLen.append(TempResLen)
            AllVariance.append(TempVariance)
        Xlab='Nucleus Radius (micrometers)'
        if X1==400:
            #Plot standard statistics for varying radius at the given X position 

            Plotgraphs(AllTot,AllxVec,AllyVec,AllzVec,AllResLen,AllVariance,AllPositions,Xlab) 
        
        
        print(Count)
        #Boxplot fo x force vector on the nucleus against radius for the three different positions of the nucleus
        plt.figure(6)
        c=col[NumX]
        bp=plt.boxplot(AllxVec, positions=PosBoxplot , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))
        plt.plot([PosBoxplot[0],PosBoxplot[-1]],[0,0],'--k')
        plt.xticks(PosBoxplot, AllPosLabels)
        plt.xlabel(Xlab)
        plt.ylabel('Total x-Force')

        #Boxplot of the total force on the nucleus against radius for the three different positions of the nucleus
        plt.figure(7)
        c=col[NumX]
        bp=plt.boxplot(AllTot, positions=PosBoxplot , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))
        plt.xticks(PosBoxplot, AllPosLabels)
        plt.xlabel(Xlab)
        plt.ylabel('Total Force')

        #Boxplot of variance against radius for the three different positions of the nucleus
        plt.figure(8)
        c=col[NumX]
        bp=plt.boxplot(AllVariance, positions=PosBoxplot , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))
        plt.xticks(PosBoxplot, AllPosLabels)
        plt.xlabel(Xlab)
        plt.ylabel('Variance')
        PosBoxplot=[i+1 for i in PosBoxplot]
         
        plt.figure(9)
        y=[]
        x=[]
        for i,j in enumerate(AllTot):
            y.append(np.mean(j))
            x.append((float(AllPositions[i])*8/1000)**2)
        print(x)
        plt.plot(x,y,'-+')
        plt.ylabel('Total Force')
        plt.xlabel('size**2')
        print((y[-1]-y[0])/(x[-1]-x[0]))
        
    plt.show()

def RangeCylinder():
    """Load given files for the number of repeats given and compare on graphs. Varys position along the cuboidal external mesh. """
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020031719411584474060/2020031719411584474060_" #10 repeats. Vincent cuboid
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032017241584725094/2020032017241584725094_" #30 repeats. Vincent cuboid
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032611531585223613/2020032611531585223613_" #3 repeats. Cylinder. Range position
    #vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032612041585224268/2020032612041585224268_" #3 repeats. Cylinder. Range size
#    vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032613171585228645/2020032613171585228645_" #3 repeats. Cylinder. Range Cylinder Length
#    vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020032617401585244431/2020032617401585244431_" #3 repeats. Cylinder. Range Cylinder Length
    vtk_path="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020051514041589547848/2020051514041589547848_" #3 repeats. Cylinder. Range Cylinder Length
    
    RepNum=10
    VTK=10000
    #AllPositions=['100','200','300','400','500','600','700','800','900','1000','1100','1200','1300','1400']
    #AllPositions=['0', '500','1000','1500','2000','2500','3000','3500','4000']
    AllPositions=['2000','3000','4000','5000','6000','7000','8000']
    AllTot=[]
    AllxVec=[]
    AllyVec=[]
    AllzVec=[]
    AllResLen=[]
    AllVariance=[]
    for num in AllPositions:
        TempTot=[]
        TempxVec=[]
        TempyVec=[]
        TempzVec=[]
        TempResLen=[]
        TempVariance=[]
        for Rep in range(1,RepNum+1):
            #vtk_fullname=vtk_path+'Nucleus_'+str(num)+'_repeat_'+str(Rep)+'__Cont_2_10000.vtk'
            #vtk_fullname=vtk_path+'Cylinder_Rad300_Len'+str(num)+'_Mesh10000_repeat_'+str(Rep)+'__Cont_2_'+str(VTK)+'.vtk'
            vtk_fullname=vtk_path+'Cylinder_Rad625_Len'+str(num)+'_Mesh50_repeat_'+str(Rep)+'__Cont_2_'+str(VTK)+'.vtk'
            
            data=read_vtk(vtk_fullname)
            #nam="Forces"
            nam="Forces_NN_Direction"
            #name="Forces_NN"
            Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_Forces(data,nam)
            TempTot.append(Tot)
            if len(ResVect)!=0:
                print(ResVect)
                print(len(ResVect))
                TempxVec.append(ResVect[0])
                TempyVec.append(ResVect[1])
                TempzVec.append(ResVect[2])
                TempResLen.append(ResLength)
                TempVariance.append(Variance)
        AllTot.append(TempTot)
        AllxVec.append(TempxVec)
        AllyVec.append(TempyVec)
        AllzVec.append(TempzVec)
        AllResLen.append(TempResLen)
        AllVariance.append(TempVariance)
    Xlab='$L_{cyn}$'
    Plotgraphs(AllTot,AllxVec,AllyVec,AllzVec,AllResLen,AllVariance,AllPositions,Xlab)  
    plt.show() 

 
def Plotgraphs(AllTot,AllxVec,AllyVec,AllzVec,AllResLen,AllVariance,AllPositions,Xlab):  
    """Plotting graphs showing the data"""
    plt.figure(1)
    plt.boxplot(AllTot, labels=AllPositions, widths = 0.6)
    #plt.plot(AllPositions,AllTot)
    plt.ylabel('Number of interactions with contour')
    plt.xlabel(Xlab)
    plt.figure(2)
    j=[i for i in range(1,len(AllPositions)*4+1,4)]
    print(j)
    c='red'
    print(AllxVec)
    bp=plt.boxplot(AllxVec, positions=j , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))   
    c='blue'
    j=[i+1 for i in j]
    print(j)
    bp1=plt.boxplot(AllyVec, positions=j , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))   
    c='green'
    j=[i+1 for i in j]
    print(j)
    bp2=plt.boxplot(AllzVec, positions=j , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))   
    plt.legend([bp["boxes"][0], bp1["boxes"][0], bp2["boxes"][0]], ['x', 'y', 'z'], loc='upper right')
    plt.hlines(0,0,j[-1]+1,linestyles='dashed')
    j=[i-3 for i in j]
    plt.xticks(j, AllPositions)
    plt.xlabel(Xlab)

    #plt.plot(AllPositions,AllxVec,label='x direc')
    #plt.plot(AllPositions,AllyVec,label='y direc')
    #plt.plot(AllPositions,AllzVec, label='z direc')
    #plt.legend()
    #plt.title('Force net direction')
    plt.figure(3)
    plt.boxplot(AllResLen, labels=AllPositions, widths = 0.6)
    plt.xlabel(Xlab)
    #plt.plot(AllPositions,AllResLen)
    plt.ylabel('|m|') #modulus of res vector
    plt.figure(4)
    plt.boxplot(AllVariance, labels=AllPositions, widths = 0.6)
    plt.xlabel(Xlab)
    #plt.plot(AllPositions,AllVariance)
    plt.ylabel('Var=1-|m|')


def main():
    #SingleResult()
    #RangePosition()
    RangeSize()
    #RangeCylinder()

if __name__ == '__main__':
    main()