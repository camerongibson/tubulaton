#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
import matplotlib.pyplot as plt
import os, sys
  
def Plotgraphs(AllTot,AllxVec,AllyVec,AllzVec,AllResLen,AllVariance,AllPositions,Xlab):  
    """Plotting graphs showing the data"""
    plt.figure(1)
    plt.boxplot(AllTot, labels=AllPositions, widths = 0.6)
    #plt.plot(AllPositions,AllTot)
    plt.ylabel('Number of interactions with contour')
    plt.xlabel(Xlab)
    plt.figure(2)
    j=[i for i in range(1,len(AllPositions)*4+1,4)]
    c='red'
    bp=plt.boxplot(AllxVec, positions=j , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))   
    c='blue'
    j=[i+1 for i in j]
    bp1=plt.boxplot(AllyVec, positions=j , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))   
    c='green'
    j=[i+1 for i in j]
    bp2=plt.boxplot(AllzVec, positions=j , widths = 0.6, boxprops=dict(color=c), capprops=dict(color=c), whiskerprops=dict(color=c), flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))   
    plt.legend([bp["boxes"][0], bp1["boxes"][0], bp2["boxes"][0]], ['x', 'y', 'z'], loc='upper right')
    plt.hlines(0,0,j[-1]+1,linestyles='dashed')
    j=[i-3 for i in j]
    plt.xticks(j, AllPositions)
    plt.xlabel(Xlab)

    #plt.plot(AllPositions,AllxVec,label='x direc')
    #plt.plot(AllPositions,AllyVec,label='y direc')
    #plt.plot(AllPositions,AllzVec, label='z direc')
    #plt.legend()
    #plt.title('Force net direction')
    plt.figure(3)
    plt.ylim([0,1])
    plt.boxplot(AllResLen, labels=AllPositions, widths = 0.6)
    plt.xlabel(Xlab)
    #plt.plot(AllPositions,AllResLen)
    plt.ylabel('|m|') #modulus of res vector
    plt.figure(4)
    plt.ylim([0,1])
    plt.boxplot(AllVariance, labels=AllPositions, widths = 0.6)
    plt.xlabel(Xlab)
    #plt.plot(AllPositions,AllVariance)
    plt.ylabel('Var=1-|m|') 
    plt.show()
    
def main():
    """
    Reads in inputs provided on command line then calculates the anisotropy. 
    Order of inputs: (1) file directory and name of file (2) simulation number to look at normally nb_max_steps (3) repition number .
    """
    
    #FileLocation="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/2020033118081585674524_Cont_1_Forces.txt" #forces on cylinder
    FileLocation="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/2020033118081585674524_Cont_2_Forces.txt" #forces on sphere
    LenAll=[0,500,1000,1500,2000,2500,3000,3500,4000]
    FileLocation="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/202005151347158954850_Cont_2_Forces.txt" #forces on sphere
    LenAll=[100,200,300,400,500]
    RadAll=300
    MeshAll=10000
    ForcesX=[[] for _ in LenAll]
    ForcesY=[[] for _ in LenAll]
    ForcesZ=[[] for _ in LenAll]
    ForcesVar=[[] for _ in LenAll]
    ForcesTot=[[] for _ in LenAll]
    ForcesResLen=[[] for _ in LenAll]
    f=open(FileLocation,"r")
    lines=f.readlines()
    
    plt.figure(1)
    ax = plt.axes()
    for x in lines:
        x=x.split(';')
        Name=x[3]
        Name=Name.split('_')
        for j in Name:
            if "Rad" in j:
                Rad=int(j[3:])
            if 'Len' in j:
                Len=int(j[3:])
            if 'Mesh' in j:
                Mesh=int(j[4:])
        if (Rad==RadAll and Mesh==MeshAll):
            for i,L in enumerate(LenAll):
                if Len==L: 
                    ForcesX[i].append(float(x[37]))   
                    ForcesY[i].append(float(x[38]))
                    ForcesZ[i].append(float(x[39]))
                    ForcesTot[i].append(int(x[51]))             
                    ForcesVar[i].append(float(x[41]))  
                    ForcesResLen[i].append(float(x[40]))  
    
    Xlab='$L_{cyn}$'
    Plotgraphs(ForcesTot,ForcesX,ForcesY,ForcesZ,ForcesResLen,ForcesVar,LenAll,Xlab)  
    plt.show() 
        
if __name__ == '__main__':
    main()