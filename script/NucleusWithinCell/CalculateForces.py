#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
import matplotlib.pyplot as plt
import os, sys
   
def read_vtk(vtk_path):
    """Read in the data of the vtk mesh"""
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(vtk_path)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()
    data = reader.GetOutput()
    return data

def SphericalStatistic_ForcesLengthPush(data,nam1):
    """Calculte spherical statistics on the Point and Force data, hwen force data in x,y,z direction provided in three different arrays"""
    LenNum=data.GetNumberOfPoints()
    Forces_AllX=data.GetPointData().GetArray(nam1[0])
    Forces_AllY=data.GetPointData().GetArray(nam1[1])
    Forces_AllZ=data.GetPointData().GetArray(nam1[2])
    Forces=[] 
    Points_All=data.GetPointData().GetArray("scalars")
    Points=[]   
    moment=[0,0,0]
    for j in range (0,LenNum):
        Temp1=math.sqrt(Forces_AllX.GetValue(j)**2+Forces_AllY.GetValue(j)**2+Forces_AllZ.GetValue(j)**2)
        if abs(Temp1)>0.0000001:
            print("Total: ",Temp1)
        if abs(Forces_AllX.GetValue(j))>0.0000001:
            print("ForcesX: ",Forces_AllX.GetValue(j))
        
        Forces.append(Temp1)
        Temp=[Points_All.GetValue(3*j),Points_All.GetValue(3*j+1),Points_All.GetValue(3*j+2)]  
        Points.append(Temp)       
        moment=[moment[0]+Forces_AllX.GetValue(j),moment[1]+Forces_AllY.GetValue(j),moment[2]+Forces_AllZ.GetValue(j)]
    print("Moment: ", moment[0])
    Forces=[abs(F) for F in Forces]
    Tot=np.sum(Forces)
    if abs(Tot)<0.00000001:
        ResVect=[]
        ResLength=-10
        Variance=-10
    else :
        ResVect=[moment[0]/Tot,moment[1]/Tot,moment[2]/Tot]
        ResLength=math.sqrt(ResVect[0]**2+ResVect[1]**2+ResVect[2]**2)
        Variance=1-ResLength

    return Tot,moment,ResVect,ResLength,Variance 

def SphericalStatistic_Forces(data,nam):
    """Calculte spherical statistics on the Point and Force data"""
    LenNum=data.GetNumberOfPoints()
    Forces_All=data.GetPointData().GetArray(nam)
    Forces=[] 
    Points_All=data.GetPointData().GetArray("scalars")
    Points=[]   
    moment=[0,0,0]
    for j in range (0,LenNum):
        Forces.append(Forces_All.GetValue(j))  
        Temp=[Points_All.GetValue(3*j),Points_All.GetValue(3*j+1),Points_All.GetValue(3*j+2)]  
        Points.append(Temp)       
        moment=[moment[0]+Forces_All.GetValue(j)*Temp[0],moment[1]+Forces_All.GetValue(j)*Temp[1],moment[2]+Forces_All.GetValue(j)*Temp[2]]
    Forces=[abs(F) for F in Forces]
    Tot=np.sum(Forces)
    if abs(Tot)<0.00000001:
        ResVect=[]
        ResLength=-10
        Variance=-10
    else :
        ResVect=[moment[0]/Tot,moment[1]/Tot,moment[2]/Tot]
        ResLength=math.sqrt(ResVect[0]**2+ResVect[1]**2+ResVect[2]**2)
        Variance=1-ResLength

    return Tot,moment,ResVect,ResLength,Variance 

def WriteData(f2, Data):
    """Print all the force data """
    #print(Data)
    for j in Data: 
        f2.write(str(j))
        f2.write("; ")

def WriteFirstList(f2):
    """Print first line with info of what is in each column. """
    FirstLine=["File name","Vtk num","Repitition"]
    for j in FirstLine:
        f2.write(str(j))
        f2.write("; ")

def WriteMainOld():
    """
    Reads in inputs provided on command line then calculates the forces 
    Order of inputs: (1) file directory and name of file (2) simulation number to look at normally nb_max_steps (3) repition number .
    """
    #print (sys.argv)
    args = sys.argv[1:]
    Vtk_num=int(args[2])
    Repitition=int(args[3])
    NumCont=int(args[4])
    for NumLev in range(1,NumCont+1): 
        FullFileName=args[0]+args[1]+"_Cont_"+str(NumLev)+"_"+str(Vtk_num)+".vtk" 
        print(FullFileName)           
        Temp=FullFileName.split('/')
        TempLon=Temp[-1].split('_')
        for j in TempLon:
            if "Rad" in j:
                Rad=int(j[3:])
            if 'Len' in j:
                Len=int(j[3:])
            if 'Mesh' in j:
                Mesh=int(j[4:])
        #print(Rad)
        #print(Len)
        #print(FullFileName)
        print(os.path.isfile(FullFileName))
        if os.path.isfile(FullFileName):
            data=read_vtk(FullFileName)
            OutputFile=Temp[:-4]
            OutputFile.append('AnisotropyTextFiles')
            #print(Temp)
            OutputFile.append(Temp[-2]+'_Cont_'+str(NumLev)+'_Forces.txt')
            OutputFile=('/').join(OutputFile)
            f2=open(OutputFile,"a+")
            WriteFirstList(f2)   
            WriteData(f2,[Temp[-1]])
            WriteData(f2,[Vtk_num])
            WriteData(f2,[Repitition])
            for nam in ["Forces", "Forces_NN_Direction", "Forces_NN"]:
                Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_Forces(data,nam)
                NextLine=[nam+" tot", nam+" moment_1", nam+" moment_2", nam+" moment_3", nam+" Res vect_1", nam+" Res vect_2", nam+" Res vect_3", nam+" Res length", nam+" variance"]
                WriteData(f2,NextLine)
                WriteData(f2,[Tot])    
                WriteData(f2,moment)
                WriteData(f2,ResVect)
                WriteData(f2,[ResLength])
                WriteData(f2,[Variance])
            f2.write("\n")
            f2.close()
        else : 
            string_data="Cannot open file: "+ str(FullFileName)
            print(string_data)

def WriteFromBash():
    """Designed for writing to a file containing the force data. Designed for calling from a bash script just after tubulaton has been run"""
    #Read in the data from the command line
    args = sys.argv[1:]
    FileLocation=args[0]
    nom_suf_vtk=args[3]
    nom_suf_vtk_2=args[4]
    VtkSteps=int(args[5])
    TotSteps=int(args[6])
    Repitition=int(args[7])
    OutputFile=args[2]
    #print(OutputFile)
        
    #ReadInData
    FullFileName=args[0]+args[1]+"_Cont_2_"+str(TotSteps)+".vtk" 
    print(os.path.isfile(FullFileName))
    if os.path.isfile(FullFileName):
        data=read_vtk(FullFileName)

        #Print data
        f2=open(OutputFile,"a+")
        #WriteFirstList(f2)   
        WriteData(f2,[nom_suf_vtk])
        WriteData(f2,[nom_suf_vtk_2])
        WriteData(f2,[Repitition])
        WriteData(f2,[VtkSteps])
        WriteData(f2,[TotSteps])
        for nam in ["Forces", "Forces_NN_Direction", "Forces_NN"]:
            Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_Forces(data,nam) #Calculate forces
            #NextLine=[nam+" tot", nam+" moment_1", nam+" moment_2", nam+" moment_3", nam+" Res vect_1", nam+" Res vect_2", nam+" Res vect_3", nam+" Res length", nam+" variance"]
            #WriteData(f2,NextLine)
            WriteData(f2,[nam])
            WriteData(f2,[Tot])    
            WriteData(f2,moment)
            WriteData(f2,ResVect)
            WriteData(f2,[ResLength])
            WriteData(f2,[Variance])
        for nam in [["ForcesLengthX","ForcesLengthY","ForcesLengthZ"], ["Forces_PushingX_All","Forces_PushingY_All","Forces_PushingZ_All"]]:
            print(nam)
            Tot,moment,ResVect,ResLength,Variance = SphericalStatistic_ForcesLengthPush(data,nam)
            print("Total: ", Tot)
            WriteData(f2,[nam[0]])
            WriteData(f2,[Tot])    
            WriteData(f2,moment)
            WriteData(f2,ResVect)
            WriteData(f2,[ResLength])
            WriteData(f2,[Variance])
        f2.write("\n")
        f2.close()    
    else : 
        string_data="Cannot open file: "+ str(FullFileName)
        print(string_data)
                
def main():
    #WriteMainOld()
    WriteFromBash()

if __name__ == '__main__':
    main()