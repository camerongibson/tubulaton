This folder contains scripts used for generating data and plotting. Mostly the data scripts are designed to be run directly as commands and will call "programme" when required. For example, on a linux operating system

<tt>cd DurandEtAl </tt><br>
<tt>./ExampleScript</tt><br>
