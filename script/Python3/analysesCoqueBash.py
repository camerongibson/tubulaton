#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# author : Vincent Mirabet
#
#   Cette version considère que la coque de la cellule n'est pas dans le résultat
#   This version considers that the shell of the cell is not in the result
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import scipy.spatial.distance
import pickle
import math
from mayavi import mlab
import matplotlib.pyplot as plt

 
class CoqueErrorException(Exception):
    pass

class NumberOfArgumentsException(Exception):
    pass

class FileAlreadyAnalysed(Exception):
    pass

def subSpatial(d, size):
    """
    The space is broken up into cubes of side length proportional to "size". And each element assigned a subsapce based on their position. Subspaces are classified by three numbers dictaing relative x,y and z position. Subspace is appended to the data.
    Inputs: d the data, size the length size of the cube subspaces
    Outputs: d from input, d2 is d with subspace appended
    """
    red=lambda X:np.round(X/size)
    d3=d[:,0:3].copy()
    spatial=red(d3[:,0:3])
    d2=np.hstack((d,spatial))
    return d2, d

def neighbourZones(d, xxx_todo_changeme):
    '''
    searches for neighbouring cubes
    '''
    (ii,jj,kk) = xxx_todo_changeme
    condGen=np.zeros(len(d)).astype(np.bool)
    for i in [-1,0,1]:
        for j in [-1,0,1]:
            for k in [-1,0,1]:
                condGen+= ( d[:,8]==ii + i) *  (d[:,9]==jj + j) * (d[:,10]==kk + k)
    condRest=( d[:,8]==ii ) *  (d[:,9]==jj) * (d[:,10]==kk)
    return condRest, condGen

def lecture(nom, rep="./"):
    """Read in data from the .vtk file output from the microtubule simulations  
       Inputs: nom is the name of the file. rep is the file location, if none provided assume in current directory
    Output: p the read in data   """
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    print (r.file_name)
    r.update()
    p=r.output
    return p

def lectureCoque(nom):
    r=tvtk.PolyDataReader()
    r.file_name=nom
    r.update()
    p=r.output
    return p

def ajoutCoque(p,d,verbose=False):
    """
    cette fonction n'est utile que dans le cas où le contour de la cellule n'était pas incorporé à la simulation

	this function is only useful if the cell contour was not incorporated into the simulation
    """
    #à priori le type est 2 et l'identité est 1 dans la situation 'à coque'
    if 2 in np.unique(d[:,7]):
        raise CoqueErrorException("il y a une coque dans les données")
    
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    
    identite=np.array([1 for i in p.point_data.get_array(0)])
    if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([2 for i in p.point_data.get_array(0)])
    
    dCoque=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    d2=np.concatenate((dCoque,d))
    return d2

def filtreBundle(d,verbose=True):
    """
    TODO il faudrait enlever le verbose=True quand c'est fini 
    Verbose = True should be removed when it's over

    cette fonction a pour but de générer un d sans bundles
    this function aims to generate a d without bundles
    """
    toto={}
    tata={}
    for r in d:
        toto.setdefault(tuple(np.round(r[3:6],3)),[]).append(r)
        tata.setdefault(tuple(-np.round(r[3:6],3)),[]).append(r)

    #on met dans toto toutes les directions uniques de microtubules. Certaines sont ainsi associées à plusieurs lignes de la matrice. we put in toto all the unique directions of microtubules. Some are thus associated with several lines of the matrix.
    clefs=list(tata.keys())
    l=len(list(toto.keys()))
    for i,k in enumerate(toto.keys()):
        try:
            toto[k].extend(tata[k])
            #print k, i/float(l), len(toto[k])
            print(i/float(l))
        except Exception as e:
            #print "erreur", e
            pass
        l=[]
    for i in toto:
        l.append(np.mean(toto[i],axis=0))

    df=np.array(l)
    return df
    


def extraction(p,verbose=0):
    """
	This function extracts data from p. It creates a numpy array with the following data in each position:
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def calculate_distances(d):
    """
    vérifier les histoires de cond, condr poru distance1/distance2
    
    pour le moment cette fonction ne marche pas...
    dist2[d[:,7]==2]
    """
    directions={}
    distance1=np.ones(d.shape[0])*-1
    distance2=np.ones(d.shape[0])*-1
    nb=0.
    longueur=len(list(set([tuple(i) for i in d[:,8:11]])))
    pour=0
    for ii,jj,kk in list(set([tuple(i) for i in d[:,8:11]])):
        if int(nb/longueur*10)*10==pour:
            pass
        else:
            print(int(nb/longueur*10)*10,"%","(",ii,jj,kk,")")
            pour=int(nb/longueur*10)*10
        nb+=1
        #take the subspace and its neighbours
        condr, cond=neighbourZones(d, (ii,jj,kk))
        dsub=d[cond]
        dsubr=d[condr]
        dsubn=d[cond-condr]
        #########subspace :
        #coordinates of tubulines
        Xr=dsubr[:,0:3][dsubr[:,7]==1]    
        #directions of tubulins of the subpsace
        VXr=dsubr[:,3:6][dsubr[:,7]==1]
        #coordinates of the mb
        Xm=dsub[:,0:3][dsub[:,7]==2]
        #directions of the mb normals
        Xmn=dsub[:,3:6][dsub[:,7]==2]
        ###########subpsace and its neighbours
        #coordinates of tubulines
        X=dsub[:,0:3][dsub[:,7]==1]
        #directions of tubulins
        VX=dsub[:,3:6][dsub[:,7]==1]
        ###########only neighbours : souci si vide
        #coordinates of tubulines
        #Xn=dsubn[:,0:3][dsubn[:,7]==1]
        #directions of tubulins
        #VXn=dsubn[:,3:6][dsubn[:,7]==1]
        #calculations
        axi,bxi=np.ix_(Xr[:,0],Xm[:,0])
        ayi,byi=np.ix_(Xr[:,1],Xm[:,1])
        azi,bzi=np.ix_(Xr[:,2],Xm[:,2])
        Xri,Xi=np.ix_(Xr[:,0],X[:,0])
        Yri,Yi=np.ix_(Xr[:,1],X[:,1])
        Zri,Zi=np.ix_(Xr[:,2],X[:,2])
        Ident=dsub[:,6][dsub[:,7]==1]
        Identr=dsubr[:,6][dsubr[:,7]==1]
        #################################distances between points
        matrice_distance1=scipy.spatial.distance.cdist(Xr,X)
        #matrice points - membrane (sans correction de plan)
        #matrice_distance1=(axi-bxi)*(axi-bxi) + (ayi-byi)*(ayi-byi) + (azi-bzi)*(azi-bzi)
        distances_1_dsubr=np.ones(len(dsubr))*-1
        #print len(Identr), matrice_distance1.shape[0] ok
        for k in range(matrice_distance1.shape[0]):
            ref=Identr[k]
            if (len(matrice_distance1[k])>0) and len(matrice_distance1[k][Ident!=ref])>0:
                #must not be the same tubulin, must not be on the same microtubule
                cand=matrice_distance1[k] == np.min(matrice_distance1[k][Ident!=ref])
                dist=np.abs(float(matrice_distance1[k,cand][0]))
                #print dist, ref, Ident
                distances_1_dsubr[k]=dist
        distance1[condr]=distances_1_dsubr
        #################################distances between points and mb
        #matrice scalaires : point-mb * normale
        matrice_distance2=np.abs((axi-bxi)*Xmn[:,0] + (ayi-byi)*Xmn[:,1] + (azi-bzi)*Xmn[:,2])
        distances_2_dsubr=np.ones(len(dsubr))*-1
        for k in range(matrice_distance2.shape[0]):
            if len(matrice_distance2[k])>0:
                ref=Identr[k]
                #print ref
                cand=matrice_distance2[k] == np.min(matrice_distance2[k])
                dist=np.abs(float(matrice_distance2[k,cand][0]))
                distances_2_dsubr[k]=dist
        distance2[condr]=distances_2_dsubr
        #################################directions (FAUT IL NORMALISER?)
        #directions[(ii,jj,kk)]=np.outer(np.mean(VXr, axis=0), np.mean(VXn, axis=0))
    d1={}
    d2={}
    for k,i in enumerate(d):
        d1.setdefault((i[8],i[9],i[10]),[]).append(distance1[k])
        d2.setdefault((i[8],i[9],i[10]),[]).append(distance2[k])
    for i in d1:
        d1[i]=np.array(d1[i])
    for i in d2:
        d2[i]=np.array(d2[i])
    return d1, d2, distance1, distance2


def save(d, nom):
    np.savetxt("toto.txt.gz",d)


def taille(vec):
    return np.sqrt((np.sum(vec**2)))

def normalise(vec):
    res= vec/taille(vec)
    return res


def vv(toto):
    """  Calculating the matrix from which the anisotropy eigenvalues will be extracted. See "The self-organisation of plant microtubules inside the cell volume yields the cortical localization, stable alignment and sensitivity to external cues where the methods section discusses calculating this anisotropy in "Quantifying anisotropy of microtubule arrays".
   Inputs: 3*N matrix tot giving all tubul ring directions. 
   Outputs: ee the eigenvalues, m eigenvectors of each of the three eignevalues 
   """
    #inputs the 3 times N array of all tubulin directions in the specified box. Then calculates the eigenvalues and eigenvectors of (transpose(toto) dot toto / size) for use in calculating anisotropy.
    corr=1./toto.shape[0]*np.dot(toto.T, toto)
    e=np.linalg.eig(corr)
    m=e[1]
    ee=e[0]
    return ee, [m[0,:], m[1,:], m[2,:]]
    #return ee, [m[:,0], m[:,1], m[:,2]]

#def normalise_rows(Ax):
#    """ Function called by vv_normalised to normalise the matrix rows before calculating the order tensor and eigenvalues.
#        Inputs: Ax is a numpy array of length 3
#        Outputs: Ax the normalised length 3 numpy array
#	 FUNCTION OBSOLETE
#    """
#    norm=math.sqrt(Ax[0]**2+Ax[1]**2+Ax[2]**2)
#    if (math.isinf(norm) or math.isnan(norm) or norm==0):
#        Ax=[np.nan,np.nan,np.nan]
#    else : 
#        Ax=Ax/norm
#        if (Ax[0]**2+Ax[1]**2+Ax[2]**2-1)>0.000000001:
#            print("Error:normalised row but not equal to 1")
#    return Ax

def vv_normalised(toto):
    """  Calculating the matrix from which the anisotropy eigenvalues will be extracted. See "Introduction of liquid crystals" by Denis Andrienko, Journal of Molecular Liquids. First I normalise all the rows to ensure the directions are unit vectors. Then I claculate the order tensor "corr" and from that the eigenvalues. 
   Inputs: 3*N matrix tot giving all tubul ring directions. 
   Outputs: ee the eigenvalues, m eigenvectors of each of the three eignevalues 
   """
    #inputs the 3 times N array of all tubulin directions in the specified box. Then calculates the eigenvalues and eigenvectors of (transpose(toto) dot toto / size) for use in calculating anisotropy.
    #toto=np.apply_along_axis(normalise_rows,1,toto)
    #rownum_temp=toto.shape[0]
    vec=np.linalg.norm(toto,None,1)
    vec=np.array([vec,]*3).transpose()
    toto=toto[~(np.isnan(vec).any(axis=1))]
    vec=vec[~(np.isnan(vec).any(axis=1))]
    toto=toto[~(np.isinf(vec).any(axis=1))]
    vec=vec[~(np.isinf(vec).any(axis=1))]
    toto=toto[~(abs(vec)<0.000000001).any(axis=1)]
    vec=vec[~(abs(vec)<0.000000001).any(axis=1)]
    #print (vec)
    toto=toto/vec
    #toto=toto[~np.isnan(toto).any(axis=1)]
    #rownum_temp1=toto.shape[0]
    #if (rownum_temp-rownum_temp1)!=0:
    #    print("Lost rows =", rownum_temp-rownum_temp1,"   Size of toto=", toto.shape[0])
    #    exit()
    #print(toto.shape[0])
    corr=np.dot(toto.T, toto)/toto.shape[0]-np.identity(3)/3
    if np.trace(corr)>0.000000001 :
        print("Error:Trace non-zero", corr)
    if toto.shape[0]!=0:
        e=np.linalg.eig(corr)
        m=e[1]
        ee=e[0]
    else :
        ee=[float('nan'),float('nan'),float('nan')];
        m=np.zeros((3,3))
    return ee, [m[0,:], m[1,:], m[2,:]]
    #return ee, [m[:,0], m[:,1], m[:,2]]

def plotMt(d, opacite=0.1):
    x=d[:,0][d[:,7]==1]
    y=d[:,1][d[:,7]==1]
    z=d[:,2][d[:,7]==1]
    vx=d[:,3][d[:,7]==1]
    vy=d[:,4][d[:,7]==1]
    vz=d[:,5][d[:,7]==1]
    mlab.quiver3d(x,y,z,vx,vy,vz, color=(0,1,0), scale_factor=20, opacity=opacite)
    #mlab.points3d(x,y,z, d[:,10][d[:,7]==1], scale_factor=0.1)
    
def plotMoy(points_space,vectors_space):
    x,y,z,vx,vy,vz=[],[],[],[],[],[]
    for i in points_space:
        for k,l in enumerate(vectors_space[i]):
            x.append(points_space[i][0])
            y.append(points_space[i][1])
            z.append(points_space[i][2])
            vx.append(vectors_space[i][:,0][k])
            vy.append(vectors_space[i][:,1][k])
            vz.append(vectors_space[i][:,2][k])
    mlab.quiver3d(x,y,z,vx,vy,vz, color=(1,1,1), scale_factor=20, opacity=0.1)
    #mlab.points3d(x,y,z, d[:,10][d[:,7]==1], scale_factor=0.1)


def plotPd(points_space, vv_space, princ_space, opacite=1):
    x,y,z,vx,vy,vz=[],[],[],[],[],[]
    vx1,vy1,vz1=[],[],[]
    vx2,vy2,vz2=[],[],[]
    scal=[]
    for i in points_space:
        x.append(points_space[i][0])
        y.append(points_space[i][1])
        z.append(points_space[i][2])
        x.append(points_space[i][0])
        y.append(points_space[i][1])
        z.append(points_space[i][2])
        x.append(points_space[i][0])
        y.append(points_space[i][1])
        z.append(points_space[i][2])
        vx.append(vv_space[i][1][0][0])
        vy.append(vv_space[i][1][1][0])
        vz.append(vv_space[i][1][2][0])
        vx.append(vv_space[i][1][0][1])
        vy.append(vv_space[i][1][1][1])
        vz.append(vv_space[i][1][2][1])
        vx.append(vv_space[i][1][0][2])
        vy.append(vv_space[i][1][1][2])
        vz.append(vv_space[i][1][2][2])
        scal.append(vv_space[i][0][0])
        scal.append(vv_space[i][0][1])
        scal.append(vv_space[i][0][2])
    x=np.array(x)
    y=np.array(y)
    z=np.array(z)
    vx=np.array(vx)
    vy=np.array(vy)
    vz=np.array(vz)
    mlab.quiver3d(x,y,z,vx,vy,vz, color=(1,0,0),scalars=scal, scale_mode='scalar', scale_factor=100, opacity=opacite)
    mlab.quiver3d(x,y,z,-vx,-vy,-vz, color=(1,0,0),scalars=scal, scale_mode='scalar', scale_factor=100, opacity=opacite)
        
#def test_calcule_microtubules_anisotropie(ecrire=0):
#    # Generally use write_ani through the main() function to calculate anisotropy. 
#    p=lecture("Test_SeveringOnANisotropy_Cut0_2000.vtk")
#    d=extraction(p)
#    tens,v,p=calcule_microtubule_tenseurs(d, True)
#    tens_c,v_c,p_c=calcule_contour_tenseurs(d,True) 
#    x_max=-1000000;
#    y_max=-1000000;
#    z_max=-1000000;
#    x_min=1000000;
#    y_min=1000000;
#    z_min=1000000;
#
#    if ecrire:
#        if ecrire == 1:
#            print(tens)
#        if ecrire == 2:
#            print(tens,v,p)
#    for k in list(tens.keys())[0:1]:
#        ee,m = tens[k]
#        x,y,z=p[k][:,0],p[k][:,1],p[k][:,2]
#        x2,y2,z2=np.array([np.mean(x) for i in range(3)]),np.array([np.mean(y) for i in range(3)]),np.array([np.mean(z) for i in range(3)])
#        vx,vy,vz=v[k][:,0],v[k][:,1],v[k][:,2]
#        print(k, len(x), len(vx), m)
#        mlab.quiver3d(x,y,z,vx, vy, vz, color=(0,0,1))
#        mlab.quiver3d(x2,y2,z2, m[0], m[1], m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)
#        mlab.quiver3d(x2,y2,z2, -m[0], -m[1], -m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)
    #for k in list(tens_c.keys())[0:1]:
    #    ee,m = tens_c[k]
    #    x,y,z=p_c[k][:,0],p_c[k][:,1],p_c[k][:,2]
    #    x2,y2,z2=np.array([np.mean(x) for i in range(3)]),np.array([np.mean(y) for i in range(3)]),np.array([np.mean(z) for i in range(3)])
    #    vx,vy,vz=v_c[k][:,0],v_c[k][:,1],v_c[k][:,2]
    #    print(k, len(x), len(vx), m)
    #    mlab.quiver3d(x,y,z,vx, vy, vz, color=(0,0,1))
    #    mlab.quiver3d(x2,y2,z2, m[0], m[1], m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)
    #    mlab.quiver3d(x2,y2,z2, -m[0], -m[1], -m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)



def plot_tens(tens,p,k):
    xa,ya,za=p[k][:,0],p[k][:,1],p[k][:,2]
    x2,y2,z2=np.mean(p[k][:,0]),np.mean(p[k][:,1]),np.mean(p[k][:,2])
    ee,m = tens[k]
    x,y,z=[0,0,0],[0,0,0],[0,0,0]
    x[0],y[0],z[0]=m[0][0], m[1][0], m[2][0]
    x[1],y[1],z[1]=m[0][1], m[1][1], m[2][1]
    x[2],y[2],z[2]=m[0][2], m[1][2], m[2][2]
    print(x,y,z,x2,y2,z2,ee)
    mlab.points3d(xa,ya,za, color=(0,0,1),scale_factor=1)
    k1,k2,k3=np.argsort(ee)
    print(k1,k2,k3)
    mlab.quiver3d(x2,y2,z2,x[k1], y[k1], z[k1], color=(1,0,0),scalars=ee[k1], scale_mode='scalar',scale_factor=100, line_width=5)
    mlab.quiver3d(x2,y2,z2,x[k2], y[k2], z[k2], color=(0,1,0),scalars=ee[k2], scale_mode='scalar',scale_factor=100, line_width=5)
    mlab.quiver3d(x2,y2,z2,x[k3], y[k3], z[k3], color=(0,0,1),scalars=ee[k3], scale_mode='scalar',scale_factor=100, line_width=5)

#def test_quantification_calcule_microtubules_anisotropie(ecrire=0):
#    #Seems to test for just 1 microtubule
#    p=lecture("Test_SeveringOnANisotropy_Cut0_2000.vtk")
#    d=extraction(p)
#    tens,v,p=calcule_microtubule_tenseurs(d, True)
#    tens_c,v_c,p_c=calcule_contour_tenseurs(d,True)
#    if ecrire:
#        if ecrire == 1:
#            print(tens)
#        if ecrire == 2:
#            print(tens_c,v_c,p_c)
#    #plot_tens(tens_c,p_c,1)
#    for k in list(tens.keys())[0:1]:
#        plot_tens(tens,p,k)

#def test():
#    t=1000.
#    toto=np.random.random((t,3))-1/2.
#    toto=toto*np.array([1,5,1])
#    toto=np.array([normalise(vecteur) for vecteur in toto])
#    x,y,z=np.random.random(t)*0,np.random.random(t)*0,np.random.random(t)*0
#    ee,m=vv(toto)
#    x2,y2,z2=np.array([np.mean(x) for i in range(3)]),np.array([np.mean(y) for i in range(3)]),np.array([np.mean(z) for i in range(3)])
#    mlab.quiver3d(x2,y2,z2,m[0], m[1], m[2], color=(0,0,1),scalars=ee, scale_mode='scalar', line_width=5)
#    mlab.quiver3d(x2,y2,z2,-m[0], -m[1], -m[2], color=(0,0,1),scalars=ee, scale_mode='scalar', line_width=5)
#    mlab.quiver3d(x,y,z,toto[:,0], toto[:,1], toto[:,2], opacity=0.1)

def calcule_microtubule_tenseurs(d, tout=False):
    vects={}
    pos={}
    tens={}
    for m in d:
        if m[7]!=2:
            vects.setdefault(m[6],[]).append(m[3:6])
            pos.setdefault(m[6],[]).append(m[0:3])
    for k in list(vects.keys()):
        vects[k]=np.array(vects[k])
        pos[k]=np.array(pos[k])
        tens[k]=vv(vects[k])
    if tout:
        return tens, vects, pos
    else:
        return tens


def calcule_contour_tenseurs(d, tout=False):
    vects={}
    pos={}
    tens={}
    for m in d:
        if m[7]==2:
            vects.setdefault(m[6],[]).append(m[3:6])
            pos.setdefault(m[6],[]).append(m[0:3])
    for k in list(vects.keys()):
        vects[k]=np.array(vects[k])
        pos[k]=np.array(pos[k])
        tens[k]=vv(vects[k])
    if tout:
        return tens, vects, pos
    else:
        return tens

def calcule_discretisation(d):
    """
    d est le gros tableau général - This is the big picture
    Function takes the direction of tubulin in each space and inputs it into function vv which calculates the eigenvalues and eigenvectors (principle directions) in that space.
    Inputs: d data extracted from .vtk file (using lecture and extraction) to which the space numbers have been added using function "subSpatial".  
    Outputs: points_space is data in the array averages vertically, vectors_space array of tubulin direction data, vv_space array containing eigenvalues followed by the three associated eigenvectors, princ_space array of indicies indicating largest to smallest eignevalue.
    """
    result=d.copy()
    spaces=sorted(list(set([tuple(i) for i in result[:,8:11]])))
    #print(spaces)
    vectors_space={}
    points_space={}
    princ_space={}
    count=0
    for i in spaces:
        res=result[np.where((result[:,8]==i[0]) * (result[:,9]==i[1]) * (result[:,10]==i[2]) * (result[:,7]==1))]
        for j in res:
            #print j[0:3], j[3:6], taille(j[3:6])
            vectors_space.setdefault(i,[]).append(j[3:6])
            points_space.setdefault(i,[]).append(j[0:3])
    for i in vectors_space:
        vectors_space[i]=np.array(vectors_space[i])
        points_space[i]=np.mean(np.array(points_space[i]), axis=0)
    vv_space={}
    vv_space_norm={}
    Temp=[]
    for i in spaces:
        res=result[np.where((result[:,8]==i[0]) * (result[:,9]==i[1]) * (result[:,10]==i[2]) * (result[:,7]==1))]
        #for j in res:
        vv_space[i]=vv(vectors_space[i])
        princ_space[i]=np.argsort(vv_space[i][0])
        Temp=vv_normalised(vectors_space[i])
        #print(Temp,Temp[0][0])
        if ~np.isnan(Temp[0][0]):
            vv_space_norm[i]=Temp
    return points_space,vectors_space, vv_space, princ_space, vv_space_norm
    #Output consists of points_space - mean position in each cell . vectors_space - directions of all tubulin in every cell. vv_space - eigenvectors and eigenvalues in each cell. princ_space - array of indivicies detailing the order of eigenvalue size. 

def calcul_anisotropie(vv_space, princ_space):
    """An old version calculating anisotropy (ani) as the ratio of two eigenvalues. This version of anisotropy was not used in the end
       Inputs: vv_space contains details of the eignevalues and eigenvectors in each space. princ_space contains a list of indicies indicating the smallest to largest eigenavlues in each space
       Ouputs: ani old anisotropy measure of ratio of eigenvalues in each space, trois array of the eigenvalues.  
    """
    ani={}
    trois={}
    for i in sorted(vv_space.keys()):
        vals=vv_space[i][0][princ_space[i][::-1]]
        #ani[i]=vals[1]/vals[0]
        ani[i]=vals[1]
        trois[i]=vals
    return ani, trois
    #vals are the eigenvalues starting from the largest. trois are the eigenvalues. (ani?)


def donnees_distance(d1,d2):
    """sub contains:
	- average, std of non-1 values
	- the number of vlues, the number of -1
	- for both distances

	sub contient :
        - moyenne, std des valeurs non -1
        - le nombre de valeurs, le nombre de -1
        - pour les deux distances

    """
    
    sub={}
    for i in d1:
        nb1=len(d1[i])
        nb_out1=len(d1[i][d1[i]==-1])
        if nb1==nb_out1:
            moy1=-3
            std1=-3
        else:
            moy1=np.mean(d1[i][d1[i]!=-1])
            std1=np.std(d1[i][d1[i]!=-1])
        nb2=len(d2[i])
        nb_out2=len(d2[i][d2[i]==-1])
        if nb2==nb_out2:
            moy2=-3
            std2=-3
        else:
            moy2=np.mean(d2[i][d2[i]!=-1])
            std2=np.std(d2[i][d2[i]!=-1])
        toto=[moy1, std1, nb1, nb_out1, moy2, std2, nb2, nb_out2]
        sub[i]=toto
    return sub


def donnees_anisotropie(dori):
    """
            sub contient : - les trois valeurs d'anisotropie
       Function calculating anisotropy by first calling calcule_discretisation to calculate the eigenvalues (and eigenvectors) and then calling calcul_anisotropie (which is an old function which calculates ani which is now unused). 
       Inputs: dori data extracted from the old vtk file and with the subspace number of each tubulin appended to the end.
       Ouputs: sub list of eigenvalues, sub2 list of eigenvalues from the normalised system with matrix of trace 0 (eigenvalues will be used differently to calculate anisotropy)
    """
    points_space,vectors_space, vv_space, princ_space, vv_space_normalised = calcule_discretisation(dori)
    #print("Done calcule_discretisation")
    ani, vals=calcul_anisotropie(vv_space, princ_space)
    ani2, vals2=calcul_anisotropie(vv_space_normalised, princ_space)
    #print(ani)
    sub={}
    sub2={}
    #print ("This  is ani")
    #print (ani)
    #print ("This is vals")
    #print (vals)
    for i in ani:
        sub[i]=vals[i]
    for i in ani2:
        sub2[i]=vals2[i]
    return sub, sub2

def donnees_generales(d, dist1, dist2):
    """
    donne :
    - nb de tubulines
    - nb de microtubules
    - longueur moyenne des microtubules
    - écart type de cette longueur
    - longueurs
    - distance pt-mb moyenne
    - distance pt-mb std
    - distance pt-pt moyenne
    - distance pt-pt std
    - nb de distances < seuil
    """
    pass
    nb_tubulines=len(d[d[:,7]==1])
    nb_microtubules=len(np.unique(d[:,6]))-1
    longueur=[]
    for i in np.unique(d[:,6]):
        if i != 1:
            longueur.append(len(d[d[:,6]==i]))
    longueur=np.array(longueur)
    lg_moy=np.mean(longueur)
    lg_std=np.std(longueur)
    d_ptpt_moy=np.mean(dist1[dist1>=0])
    d_ptpt_std=np.std(dist1[dist1>=0])
    d_ptmb_moy=np.mean(dist2[dist2>=0])
    d_ptmb_std=np.std(dist2[dist2>=0])
    nb_bundle=len(dist1[(dist1>=0)*(dist1<=6.125)])
    return nb_tubulines, nb_microtubules, longueur, lg_moy, lg_std, d_ptpt_moy, d_ptpt_std, d_ptmb_moy, d_ptmb_std, nb_bundle


def write_gen(rep,nom3,d, dist1, dist2,pre):
    nb_tubulines, nb_microtubules, longueur, lg_moy, lg_std, d_ptpt_moy, d_ptpt_std, d_ptmb_moy, d_ptmb_std, nb_bundle=donnees_generales(d, dist1, dist2)
    f3=open(rep+nom3, "a")
    ligne=[nb_tubulines, nb_microtubules,longueur, lg_moy, lg_std, d_ptpt_moy, d_ptpt_std, d_ptmb_moy, d_ptmb_std, nb_bundle]
    f3.write(";".join([str(j) for j in pre]))
    f3.write(";")
    f3.write(";".join([str(val) for val in ligne]))
    f3.write("\n")
    f3.close()

def write_ani(d,rep,nom2,pre):
    """ Outputs information on the anisotropy in each different spatialisation blocs then outputs the information to a text file. 
    Note: This only calculates the anisotropy eigenvalues (and eigenvectors) but this data is then normally read into R where the standard nematic order parameter is calculated
        Inputs: d is the extracted data (normally filtered for bundles), rep the folder to output hte text file to, nom2 is the name of the text file to output the data to, pre is the name of the file we extracted data from broken into seperate entries at each underscore. 
    """
    #SPATIALISATION
    # donnees issues de la spatialisation en blocs à 50
    d, dsave=subSpatial(d,50)
    ani50, ani50_normalised=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 100
    d, dsave=subSpatial(dsave,100)
    ani100, ani100_normalised=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 200
    d, dsave=subSpatial(dsave,200)
    ani200, ani200_normalised=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 300
    d, dsave=subSpatial(dsave,300)
    ani300, ani300_normalised=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 400
    d, dsave=subSpatial(dsave,400)
    ani400, ani400_normalised=donnees_anisotropie(d)
    #print result
    f2=open(rep+nom2, "a")
    for i in ani50:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";50;")
        f2.write(";".join([str(np.real(j)) for j in ani50[i]]))
        f2.write("\n")
    for i in ani100:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";100;")
        f2.write(";".join([str(np.real(j)) for j in ani100[i]]))
        f2.write("\n")
    for i in ani200:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";200;")
        f2.write(";".join([str(np.real(j)) for j in ani200[i]]))
        f2.write("\n")
    for i in ani300:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";300;")
        f2.write(";".join([str(np.real(j)) for j in ani300[i]]))
        f2.write("\n")
    for i in ani400:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";400;")
        f2.write(";".join([str(np.real(j)) for j in ani400[i]]))
        f2.write("\n")
    f2.close()

def write_total_ani(d,rep,nom2,pre):
    """ 
    Calculate total anisotropy across the whole domain (so the entire domain is considered as one space not multiple spaces.  
	Inputs: d is the extracted data (normally filtered for bundles), rep the folder to output the text file to, nom2 is the name of the text file to output the data to, pre is the name of the file we extracted data from broken into seperate entries at each underscore.
    """
    Size1=4.*np.amax(d[:,0:3]) +1000   #4*maximum size of all direction values + large constant - method to choose large enough value Size1 such that all microtubules are in one cell
    d, dsave=subSpatial(d,Size1)
    ani, ani_normalised=donnees_anisotropie(d)
    f2=open(rep+nom2, "a")   
    count=0; 
    for i in ani:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";")
        f2.write("\n")
        f2.write(";".join([str(np.real(j)) for j in ani[i]]))
        f2.write("\n")
        count=count+1
    if count!=1:
       print('error: should have only 1 line in ani in function write_total_ani') 
    Aniso=LocalAniso(ani)
    f2.write("Local anistropy;")
    f2.write(str(np.real(Aniso)))
    f2.write("\n")
    return Aniso

def Multi_Types_ani(d,rep,nom2,pre):
    """ 
    Calculate anisotropy in multiple ways
	Inputs: d is the extracted data (normally filtered for bundles), rep the folder to output the text file to, nom2 is the name of the text file to output the data to, pre is the name of the file we extracted data from broken into seperate entries at each underscore.
        Outputs: Aniso50 is the anisotropy calculated using grids of size 50, Aniso400 is the anisotropy calculated using grids of size 400 and AnisoGlob calculates the anisotropy using the entire shape as one space
    """
    data_all=d
    ##Total Anisotropy
    #Size1=4.*np.amax(d[:,0:3]) +1000   #4*maximum size of all direction values + large constant - method to choose large enough value Size1 such that all microtubules are in one cell
    #print("Start 100 ani")
    Size1=100
    d, dsave=subSpatial(data_all,Size1)
    #print("Done subspatial")
    ani100, ani100_norm=donnees_anisotropie(d)
    #print("Done 100 ani")
    Aniso100=LocalAniso(ani100)
    Aniso100_norm=EigenvalueAniso(ani100_norm)
    #print(Aniso100)
    #print("Start 400 ani")
    Size1=400
    d, dsave=subSpatial(data_all,Size1)
    #print("Done subspatial")
    ani400, ani400_norm=donnees_anisotropie(d)
    Aniso400=LocalAniso(ani400)
    Aniso400_norm=EigenvalueAniso(ani400_norm)
    #print("Done 400 ani")
    #print(Aniso400)
    #print("Start Glob ani")
    Size1=4.*np.amax(d[:,0:3]) +1000   #4*maximum size of all direction values + large constant - method to choose large enough value Size1 such that all microtubules are in one cell
    d, dsave=subSpatial(data_all,Size1)
    #print("Done subspatial")
    aniGlob, aniGlob_norm=donnees_anisotropie(d)
    #print("Done Glob ani")
    AnisoGlob=LocalAniso(aniGlob)
    AnisoGlob_norm=EigenvalueAniso(aniGlob_norm)
    #print(AnisoGlob)
    #f2=open(rep+nom2, "a")   
    #Aniso=LocalAniso(ani)
    #f2.write("Local anistropy size 50;")
    #f2.write(str(np.real(ani50)))
    #f2.write("\n")
    return Aniso100, Aniso400, AnisoGlob, Aniso100_norm, Aniso400_norm, AnisoGlob_norm

def LocalAniso(ani):
    """Caculate local anisotropy from eigenvalue data.
    Input: ani is the list of sets of three eigenvalues
    Output: Aniso is a single value giving the local anisotropy

    Note: this sometimes throws a warning in the square root term saying some of the values are complex.
    """ 
    count=0; 
    TotLam_temp=[]
    Aniso_temp=[]
    for i in ani:  #Extract the three eigenvalues
        lam1=ani[i][0]  
        lam2=ani[i][1]
        lam3=ani[i][2]
        TotLam_temp.append((lam1+lam2+lam3)/3)
        if (lam1**2+lam2**2+lam3**2)>0.000000001:  #Ensure not very small else essentially dividing through by zero. Ignore these terms
            if isinstance((lam1**2+lam2**2+lam3**2), complex) or isinstance(((lam1-TotLam_temp[count])**2+(lam2-TotLam_temp[count])**2+(lam3-TotLam_temp[count])**2), complex) : #Checking whether complex values and if so that they are small, then calculating the eigenvalues
                if (lam1.imag<0.000000001 and lam2.imag<0.000000001 and lam3.imag<0.000000001):
                    lam1=lam1.real
                    lam2=lam2.real
                    lam3=lam3.real
                    Aniso_temp.append(math.sqrt(3/2)*math.sqrt((lam1-TotLam_temp[count])**2+(lam2-TotLam_temp[count])**2+(lam3-TotLam_temp[count])**2)/math.sqrt(lam1**2+lam2**2+lam3**2)) #Local anisotropy
                else :
                    print("Error: eigenvalues are imaginary")
            else :
                #print("lam1=",lam1)
                #print("lam2=",lam2)
                #print("lam3=",lam3)
                Aniso_temp.append(math.sqrt(3/2)*math.sqrt((lam1-TotLam_temp[count])**2+(lam2-TotLam_temp[count])**2+(lam3-TotLam_temp[count])**2)/math.sqrt(lam1**2+lam2**2+lam3**2))
        
        count=count+1
    Aniso=np.mean(Aniso_temp)
    return (Aniso)

def EigenvalueAniso(ani):
    """Caculate local anisotropy as largest eigenvalue. 
    Input: ani is the list of sets of three eigenvalues
    Output: Aniso is a single value giving the local anisotropy
    """
    Aniso_temp=[]
    lam=[0,0,0]
    for i in ani:  #Extract the three eigenvalues
        lam[0]=ani[i][0]  
        lam[1]=ani[i][1]
        lam[2]=ani[i][2]
        abslam=[abs(lam[0]),abs(lam[1]),abs(lam[2])]
        pos=abslam.index(max(abslam))
        Aniso_temp.append(lam[pos])
        #if lam[pos]<0:
        #    print("Note:Negative eigenvalue. E1=",lam[0]," E2=",lam[1]," E3=",lam[2]) #This is just for observation. Measure can go from -1/2 to 1 but don't expect it to be negative for our microtubules. 
    Aniso=np.mean(Aniso_temp)
    return (Aniso)

def AnisoWithFrame():
    """ One type of graph output that can be calculated if called in many_filesAniso. Calculates the global anisotropy for the inputted files at different frame numbers

    """
    name=[]
#-------------------------Inputs--------------------------
    #The next four lines are the input values for this function that should be changed as needed 
    name.append("Test_SeveringOnANisotropy_Cut0_") #Name of files to input (assumed .vtk where numbers occur afterward to distinguish frames
    in_rep="../../Output/" #Folder where the input files can be found
    out_rep="./" #Folder to output the .txt file 
    #num=[1000,2000] #Frame numbers. name+num+".vtk" should be full file names
    num=[100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000]
    name.append("Test_SeveringOnANisotropy_Cut0_1_") #Name of files for second line on the graph

#--------------------Main Code----------------------
    #Run and plot anisotropy for first input file "name"
    for i in name:
        Anisotropy=[]
        for j in num:
            name1=i+str(j)+".vtk"
            p=lecture(name1,in_rep)
            dprovi=extraction(p)
            df=filtreBundle(dprovi)
            Aniso=Multi_Types_ani(df,out_rep,i[:-4]+"-total-ani.txt",i[:-4].split("_"))
            Anisotropy.append(Aniso)
        #print(Anisotropy)
        plt.plot(num,Anisotropy,'+-')
        plt.ylabel('Anisotropy')
        plt.xlabel('Frame Number')

    fname=out_rep+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)

def AnisoWithRepeats():
    """ One type of graph output that can be calculated if called in many_filesAniso. Calculates three types of anisotropy for the inputted files which are repeats.

    """
    name=[]
    endName=[]
#-------------------------Inputs--------------------------
    #The next five lines are the input values for this function that should be changed as needed. Input files are name+num+endName.vtk
    name.append("Cuboid_SeveringOnANisotropy_Cut0__repeat_") #Name of files to input (assumed .vtk where numbers occur afterward to distinguish frames
    endName.append("_2000") #End explanatory to name normally giving the frame number
    in_rep="../../Output/Cuboid/" #Folder where the input files can be found
    out_rep="./" #Folder to output the .txt file 
    num=list(range(1,51)) # Repeat number
    name.append("Cuboid_SeveringOnAnisotropy_Cut0_1__repeat_") #Name of files for second line on the graph
    name.append("Cuboid_SeveringOnAnisotropy_Cut0_5__repeat_") #Name of files for third line on the graph
    name.append("Cuboid_SeveringOnAnisotropy_Cut1__repeat_") #Name of files for fourth line on the graph

#--------------------Main Code----------------------
    #Calculate anisotropy for input file "name" calculating anisotropy over three different spacings of size 50, size 400 and the entire shape
    Nam_Anisotropy100=[]
    Nam_Anisotropy400=[]
    Nam_AnisotropyGlob=[]
    Nam_Anisotropy100_norm=[]
    Nam_Anisotropy400_norm=[]
    Nam_AnisotropyGlob_norm=[]
    for i in name:
        Anisotropy100_Temp=[]
        Anisotropy400_Temp=[]
        AnisotropyGlob_Temp=[]
        Anisotropy100_Temp_norm=[]
        Anisotropy400_Temp_norm=[]
        AnisotropyGlob_Temp_norm=[]
        for j in num:
            print(j)
            name1=i+str(j)+endName[0]+".vtk"
            p=lecture(name1,in_rep)
            dprovi=extraction(p)
            df=filtreBundle(dprovi)
            Ani100, Ani400, AniGlob, Ani100_norm, Ani400_norm, AniGlob_norm =Multi_Types_ani(df,out_rep,i[:-4]+"-total-ani.txt",i[:-4].split("_"))
            Anisotropy100_Temp.append(Ani100)
            Anisotropy400_Temp.append(Ani400)
            AnisotropyGlob_Temp.append(AniGlob)
            Anisotropy100_Temp_norm.append(Ani100_norm)
            Anisotropy400_Temp_norm.append(Ani400_norm)
            AnisotropyGlob_Temp_norm.append(AniGlob_norm)
        Nam_Anisotropy100.append(Anisotropy100_Temp)
        Nam_Anisotropy400.append(Anisotropy400_Temp)
        Nam_AnisotropyGlob.append(AnisotropyGlob_Temp)
        Nam_Anisotropy100_norm.append(Anisotropy100_Temp_norm)
        Nam_Anisotropy400_norm.append(Anisotropy400_Temp_norm)
        Nam_AnisotropyGlob_norm.append(AnisotropyGlob_Temp_norm)
        #print(Anisotropy)
 
    #Plot Anisotropy using 100 spaces
    plt.figure(1)
    plt.boxplot(Nam_Anisotropy100)
    plt.ylabel('Anisotropy 100 Space')
    plt.xlabel('Probability crossing microtubule is cut')
    plt.xticks([1, 2, 3], ['0', '0.1', '0.5'])
    fname=out_rep+"Ani100_"+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)

    #Plot Anisotropy using 400 spaces 
    plt.figure(2)
    plt.boxplot(Nam_Anisotropy400)
    plt.ylabel('Anisotropy 400 Space')
    plt.xlabel('Probability crossing microtubule is cut')
    plt.xticks([1, 2, 3], ['0', '0.1', '0.5'])
    fname=out_rep+"Ani400_"+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)  

    #Plot global anisotropy
    plt.figure(3)
    plt.boxplot(Nam_AnisotropyGlob)
    plt.ylabel('Global Anisotropy')
    plt.xlabel('Probability crossing microtubule is cut')
    plt.xticks([1, 2, 3], ['0', '0.1', '0.5'])
    fname=out_rep+"Global_"+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.close


    #Plot Eigenvalue Anisotropy using 100 spaces
    plt.figure(4)
    plt.boxplot(Nam_Anisotropy100_norm)
    plt.ylabel('Anisotropy with eigenvalue - Spaces 100')
    plt.xlabel('Probability crossing microtubule is cut')
    plt.xticks([1, 2, 3], ['0', '0.1', '0.5'])
    fname=out_rep+"Ani100_norm"+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)

    #Plot Eigenvalue Anisotropy using 400 spaces 
    plt.figure(5)
    plt.boxplot(Nam_Anisotropy400_norm)
    plt.ylabel('Anisotropy with eigenvalue - Spaces 400')
    plt.xlabel('Probability crossing microtubule is cut')
    plt.xticks([1, 2, 3], ['0', '0.1', '0.5'])
    fname=out_rep+"Ani400_norm"+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)  

    #Plot Eigenvalue global anisotropy
    plt.figure(6)
    plt.boxplot(Nam_AnisotropyGlob_norm)
    plt.ylabel('Anisotropy with eigenvalue - Global')
    plt.xlabel('Probability crossing microtubule is cut')
    plt.xticks([1, 2, 3], ['0', '0.1', '0.5'])
    fname=out_rep+"Global_norm"+name[0]+".png"
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.close


def many_filesAniso():
    """ From input files given below calculate the anisotropy for iput files and then plot. Depending on what function you include below depends on what graphs are plotted.
    """
    #AnisoWithFrame() #Calculates global anisotropy for given files at multiple frames 
    AnisoWithRepeats() #Calculates global anisotropy for given files showing box plot averaging over many repeats

def main():
    """
    TODO : changer ici pour rendre compatible avec une version ne lisant qu'un fichier
    TODO : change here to make it compatible with a version that only reads a file

    This function is designed to be run from the command line. Following Python3 and the file name it requires the name of the folder, the name of the vtk file, the name of the contour and a value for verbose e.g 
    python3 analysesCoqueBash.py ~/Documents/microtubule_simulations/script/Python3/ Test_SeveringOnANisotropy_Cut0_1_0_2000.vtk Test_SeveringOnANisotropy_Cut0_1_0_2000.vtk 1

    An alternative is if four inputs are not provided on the command line it will call many_filesAniso which uses input files provided in the script but does allow it to call multiple scripts and calculate the isotropy for each setup.
    """
    args = sys.argv[1:]
    if len(args)!=4:
        many_filesAniso()
#        print("give the folder, the name of the file, the path/name of the contour, verbose")
#        raise NumberOfArgumentsException("Thou shall give 4 parameters!")
    else : 
        rep=args[0]
        nom=args[1]
        verbose=int(args[3])
        if nom[-4:]!=".vtk":
            if verbose:print("wrong file", nom[-4:])
        coque=args[2]
        #ici un flag dans le répertoire. Here a flag in the directory
        p=lecture(nom, rep)
        p2=lectureCoque(coque)
        pre=nom[:-4].split("_")
        #ici on récupère les données en matrices. Here we recover the data in a matrix
        if verbose:print("départ")
        dprovi=extraction(p)
        if verbose:print("extraction")
        d=ajoutCoque(p2,dprovi)
        if verbose:print("ajoutCoque")
        #DONNEES GENERALES. General data.
        df=filtreBundle(dprovi)
        if verbose:print("filtreBundle")
        d, dsave=subSpatial(d,400)
        #if verbose:print("subspatial")
        #d1, d2, dist1, dist2=calculate_distances(d)
        #if verbose:print("calculate_distances")
        #write_dir()
        #try:
        #    write_gen(rep,nom[:-4]+"-gen.txt",d, dist1, dist2,nom[:-4].split("_"))
        #except :
        #    print("error in gen writing")
    
        #try:
        #    write_ani(df,rep,nom[:-4]+"-ani.txt",nom[:-4].split("_"))
        #except :
        #    print("error in ani writing")
        #if verbose:print("write distributed anisotropy")

        try:
            Aniso=write_total_ani(df,rep,nom[:-4]+"-total-ani.txt",nom[:-4].split("_"))
        except :
            print("error in total-ani writing")
        if verbose:print("write total anisotropy")


if __name__ == '__main__':
    main()








