#!/bin/bash
#
# You have to give it the correct folders
# DDIR being the folder with the vtk mesh describing the outline of the cell
# HDIR is where it puts the various results
# WORKDIR is the program directory
#
PROJECTDIR="/home/tamsin/Active/Documents/tubulaton-Adding_severing_Rules/"  # CHANGE THIS!!!
DDIR=${PROJECTDIR}"structures/"
HDIR=${PROJECTDIR}"results/"                                  # MAKE SURE THIS EXISTS!!!
WORKDIR=${PROJECTDIR}"bin/"
WORKDIR1=${PROJECTDIR}"init/PythonGeneratedScripts/"
WORKDIR2=${PROJECTDIR}"script/DurandEtAl/"
WORKDIR3=${PROJECTDIR}"Output/PythonGeneratedOutput/"
WORKDIR4=${WORKDIR2}"Plotting/"

#Make directories if they do not exist
if [ ! -d ${WORKDIR1} ]; then
	mkdir ${WORKDIR1}
fi
if [ ! -d ${PROJECTDIR}"Output" ]; then
	mkdir ${PROJECTDIR}"Output"
fi
if [ ! -d ${PROJECTDIR}"Output/PythonGeneratedOutput" ]; then
	mkdir ${PROJECTDIR}"Output/PythonGeneratedOutput"
fi
if [ ! -d ${PROJECTDIR}"Output/PythonGeneratedOutput/AnisotropyTextFiles" ]; then
	mkdir ${PROJECTDIR}"Output/PythonGeneratedOutput/AnisotropyTextFiles"
fi
if [ ! -d ${PROJECTDIR}"Output/PythonGeneratedOutput/Data" ]; then
	mkdir ${PROJECTDIR}"Output/PythonGeneratedOutput/Data"
fi
if [ ! -d ${PROJECTDIR}"Output/PythonGeneratedOutput/Data/VTK" ]; then
	mkdir ${PROJECTDIR}"Output/PythonGeneratedOutput/Data/VTK"
fi

# Set some simulation parameters
prefix="Aniso" # Name prefix for all files created

#Set the model parameters of the simulation
#vtksource="contourCube_trou_NO" #cell mesh to be used (several are found in $PROJECTDIR/stuctures directory) 

#Some parameters they probably won't be changed much
Angle_mb_limite=0.7
AngleMbTrajectoire=0.7
DBundle=49
cortical=0
d_mb=10
decision_accrochage=0
decision_cut=0
decision_rencontre=-1
details=1
epaisseur_corticale=1.0
gr_st_moins=0
gr_st_plus=1
part_influence_influence=0
part_influence_normale=1
proba_detachement_par_step_par_microtubule=0.001
taille_microtubule=8.0
tan_Angle_mb=0.839099631177

#Input Files
nom_folder_input_vtk="${DDIR}"
nom_input_vtk=contourLong_trou_NO
nom_input_vtk_ref=contourLong_trou_NO

#Output Files
in_nom_config="Rectangle_NoShrink"
nom_folder_output_vtk="${WORKDIR3}Data/VTK/"
in_nom_output_vtk="Rectangle_"
in_nom_rapport="Rectangle__"

#Numerical Parameter
garbage_steps=500
nb_max_steps=3000
save_events=3000
stop=0
vtk_steps=3000 #400

#Biological factors effecting microtubule behaviour 
#Angle_bundle=0.7
Angle_cut=0.6
nb_microtubules_init=30
part_alea_alea=1
part_alea_fixe=39
proba_initialisation_par_step=0.2
proba_tocut=0
proba_shrink=0
#proba_crossmicro_shrink=0
#proba_crossmicro_cut=0.5
cutgroup=0

#Other inputs not part of .ini file
Date=`date +"%Y%m%d%H%M%s"`
All_Ano_Data="${WORKDIR3}AnisotropyTextFiles/CuttingMultiRepetions_$Date"
RepsMax=20
declare -a CutList=("1" "0_1" "0_01" "0_001" "0_0001" "0")
cd ${WORKDIR3}"Data/VTK"
declare -a BundList=("0_8" "0_2"  "0" )
declare -a IndCatast=("0_01" "0_001" "0")
mkdir $Date
nom_folder_output_vtk=${nom_folder_output_vtk}"${Date}""/"
for i in $(seq 0 $((${#IndCatast[*]}-1))); do
	cat=${IndCatast[i]}
	Bund=${BundList[i]}
	for cut in "${CutList[@]}"; do
		proba_crossmicro_shrink=${cat//_/.}
		proba_crossmicro_cut=${cut//_/.}
        	Angle_bundle=${Bund//_/.}
        	nom_config="$Date"_"$prefix"_"$in_nom_config"_"Cut$cut"_"Bund$Bund"_"Cat$cat"
        	nom_rapport="$Date"_"$prefix"_"$in_nom_rapport"_"Cut$cut"_"Bund$Bund"_"Cat$cat"
        	nom_output_vtk="$Date"_"$prefix"_"$in_nom_output_vtk"_"Cut$cut"_"Bund$Bund"_"Cat$cat"
        	cd $WORKDIR1
        	#here you have the generation of the .ini file via a python script
        	echo "-----"
        	echo "Running python script to create .ini file."
        	echo "-----"

        	python2 ${WORKDIR2}Generate_Init_File.py Angle_bundle=$Angle_bundle Angle_cut=$Angle_cut Angle_mb_limite=$Angle_mb_limite Angle_mb_trajectoire=$AngleMbTrajectoire D_bundle=$DBundle cortical=$cortical d_mb=$d_mb decision_accrochage=$decision_accrochage decision_cut=$decision_cut decision_rencontre=$decision_rencontre details=$details epaisseur_corticale=$epaisseur_corticale garbage_steps=$garbage_steps gr_st_moins=$gr_st_moins gr_st_plus=$gr_st_plus nb_max_steps=$nb_max_steps nb_microtubules_init=$nb_microtubules_init nom_config="$nom_config".ini nom_folder_input_vtk="$nom_folder_input_vtk" nom_folder_output_vtk="$nom_folder_output_vtk" nom_input_vtk="$nom_input_vtk".vtk nom_input_vtk_ref="$nom_input_vtk_ref".vtk nom_output_vtk="$nom_output_vtk" nom_rapport="$nom_rapport".txt part_alea_alea=$part_alea_alea part_alea_fixe=$part_alea_fixe part_influence_influence=$part_influence_influence part_influence_normale=$part_influence_normale proba_detachement_par_step_par_microtubule=$proba_detachement_par_step_par_microtubule proba_initialisation_par_step=$proba_initialisation_par_step proba_tocut=$proba_tocut proba_shrink=$proba_shrink proba_crossmicro_shrink=$proba_crossmicro_shrink proba_crossmicro_cut=$proba_crossmicro_cut cutgroup=$cutgroup save_events=$save_events stop=$stop taille_microtubule=$taille_microtubule tan_Angle_mb=$tan_Angle_mb vtk_steps=$vtk_steps  
                 
		for ((repitition=1; repitition<=RepsMax; repitition++)); do   #Second number in the liast {} sets the number of repitions
			echo "-----"
			echo "Running python script to update .ini file."
			echo "-----"
			python ${WORKDIR2}config_generator_initrepetition.py ${WORKDIR1}$nom_config.ini $repitition #Inputting init file full directory and then the repition number.



	        	#here the main program is launched
	        	echo "-----"
	        	echo "Running main program using ${nb_max_steps} time steps, VTK output every ${vtk_steps} and reporting every ${save_events} steps"
	        	echo "-----"
	        	cd ${WORKDIR}
	        	${WORKDIR}programme ${WORKDIR1}"$nom_config".ini


	        	##Calculate anisotropy
	        	echo "-----"
	        	echo "Calculate Anisotropy"
	        	echo "-----"
	        	cd ${WORKDIR2}
	        	python3 ${WORKDIR2}CalculateAnisotropy "$WORKDIR3""Data/VTK/$Date""/" "$nom_output_vtk"_"repeat"_"$repitition"_ $vtk_steps $nb_max_steps $cut $repitition "$All_Ano_Data"".txt" $Bund $cat
		done

	done
done
#python3 ${WORKDIR4}PlotCuttingAverage $All_Ano_Data $RepsMax $vtk_steps $nb_max_steps "${CutList[@]}"
#python3 ${WORKDIR4}PlotCutAgainstAnisotropy "$All_Ano_Data"".txt" $nb_max_steps
#python3 ${WORKDIR4}PlotCutAgainstAnisotropy "$All_Ano_Data""_bundles.txt" $nb_max_steps
#python3 ${WORKDIR4}PlotVTKAgainstAnisotropy "$All_Ano_Data"".txt" 
#python3 ${WORKDIR4}PlotVTKAgainstAnisotropy "$All_Ano_Data""_bundles.txt" 
#python3 ${WORKDIR2}PlotAll "$All_Ano_Data"".txt" $nb_max_steps
#python3 ${WORKDIR2}PlotCutAgainstAnisotropy_CompBund $All_Ano_Data".txt" $nb_max_steps
