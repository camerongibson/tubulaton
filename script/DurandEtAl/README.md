This folder contains scripts written for :

P. Durand-Smet, Tamsin A. Spelman,  View ORCID ProfileE. M. Meyerowitz, H. Jönsson (2019)
Cytoskeletal organization in isolated plant cells under geometry control
<i>bioRxiv</i> doi: https://doi.org/10.1101/784595


Scripts for generating the data used for each of the figures are provided, along with scripts used for calculating the anisotropy and anisotropy direction. Examples of plotting scripts are also included. 
