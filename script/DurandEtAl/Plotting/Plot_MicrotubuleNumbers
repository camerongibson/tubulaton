#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt

def main():
    """
    Reads in data from the .txt file. This contains data output for microtubule simulation on microtubule lengths. It then takes the list of microtubule lengths and plots a histogram of the data. Note this is set up for reading a text file where microtubule lengths are only printed out at the beginning and end. Also, the text file can contain information from three runs at three different cuts, calculated by changing the .init file and running the simulation again from the command line. Often used with Microtubule_LengthCalc.ini and the produced .txt file of Microtubule_LengthCalc_lengths.txt
    """
    FileLocation="/home/tamsin/Documents/microtubule_simulations/Output/"                #File location
    File="Microtubule_LengthCalc_lengths.txt"        #File name of text data

    #Read in data
    f=open(FileLocation+File,"r")
    lines=f.readlines()
    fig, axs = plt.subplots(1, 3)
    n=0
    for j in [1,3,5]: #Rows to extract data from
        #extract data from each of the stated rows and plot histogram of the data
        x=lines[j]
        x=x.split(";")
        x.remove("\n")
        x=[float(y) for y in x] #change strings to floats

        axs[n].hist(x)
        axs[n].set_ylim([0,450])
        axs[n].set_xlim([0,1000])
        #Set subplot title based on what I alread know about the data in the text file. 
        if j==1:
            axs[n].title.set_text("Cut=1")
        elif j==3:
            axs[n].title.set_text("Cut=0.01")
        else:
            axs[n].title.set_text("Cut=0.0001")
        n=n+1
    axs[0].set_ylabel("Number of microtubules")
    axs[1].set_xlabel("Length of microtubules")
    plt.show()

    

if __name__ == '__main__':
    main()
