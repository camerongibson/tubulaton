#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties


def main():
    """Plot for a range of bundles which I enter as a list manually. Similarly for the cut list. Similarly for a range of bundling and cuts which I enter manually. Plots two graphs one ordering the directional anisotropy and plotting the directions in order and a second one scatter plotting all the angles. 
    """
    #Read in variables
    args = sys.argv[1:]
    VTK=int(args[2])

    graph_val=[[0.001,0.7]]# Tuples are (cat_prob,bund angle).
    cut=0.005

#------------------------- Graph for Square ------------------------------
    FileLocation=args[0]
    f=open(FileLocation,"r")
    lines=f.readlines()


    plt.figure(1)
    ax = plt.axes()
    val_aniso_direc=[]
    for x in lines:
        x=x.split(";")
        if int(x[5])==VTK:
            if ((float(x[2])==cut) and ([float(x[8]),float(x[7])] in graph_val)):
                val_aniso_direc.append(float(x[1])*180/math.pi)
    plt.figure(2)
    ax2=plt.axes()
    x_line=[0.25 for i in range (0,len(val_aniso_direc))]
    plt.plot(x_line,val_aniso_direc,'+k')

    plt.figure(1)
    val_aniso_direc.sort()    
    total_num=len(val_aniso_direc)
    k=1/total_num;
    for value in val_aniso_direc:
        if k<2/total_num-0.00001:
            plt.plot(value,k,'+b',label='Square')
        else :
            plt.plot(value,k,'+b')
        k+=1/total_num

#------------------------- Graph for Rectangle ------------------------------
    FileLocation=args[1]
    f=open(FileLocation,"r")
    lines=f.readlines()

    #for cut in cut_list:
    val_aniso_direc=[]
    for x in lines:
        x=x.split(";")
        if int(x[5])==VTK:
            if ((float(x[2])==cut) and ([float(x[8]),float(x[7])] in graph_val)):
                val_aniso_direc.append(float(x[1])*180/math.pi)

    plt.figure(2)
    ax2=plt.axes()
    x_line=[0.75 for i in range (0,len(val_aniso_direc))]
    plt.plot(x_line,val_aniso_direc,'+k')

    plt.figure(1)
    val_aniso_direc.sort()    
    total_num=len(val_aniso_direc)
    k=1/total_num;
    for value in val_aniso_direc:
        if k<2/total_num-0.00001:
            plt.plot(value,k,'+r',label='Rectangle')
        else :
            plt.plot(value,k,'+r')
        k+=1/total_num


#------------------------- Plotting Specifics ----------------------------
    plt.xlim([-90,90])
    ax.set_xticks([-75,-50,-25,0,25,50,75])
    plt.ylim([0,1])
    plt.grid()
    plt.legend()
    ax.legend(loc='upper left')
    plt.xlabel(r"$\Theta_{S_2}$")
    plt.ylabel("CDF")
    
    plt.figure(2)
    plt.xlim([0,1])
#    
# ------------------------- Save Graph ---------------------------------
    plt.figure(1)
    FileLoc1=args[1].split("/")
    FileName1=FileLoc1[-1]
    FileLoc2=args[0].split("/")
    FileName2=FileLoc2[-1]
    Direc=FileLocation.replace(FileName1,'')
    Direc=Direc+"Figures/"
    FileName1=FileName1.replace(".txt",'')
    FileName2=FileName2.replace(".txt",'')
    fname=Direc+"GraphPaper_Direction_VTK"+str(VTK)+"__"+FileName1+"__"+FileName2+"_cut0_005.png"
    print(fname)
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.show()

if __name__ == '__main__':
    main()
