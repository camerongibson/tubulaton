#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove

def replace(file_path, pattern, subst, rep_num):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if 'nom_output_vtk' in line:
                    index=line.find('#')
                    if index==-1:
                        index=line.find('/n')
                    if pattern in line:
                        new_file.write(line.replace(pattern, subst))
		    else :
                        if rep_num!=1:
                           print("Warning in config_generator_initrepition.py: No previous repitions but reptition not equal to 1")
                        new_file.write(line[:index]+subst+line[index:])
                        print(line+subst)
                else :
		    new_file.write(line)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)


def main():
    """
    Called by script_repeats in /script/Python3. Used when doing multiple repitions of the same simulation. This updates the init file so the output directory changes so each result is printed to different file
    Inputs: file name including directory of the init file to update, the number of this reptition to input in output file name.
    """
    init_file=sys.argv[1]
    rep_num=sys.argv[2]
    print(rep_num)
    temp=int(rep_num)-1
    pattern='_repeat_'+str(temp)+'_'
    subst='_repeat_'+str(rep_num)+'_'

    replace(init_file,pattern,subst,rep_num)


if __name__ == '__main__':
    main()









