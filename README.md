<h2>Program</h2>

This software is developed for simulating the dynamics and interaction of individual microtubule elements
in 3D and confined within a closed surface (cell).

<h3>References</h3>

Mirabet V, Krupinski P, Hamant O, Meyerowitz EM, Jönsson H, Boudaoud A (2018) 
The self-organization of plant microtubules inside the cell volume yields 
cortical localization, stable alignment, and sensitivity to external cues.
<i>PLoS Comp Biol</i>.

P. Durand-Smet, Tamsin A. Spelman, E. M. Meyerowitz, H. Jönsson (2019)
Cytoskeletal organization in isolated plant cells under geometry control
<i>bioRxiv</i> doi: https://doi.org/10.1101/784595

<h3>Main Author</h3>

Vincent Mirabet, vincent.mirabet@gmail.com

<h3>Contributors</h3>

Ross Carter, ross.carter@slcu.cam.ac.uk

Tamsin Spelman, tamsin.spelman@slcu.cam.ac.uk

Pauline Durand, pauline.durand@slcu.cam.ac.uk

<h3>Compilation</h3>

You need various libraries, notably the vtk library, vtk6 or higher. Note, if 
you use VTK>7.1 a preprocessor macro <tt>VTK_VERSION_7_1PLUS</tt> will need to be defined in the preprocessor definitions.
Cmake should be able to detect your VTK version and set definitions automatically.<br>
Using make you can try

<tt>make CPPFLAGS=-DVTK_VERSION_7_1PLUS</tt>

<b>Some examples to get the libraries:</b>

<i>Ubuntu linux (have been tested on 16.04):</i> 

<tt>sudo apt-get install vtk6 libvtk6-dev</tt>

On some machines we also had to install the libproj-dev package:

<tt>sudo apt-get install libproj-dev</tt>

<i>Mac OS X (have been tested on 10.10-10.12):</i> 

<tt>sudo port install vtk</tt> (using <a href="https://www.macports.org/">macports</a>).

<b>Compiling using cmake:</b>

In the main <tt>microtubule_simulation</tt> directory, do

<tt>cd build</tt><br>
<tt>cmake ..</tt><br>
<tt>make</tt>

This will build a target <tt>programme</tt> in the <tt>build</tt> directory.<br>
To install it into <tt>bin</tt> use

<tt>make install</tt>

If you get the error:  <tt>fatal error: tbb/atomic.h: No such file or directory</tt>  during compilation
you may be using a version of VTK install using anaconda. If so make sure the tbb dependices of VTK are installed using: <br>
<tt>conda config --add channels conda-forge</tt><br>
<tt>conda install tbb tbb-devel</tt><br>

<b>Compiling using make:</b>

Modify <tt>src/Makefile.example</tt> to ensure it includes the correct directories for libraries. 

In the main <tt>microtubule_simulation</tt> directory, do

<tt>cd src</tt><br>
<tt>make Makefile.example</tt>

<h3>Binary</h3>

The binary is called <tt>bin/programme</tt>. It is meant to be launched with a 
configuration file as the first (and only) argument:

<tt>bin/programme config.ini</tt>

The <tt>.ini</tt> file contains all the parameter values, and some
example files can be found in the <tt>init</tt> directory.

Usually, <tt>programme</tt> is not called directly, but rather via
bash scripts first generating configuration files (via the python
script <tt>bin/config_generator_13.py</tt>) followed by calls to the
main program. Some example scripts can be found in the <tt>script</tt>
folder. These can be used after the
<i>DIR</i> variables have been updated to point to directories on the
local machine. 

The simplest is the <tt>script/script_test</tt>. It first sets values for model
and simulation parameters, and then calls the python configuration
file generator to generate a <tt>.ini</tt> file followed by calling
the main programme binary for running a simulation. 

The main script used is <tt>script/script_standard</tt>. Inside this
bash script you create a loop where
you define the various parameter values you want to test.  In each step of
the loop it creates a <tt>.ini</tt> file which contains all the parameters
used during the simulation, which ensures that you can recall which
they were.

The <tt>bin/config_generator_13.py</tt> python file contains standard values for all
the parameters, which is convenient if you want to look at what can be
modulated.

<h3>Inputs</h3>
The available parameteres are listed below  (defaults indicate suggested values). These parameters are normally set in the .ini configuration file. Not all of these parameters need to be included in each .ini file. In particular most conditions referrring to multiple surfaces are optional (only one surface, the outer cell position is mandatory).  

- <i> num_input_vtk -- Deafult 1. integer indicating number of surfaces to be read in. Only required if more than 1. MUST occur before input details about the higher number contours (2 and upwards) so normally put first in input text file. 
- <i> Angle\_bundle </i> -- threshold angle at which microtubules bundle
- <i> Angle\_cut </i> -- threshold angle of microtubule bend above which microtubules have a probability of being cut provided decision\_cut!=0
- <i> Angle\_mb\_limite </i> -- threshold angle of microtubule bending when interacting with surface
- <i> Angle\_mb\_trajectoire </i> -- default 0.7
- <i> D\_bundle </i> -- distance in nanometers, default 49. D\_bundle/taille\_microtubule threshold is distance threshold for microtubule interaction
- <i> cortical </i> -- default 0 
- <i> cutgroup </i> -- default 0 which checks induced catastrophe and cutting at every tubulin meeting criteria. If non-zero number of tubulin between such events. 
- <i> d\_mb </i> -- default 10
- <i> decision\_accrochage </i> -- default 0. 0 is weak anchoring othwise strong anchoring.
- <i> decision\_cut </i> -- index determining cutting (see Angle\_cut). 0 don't cut, non-zero cut. 
- <i> decision\_rencontre </i> -- default -1. Microtubule behaviour after large angle bend (origionally for induced catastrophies too). 1 grow, -1 shrink.
- <i> details </i> -- default 1
- <i> epaisseur\_corticale </i> -- deafault 1.0
- <i> garbage\_steps </i> -- default 500
- <i> gr\_st\_moins </i> -- deafult 0. Initiation behaviour at microtubule at minus end: -1 shrink, 0 stationary, 1 growth.
- <i> gr\_st\_plus </i> -- deafault 1. Same as gr\_st\_moins for plus end. 
- <i> nb\_max\_steps </i> -- number of time steps of simulation
- <i> nb\_microtubules\_init </i> -- number of microtubules initiated at start of the simulation 
- <i> nb\_microtubules\_init\_n </i> -- number of microtubules initiated at start of the simulation on higher contours. If not provided and more than 1 contour then initiation occurs evenly across all contours with number across all equal to that provided for the first. only needed if num\_input\_vtk>=2. Needed for each n an integer 2,3,4... up to num\_input\_vtk. 
- <i> nom\_config </i> -- name of input .ini file
- <i> nom\_folder\_input_vtk </i> -- location of input .vtk files 
- <i> nom\_folder\_output\_vtk </i> -- location for output files to be stored
- <i> num\_input\_vtk </i> -- number of surfaces being read in. Default 1. In .ini file must occur before nom\_input\_vtk\_n and nom\_input\_vtk\_ref\_n.
- <i> nom\_input\_vtk </i> -- name of input .vtk file defining the domain 
- <i> nom\_input\_vtk\_ref </i> -- name of input reference .ftk file defining the domain. default set same as nom\_input\_vtk
- <i> nom\_input\_vtk\_n </i> -- Further vtk surface restricting the domain. only required if num\_input\_vtk>=2. Required for each n an integer 2,3,4... up to num\_input\_vtk. 
- <i> nom\_input\_vtk\_ref\_n </i> -- Further reference vtk surfaces restricting the domain. only required if num\_input\_vtk>=2. Required for each n an integer 2,3,4... up to num\_input\_vtk. 
- <i> nom\_output\_vtk </i> -- name of output .vtk file
- <i> nom\_rapport </i> -- name of output .txt file containing simulations data
- <i> Nucleation\_Direction </i> -- Default 0. Direction of nucleation on base surface (0). 0 is tangential and 1 is normal to the surface.
- <i> Nucleation\_Direction\_n </i> -- Direction of nucleation on further vtk surfaces. only required if num\_input\_vtk>=2. Required for each n an integer 2,3,4... up to num\_input\_vtk. 
 - <i> Output\_Contour </i> -- deafult 0. If 1 then will output the input surfaces with force data on the points. 
 - <i> Output\_Contour\_n </i>  --  Decision on whether to output contour for surfaces beyond the first. Only required if num\_input\_vtk>=2. Required for each n an integer 2,3,4... up to num\_input\_vtk. 
- <i> part\_alea\_alea </i> -- default 1. proportion of microtubule growth in random direction. See part\_alea\_fixe.
- <i> part\_alea\_fixe </i> -- default 39. proportion of microtubule growth in same direction as previous tubulin. See part\_alea\_alea.
- <i> part\_influence\_influence </i> -- default 0
- <i> part\_influence\_normale </i> -- deafult 1
- <i> proba\_crossmicro\_shrink </i> -- probability per tubulin recording an interaction of induced catastrophe at large angle microtubule interaction
- <i> proba\_crossmicro\_cut </i> -- probability per tubulin recording an interaction that a microtubule crossing another is severed at the crossover each timestep 
- <i> proba\_detachement\_par\_step\_par\_microtubule </i> -- probability microtubule minus end to shrinking per timstep per microtubule
- <i> proba\_initialisation\_par\_step </i> -- microtubule initialisations per timestep. Integer is number initiated and decimal chance of final microtubule. Note multiplied by domain surface area normalised by reference domain surface area. 
- <i> proba\_initialisation\_par\_step\_n </i> -- microtubule initialisations per timestep on all other surfaces. only required if num\_input\_vtk>=2. Required for each n an integer 2,3,4... up to num\_input\_vtk. If only provided on one, not all surfaces, initialisation happens randonly at that rate across all contours; but note in this case nucleation rate is normalised the ratio of surface areas of the first surface only by nom_input_vtk/nom_input_vtk_ref.
- <i> proba\_initialisation\_area </i> -- microtubule initialisations provided as per area^2 per second in micrmoeters^(-2)seconds^(-1).Only one of this or proba\_initialisation\_par\_step should be provided. Note: 1 second is 5-10 timesteps and for translating this inititation per area to a rate per time step we assume 5 steps per second. 
- <i> proba\_initialisation\_area\_n </i> -- microtubule initialisations provided as per area^2 per second. Only one of this or proba\_initialisation\_par\_step\_n should be provided. Different intitiation condition can be provided for each surface. Only required if num\_input\_vtk>=2. Required for each n an integer 2,3,4... up to num\_input\_vtk. If area initiation condition is only provided on one surface when num\_input\_vtk>=2, initialisation happens randonly at that rate across all surfaces; but note that in that specific case the rate of initiation per time step will be calculated based on the area of the first surface only.
- <i> proba\_shrink </i> -- probability microtubule plus end starts to shrink per microtubule per timestep
- <i> proba\_tocut </i> -- probability microtubules on large bend cut list is cut per timestep per position
- <i> save\_events </i> -- time steps gap between adding information to the output .txt file report
- <i> stop </i> -- default 0. if non-zero simulation appers to stop after first time step. 
- <i> taille\_microtubule </i> -- distance in nanometers, default 8. See D\_bundle.  
- <i> tan\_Angle\_mb </i> -- default 0.839099631177
- <i> vtk\_steps </i> -- time steps gap between recording the microtubule positions in an output .vtk file 

<h3>Outputs</h3>

Microtubule positions are stored as vtk files during the simulation
and the user can specify how often a 'snapshot' should be taken. These
can for example be visualised using <a
href=http://www.paraview.org>Paraview</a> (see example Figure for some
settings that can be used).

<center><img width=500 src="microtubule_simulation_output.png"></center>

Some versions of paraview do not have the Point Gaussian available. In which case, after
reading in the data create a Glyph. Set the "Glyph Type" to "sphere" with radius 0.05, and under Masking set "Glyph
Mode" to "All Points". 

Also a file with a summary of the simulation state over time is generated. This little file is
created inside <tt>src/main.cpp</tt> in the <i>rapport()</i> function (if you want to see
or adjust which parameter values that are reported). Again, the user
can specify how often the statistics is reported.

<h3>The c++ code</h3>

Be aware that it is non-optimal as it was my first attempt at coding in c++ language 
with 0 background. 

The basic principle is :
- a <i>MicrotubulePool</i> object manages the microtubules (destruction, creation, evolution...)
- an <i>ElementPool</i> contains all the elements (the tubulin vectors basically and the shell)

Then a Microtubule is an object that contains elements.
The main loop (in main.cpp) launches the evolve function of the MicrotubulePool, 
which in return launches the evolution of each of its microtubules.

The reading/setting of parameter values is made inside the <i>main</i>
function via the class <i>Parametres</i> constructor. All the
functions that concern the growth itself are called grow_something.
