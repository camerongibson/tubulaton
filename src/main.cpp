#include <iostream>
#include <list>
#include "ElementPool.hpp"
#include "MicrotubulePool.hpp"
#include "Microtubule.hpp"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "drawScene.hpp"
#include <string>
#include "math.h"
#include "Contour.hpp"
#include "Parametres.hpp"
#include <sys/time.h>
//#define _POSIX_SOURCE
#include <unistd.h>


#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

using namespace std;


long long
timeval_diff(struct timeval *difference,
             struct timeval *end_time,
             struct timeval *start_time
            )
{
  struct timeval temp_diff;

  if(difference==NULL)
  {
    difference=&temp_diff;
  }

  difference->tv_sec =end_time->tv_sec -start_time->tv_sec ;
  difference->tv_usec=end_time->tv_usec-start_time->tv_usec;

  /* Using while instead of if below makes the code slightly more robust. */

  while(difference->tv_usec<0)
  {
    difference->tv_usec+=1000000;
    difference->tv_sec -=1;
  }

  return 1000000LL*difference->tv_sec+
                   difference->tv_usec;

} /* timeval_diff() */

// Record value of certain parameters pertaining to the system state such as the number of microtubules as well as parameters which have changed since the last readout for example number of microtubules added. Output file given by "s_filename"
int rapport(string s_filename, MicrotubulePool *m,ElementPool *e)
{
    //rencontre_bundle_grow;rencontre_bundle_shrink;rencontre_mb_grow;rencontre_mb_shrink;cross;quitte_mb;destruction_microtubule;creation_microtubule;cut_microtubule;detachements
    ofstream myfile;
    char *filename = (char*)s_filename.c_str();
    myfile.open(filename, std::ios_base::app);
    myfile << e->rencontre_bundle_grow<<";";
    myfile << e->rencontre_bundle_shrink<<";";
    myfile << e->rencontre_mb_grow<<";";
    myfile << e->rencontre_mb_shrink<<";";
    myfile << e->cross<<";";
    myfile << e->quitte_mb<<";";
    myfile << m->destruction_microtubule<<";";
    myfile << m->creation_microtubule<<";";
    myfile << m->cut_microtubule<<";";
    myfile << m->override_cut_microtubule<<";";
    myfile << m->detachements<<";";
    myfile << m->getListMicrotubules().size()<<";";
    
    //Calculate number of elements in the list
    //list<Element *> m_cross_tocut = e->getlist_crossover_tocut();  
    //int temp=0;
    //for (list<Element *>::iterator it=m_cross_tocut.begin();it!=m_cross_tocut.end();it++){temp+=1;}
    myfile << e->getlist_crossover_tocut().size() <<";"; // Number of crossovers being analysed for cutting


    // Ouput the length of every microtubule
    string lenFile=s_filename;
    lenFile=lenFile.substr(0, lenFile.find(".txt"));
    lenFile=lenFile+"_lengths.txt";

    char *lenFile_1 = (char*)lenFile.c_str();
    ofstream myfile1;
    myfile1.open(lenFile_1, std::ios_base::app);
    std::vector<int> m_len=m->getMicrotubuleLengths();
    for (int count_j=0; count_j<m_len.size();count_j=count_j+1){
     	myfile1 << m_len[count_j]<<";";
       }
    myfile << endl;
    myfile.close();
    myfile1 << endl;
    myfile1.close();
   
    e->rencontre_bundle_grow=0;
    e->rencontre_bundle_shrink=0;
    e->rencontre_mb_grow=0;
    e->rencontre_mb_shrink=0;
    e->cross=0;
    e->quitte_mb=0;
    m->destruction_microtubule=0;
    m->creation_microtubule=0;
    m->cut_microtubule=0;
    m->override_cut_microtubule=0;
    m->detachements=0;
    
    return 0;
}

int main(int argc, char ** argv)
{
    struct timeval earlier;
    struct timeval later;
    struct timeval interval;

    if(gettimeofday(&earlier,NULL))
      {
	perror("gettimeofday()");
	exit(1);
      }
    if(argc!=2)
      {
	cout << endl << "Usage: " << argv[0] << " conf.ini" << endl << endl
	     << "where conf.ini is a file with model and simulation parameters "
	     << "(See README file)." << endl;
	exit(1);
      }
	  
    srand(time(0)+getpid());
    //cout << "Le nom du programme est : " << argv[0] << endl;
    //cout << "L'argument est : " << argv[1] << endl;
    //cout << "premier alea : " << rand() << endl;
    deb("main : instancie ElementPool");
    cout.precision(5);
    //srand ( time(NULL) );
    Parametres Params((string)argv[1]);
    //Params.initialise_default();
    ElementPool Epool(&Params);
    deb("main : instancie Microtubule Pool");
    MicrotubulePool Mpool(&Params,&Epool);
    deb("main : instancie Contour");    
    Contour c(&Params,&Epool, 1);
    c.open();
    Mpool.initiate();

    //cout << "Crossmicro cut = " << Params.getProprieteD("proba_crossmicro_cut") << endl;
    //cout << "Crossmicro crossmicro_shrink = " << Params.getProprieteD("proba_crossmicro_shrink") << endl;

    string s =Params.getProprieteS("nom_folder_output_vtk")+Params.getProprieteS("nom_output_vtk");
    drawScene draw;
    deb("main 3"); 
    //if (i == 1) {grow_plus();}
    //if (i == 2) {shrink_plus();}
    //if (i == 3) {shrink_moins();}
    //if (i == 4) {scission(15);}
    list<Id> lm_provi;

    for (int t=0;t<(Params.getProprieteI("nb_max_steps")+1);t++)
      { //cout<<"t=" << t << "   CutMicrotubules=" << Mpool.cut_microtubule << "     Overridden cut microtubules=" << Mpool.override_cut_microtubule<<endl; //Checking number of cuts and overrided cuts
        //timeval_diff(&interval,&later,&earlier);
        //cout << t << endl;
        deb("main 4 boucle evolve()"); 
        Mpool.evolve(&Epool);
        if (fmod(t,Params.getProprieteI("garbage_steps")) == 1)
	  {
            Epool.garbageCollector();
	  }
        //if (fmod(t,1) == 0)
        //{
	//cout << "liste" <<endl;
	//list<Element *> l_tocut = Epool.getlist_tocut();        
	//for (list<Element *>::iterator it=l_tocut.begin();it!=l_tocut.end();it++)
	//{
	//cout << **it <<  endl;
	//if ((*it)->getPrevious())
	//{
	//cout << "\t"<<*((*it)->getPrevious()) <<  endl;
	//}
	//}
        //}
        if (fmod(t,Params.getProprieteI("save_events")) == 0)
	  {
	    deb("rapport"); 
            string s2 =Params.getProprieteS("nom_folder_output_vtk")+Params.getProprieteS("nom_rapport");
            rapport(s2,&Mpool,&Epool);
	  }
        
        if ((fmod(t,1000) == 0) )
	  {
	    deb("main 4 boucle getListMicrotubules()"); 
	    lm_provi = Mpool.getListMicrotubules();
	    deb("main 4 boucle getListElements()"); 
	    list<Element *> lm_provi2 = Epool.getListElements();
            deb("main 4 boucle getListId2Space()"); 
            list<Element *> lm_provi3 = Epool.getListId2Space();

	    if(gettimeofday(&later,NULL))
	      {
                perror("fourth gettimeofday()");
                exit(1);
	      }
	    timeval_diff(&interval,&later,&earlier);
	    cout << t << "I" << argv[0] << endl;
            deb("main 4 fin boucle getListMicrotubules()"); 
	  }
        if ( (fmod(t,Params.getProprieteI("vtk_steps")) == 0) | Params.getProprieteI("stop"))
	  {
            draw.pool2vtk(s, &Epool, &Mpool, Params.getProprieteI("details"), t);
            draw.Contour2vtk(s, &Epool, &Mpool, &Params, t);
	  }
        if (Params.getProprieteI("stop"))
	  {
	    cout << "on me demande d'arreter" <<endl;
	    exit(0);
	  }               
      }
	
	//Test to see what microtubules are left at the end of the simulation.
//      list<Id> lp1 = Mpool.getListMicrotubules();
//      int count=0;
//      for (list<Id>::iterator it=lp1.begin();it!=lp1.end();it++)
//        {   count++;
//	    std::cout << Mpool.getMicrotubule(*it)->getAge()<<Mpool.getMicrotubule(*it)->getSize()<<endl;
//	}
//	std::cout << endl << "Number of microtubules still alive: "<<count << endl;
}




