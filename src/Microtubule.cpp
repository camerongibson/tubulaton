#include <deque>
#include <cstdlib>
#include "Microtubule.hpp"
#include "Structure.hpp"
#include "ElementPool.hpp"
#include "Parametres.hpp"
#include "MicrotubulePool.hpp"
#include "math.h"
#include "utility.hpp"

#define verbose 0
#define deb(x) if (verbose>=1){cout << x << endl;}
#define deb2(x) if (verbose>=2){cout << x << endl;}
#define deb3(x) if (verbose>=3){cout << x << endl;}

using namespace std;

std::ostream& Microtubule::Print( std::ostream &os) const
{
    os << "Microtubule "<<this->getId()<<"/lg= "<< this->length()<<" age "<<this->getAge()<<endl;    
return os;
}

ostream& operator<<(ostream& os, Structure& m)
{
    m.Print(os);
    return os;
}

ostream& operator<<(ostream& os, Microtubule& m)
{
    m.Print(os);
    return os;
}



Microtubule::Microtubule():Structure()
{
    deb("Microtubule()");
    m_id=0;
    m_type=1;
    m_age=0;
    this->std_properties();

    
}

Microtubule::Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r):Structure()
{
    deb("Microtubule(ElementPool *ep, MicrotubulePool *mp, Id r)");
    m_id=r;
    m_type=1;
    m_age=0;
    m_elementPool=ep;
    m_microtubulePool=mp;
    m_params=params;
    this->std_properties();
    Element *nouv = ep->giveElement();
    Anchor a;
    Shape s;
    vector<double> vec;

	double x = 500+(float)rand()/(float)RAND_MAX * 200;
	double y = 500+(float)rand()/(float)RAND_MAX * 200;
	double z = 500+(float)rand()/(float)RAND_MAX * 200;	
	vec.push_back(x);
	vec.push_back(y);
	vec.push_back(z);

	a.setPosition(vec);
	x = -1+2*(float)rand()/(float)RAND_MAX;
	y = -1+2*(float)rand()/(float)RAND_MAX;
	z = -1+2*(float)rand()/(float)RAND_MAX;
	double pond_alea=sqrt(pow(x,2) + pow(y,2) + pow(z,2));
	vector<double> dir;
	dir.push_back(x/pond_alea);
	dir.push_back(y/pond_alea);
	dir.push_back(z/pond_alea);
	s.setDirection(dir);

    nouv->setPrevious(NULL);
    nouv->setShape(s);
    nouv->setAnchor(a);
    nouv->setStructure(this);
    m_elementPool->spaceMtRegister(nouv);
	m_body.push_back( nouv );
}

Microtubule::Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r, vector<double> v, int cont_num):Structure()
{
    deb("Microtubule(ElementPool *ep, MicrotubulePool *mp, Id r, vector<double> v)");
    m_id=r;
    m_type=1;
    m_age=0;
    m_elementPool=ep;
    m_microtubulePool=mp;
    m_params=params;
    this->std_properties();
    deb("Microtubule() before Element *nouv");
    Element *nouv = ep->giveElement();
    Anchor a;
    Shape s;
// 	double x;
// 	double y;
// 	double z;
    vector<double> vec;
    vec.push_back(v[0]);
    vec.push_back(v[1]);
    vec.push_back(v[2]);
    vector<double> dir;
    dir.push_back(v[3]);
    dir.push_back(v[4]);
    dir.push_back(v[5]);

	a.setPosition(vec);
	s.setDirection(dir);

    nouv->setPrevious(NULL);
    nouv->setShape(s);
    nouv->setAnchor(a);
    nouv->setStructure(this);
    nouv->setPropriete("Cortical_Structure",cont_num);
    deb("Microtubule() before spaceMtRegister");
    m_elementPool->spaceMtRegister(nouv);
	m_body.push_back( nouv );
}

//TODO : voir si ce constructeur est nÃ©cessaire, auquel cas adapter Ã  la version avec pointeurs
Microtubule::Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r, deque<Element *> body):Structure()
{
    deb("Microtubule(ElementPool *ep, MicrotubulePool *mp, Id r, deque<Id> body)");
    m_id=r;
    m_type=1;
    m_age=0;
    m_elementPool=ep;
    m_microtubulePool=mp;
    m_params=params;
    this->std_properties();
    for (size_t i = 0; i<body.size();i++)
    {
        body[i]->setStructure(this);
        m_body.push_back(body[i]);
        
    }
}

Microtubule::~Microtubule()
{
    //if (verbose){cout << "utilisation du destructeur Microtubule : "<<m_type<<" "<<m_id<<endl;}
}

void Microtubule::std_properties()
{

    m_proprietesI["gr_st_plus"]=1; //state of growth of the plus side
    m_proprietesI["gr_st_moins"]=0; //state of growth of the moins side
    
    //manque les conditions de nucleation 
}

// Grows or shrinks the microtubule plus and/or minus end depending on parameter values. values =1 is growth, =0 nothing, =-1 shrinking. "plus" refers to the pluss end (end of vector) and "moins" to the minus end (beginning of vector)
void Microtubule::evolve()
{
    int toto=1;
    m_age++;
    if (m_proprietesI["gr_st_plus"]==1) //state of growth of the plus side
    {
        grow_plus();
    }
    if (m_proprietesI["gr_st_plus"]==0) //state of growth of the plus side
    {
        //grow_plus();
    }
    if (m_proprietesI["gr_st_plus"]==-1)
    {
        toto = shrink_plus();
    }
    if (toto)
	{
		if ((m_proprietesI["gr_st_moins"]==-1) /*&& (fmod(m_age,2)==0)*/) //state of growth of the moins side
		{
			shrink_moins(); 
		}
		else if (m_proprietesI["gr_st_moins"]==1) 
		{
			cout << "error: Minus end of microtubule growing, not coded"<<endl;
			exit(1);
		}
	}
	
    deb("fin evolve microtubule");
}

void Microtubule::evolve(int i)
{
    if (i == 1) {grow_plus();}
    if (i == 2) {shrink_plus();}
    if (i == 3) {shrink_moins();}
    if (i == 4) 
	{
// 		int s=m_body.size();
// 		double pos=(float)rand()/(float)RAND_MAX*s;f
        //TODO ici souci de type
		//scission((int)pos);
	}
    m_age++;
}


void Microtubule::tag_tocut(Element *e)
{
    //this function will test the angle between the new tubulin and the previous one, and if necessary will enlist the tubulin in the tocut list
    if (m_params->getProprieteI("decision_cut"))
    {
        if ( e->getPrevious() )
        {
            vector<double> s1=e->getShape().getDirection();
            vector<double> s2=e->getPrevious()->getShape().getDirection();
            double a = angleVec(s1, s2);
            if ( (a >= m_params->getProprieteD("Angle_cut") ) && (e->getPrevious()->getPropriete("to_cut") != 1) && (e->getPropriete("to_cut") != 1))
            {
                //e->getPrevious()->setPropriete("to_cut", 1);
                m_elementPool->enlist_tocut(e->getPrevious());
                //cout << "en liste :"<<*(e) <<  endl;
                //cout << "\t"<<*(e->getPrevious()) <<  endl;
                //cout << "\t\t"<<*(e->getStructure()) <<  endl;
            }
        }
        else
        {
            printf("case of to_cut enlisting whereas no previous element exists");
            exit(0);
        }
    }
    
    
}

void Microtubule::grow_plus()
{
    //deb("grow plus");
// 	int cas = 0;
	//c'est ici que se concentre :
	//- le test de proximitÃ© des voisins, 
	//- le test de proximitÃ© du bord
	//rÃ©cupÃ©ration de l'id du plus actuel du microtubule
    //deb("grow plus 2");
	Element *previous=m_body.back();
    //cout<<"previous "<<*previous<<endl;

    //deb("grow plus 3");
    // test if the microtubule is empty (would not be normal)
	if (m_body.empty()){cout<<"VIDE!!!!"<<endl;exit(EXIT_FAILURE);}
	//on rÃ©cupÃ¨re les paramÃ¨tres de l'ancien plus
    std::pair<double, Element*> prochesMt = m_elementPool->NN_Mt(previous);
    std::pair<double, Element*> prochesContour = m_elementPool->NN_Contour(previous);
    //Element *el_cont = prochesContour12.second;
    //el->setPropriete("Cortical_Structure", el_cont->getPropriete("Structure_Number"));
    //cout << prochesMt.first << "  " << prochesContour.first<< endl;
    //TODO : revoir ces conditions:
    // cond 1 : distance au plus proche voisin infÃ©rieure Ã  distance des paramÃ¨tres
    // cond 2 : il y a un voisin (!=-1)
    // cond3 : le voisin prÃ©dit n'est pas la tubuline prÃ©cÃ©dente
    // cond4 : la tubuline voisine n'a pas un age de 0
    //deb("grow plus 1");
    if ( (prochesMt.first < (m_params->getProprieteD("D_bundle") / m_params->getProprieteD("taille_microtubule"))) && (prochesMt.first != -1)  && ( prochesMt.second != previous ) && ( prochesMt.second->getAge() != 0 ) )
    {
        //if ( (prochesContour.first != 10000) && (prochesContour.first < 50) && (prochesContour.first != -1))
        //{
            //grow_plus_contact_mb(previous, prochesContour);
        //}
        //else
        {

            deb("grow plus | grow_plus_contact_voisin(previous, prochesMt)");
            grow_plus_contact_voisin(previous, prochesMt, prochesContour);
        }
        previous->setAge(1);
        //m_proprietesI["gr_st_plus"]=1;
    }
    else //je suis peut Ãªtre libre ou proche d'une membrane
         //I may be free or close to the membrane
    {
        //cond1 : la distance n'est pas 10000 : useless condition cause 10000 is not assigned, it is 100000 -> sqrt
        //TODO  ---> l'enlever 
        //cond2 : la distance est infÃ©rieure Ã  100 (The distance is less than 100)
        //cond3 : il y a un voisin (There is a neighbour)
        if ( (prochesContour.first != 10000) && (prochesContour.first < 100) && (prochesContour.first != -1))
        {
            deb("grow plus | grow_plus_contact_mb(previous, prochesContour");
            grow_plus_contact_mb(previous, prochesContour);
        }
        else
        {
			//WARNING TODO : we never reach this condition, growth free is then in fact managed inside contact_mb code
			deb("grow plus | grow_plus_free(previous");
            grow_plus_free(previous);            
        }
        previous->setAge(1);
        //m_proprietesI["gr_st_plus"]=1;
    }


}

//Microtubule growth when the pluss end meets another microtubule closely. If angle small enough its direction changes so a bundle starts to form. If angle large then it can't form a bundle and stops growing. But its behaviour depends on the parameter "decision_rencontre" - normally -1 to indicate microtubule should shrink or if left at 1 microtubule doesn't grow for now but could in future if space is made. 
void Microtubule::grow_plus_contact_voisin(Element* previous, std::pair<double, Element *> proche12, std::pair<double, Element*> prochesContour12)
{
    deb2("grow_plus_contact_voisin");
    deb3("grow_plus_contact_voisin");
    vector<double> dir;
    double a;

    Element *el = proche12.second;
    el->setPropriete("contact", 1);
    //Element *el_cont = prochesContour12.second;
    //el->setPropriete("Cortical_Structure", el_cont->getPropriete("Structure_Number"));
    //cout << m_elementPool->getElement(proche12.second, "grow plus 0")->getPropriete("contact") << endl;		
    //ici je peux d'ores et dÃ©jÃ  prÃ©voir la condition "passe ou aligne toi" : il s'agit de comparer s et el->getShape()
    vector<double> s1=previous->getShape().getDirection();
    Anchor *anc = previous->getAnchor();
    double aX=*anc->getX();
    double aY=*anc->getY();
    double aZ=*anc->getZ();
    vector<double> s2=el->getShape().getDirection();
    a = angleVec(s1, s2);
    //cout<<"angle "<<a;
    deb2("grow_plus_contact_voisin 1");
    if ( (a < m_params->getProprieteD("Angle_bundle")) | (a > (3.141592653589793 - m_params->getProprieteD("Angle_bundle"))) )
    {
        if (previous->getPropriete("contact") != 1)
        {
            m_elementPool->rencontre_bundle_grow+=1;
        }
        
        //cout<<"\t glisse ";
        if (a < m_params->getProprieteD("Angle_bundle"))
        {
            //cout<<" dans le meme sens "<<endl;
            dir.push_back(s2[0]);
            dir.push_back(s2[1]);
            dir.push_back(s2[2]);
        }
        if (a > (3.141592653589793 - m_params->getProprieteD("Angle_bundle")))
        {
            //cout<<" dans l'autre sens "<<endl;
            dir.push_back(-s2[0]);
            dir.push_back(-s2[1]);
            dir.push_back(-s2[2]);
        }
        
        Element  *nouv=m_elementPool->giveElement();
        nouv->setStructure(this);
        //placement de son id dans le microtubule
        m_body.push_back(nouv);
        deb2("grow_plus_contact_voisin 2");
        //positionnement Ã  la suite de l'Ã©lÃ©ment prÃ©cÃ©dent
        vector<double> pos;
        pos.push_back( aX + s1[0]);
        pos.push_back( aY + s1[1]);
        pos.push_back( aZ + s1[2] );
        Anchor a2(pos[0], pos[1],pos[2]);
        deb2("grow_plus_contact_voisin 3");
        nouv->setAnchor(a2);    
        //on crÃ©Ã© la nouvelle direction et on l'assigne
        Shape s2(dir[0], dir[1],dir[2]);
        nouv->setShape(s2);    
        nouv->setPrevious(previous);
        nouv->setPropriete("contact", 1.);
        nouv->setPropriete("Cortical_Structure",el->getPropriete("Cortical_Structure"));
        nouv->setPropriete("nearest_contour",el->getPropriete("nearest_contour"));
		nouv->setPropriete("nearest_contour_vtk",el->getPropriete("nearest_contour_vtk"));
		nouv->setPropriete("nearest_contour_d",el->getPropriete("nearest_contour_d"));
		nouv->setPropriete("D_pos_dir2lim_mbFn",el->getPropriete("D_pos_dir2lim_mbFn"));
		nouv->setPropriete("cortical",el->getPropriete("cortical"));
        m_elementPool->spaceMtRegister(nouv);
        tag_tocut(nouv);

    }
    else//si l'angle est mauvais ->shrink If the angle is big then shrink
    {
        //if (m_proprietesI["gr_st_plus"] != m_params->getProprieteI("decision_rencontre"))
        //{
        //    m_elementPool->rencontre_bundle_shrink+=1;
        //}
        //m_proprietesI["gr_st_plus"]=m_params->getProprieteI("decision_rencontre");
        //cout<<"\t stoppe "<<endl;

	// Determine whether microtubule grows or not. First: check probability that it starts shrinking rather than crossing over.If shrinking no growth and change gr_st_plus value. Otherwise it crosses over the other microtubule and grows using our other growth function. "else" condition copied from "Microtubule::grow_plus()".

       int cross_lim=static_cast<int>(m_params->getProprieteD("cutgroup")); //Turn float input to integer type
       //cout << cross_lim << endl;
       if (previous->getPrevious()!=NULL and cross_lim!=0){
       		previous->setCrossNum(((previous->getPrevious()->getCrossNum()) % cross_lim)+1);
       		//cout<<previous->getCrossNum()<<endl;
	}
       else{
		previous->setCrossNum(1);
       		//cout<<previous->getCrossNum()<<endl;
	}

       double proba = (float)rand()/(float)RAND_MAX;
       if ((proba < m_params->getProprieteD("proba_crossmicro_shrink") and (cross_lim==0 or (cross_lim>=1 and previous->getCrossNum()==m_params->getProprieteD("cutgroup")))))
	{
	m_elementPool->rencontre_bundle_shrink+=1;
	m_proprietesI["gr_st_plus"]=-1;
	}
	else
	{       
		if ( (prochesContour12.first != 10000) && (prochesContour12.first < 100) && (prochesContour12.first != -1))
        	{
            	deb("grow plus | grow_plus_contact_mb(previous, prochesContour12");
            	grow_plus_contact_mb(previous, prochesContour12);
		
		if ((cross_lim==0 or (cross_lim>=1 and previous->getCrossNum()==cross_lim))){
			m_elementPool->enlist_crossover_tocut(previous); //Marks this element as a place we may cut as it is about to be a crossover
                	previous->setCrossMicro(el);  // Marks the element which is being crossed over
			}
                if (el->isAlive()!=1){cout<<"Error: Element not alive"<<endl;}
		}
        	else
        	{
		//WARNING TODO : we never reach this condition, growth free is then in fact managed inside contact_mb code.NOTE: this function is currently reached sometimes
		//cout << "Warning: Reach free grwoth in Microtubule.cpp" << endl; //This message can be included for clarity but probably not necessary as above message suggests contact_mb could deal with this growht. 
		deb("grow plus | grow_plus_free(previous");
            	grow_plus_free(previous); 
		if ((cross_lim==0 or (cross_lim>=1 and previous->getCrossNum()==cross_lim))){ 
                	m_elementPool->enlist_crossover_tocut(previous); //Marks this element as a place we may cut as it is about to be a crossover       
                	previous->setCrossMicro(el);    // Marks the element which is being crossed over
		}
                if (el->isAlive()!=1){cout<<"Error: Element not alive"<<endl;}
        	}
	}
    }
    
    
    deb3("FIN grow_plus_contact_voisin");
    
}

// Calculate MT growth if close to the membrane. Not: the projection of vector u onto a plane with normal n is u-(u \dot n)/norm(n)*n, see https://www.maplesoft.com/support/help/maple/view.aspx?path=MathApps%2FProjectionOfVectorOntoPlane for more details. This is used throughout this function. 
void Microtubule::grow_plus_contact_mb(Element *plus, std::pair<double, Element*> proche_paroi)
{
    //TODO (13 05 13) : vérifier peut être la normalité de tous les vecteurs du contour?
    deb2("grow_plus_contact_mb");
    //deb2("grow_plus_contact_mb");        
    //initialisation des vecteurs, directions et positions
    vector<double> pos_dir;
    vector<double> pos_dir2pos_mb;
    vector<double> n_perp;
    vector<double> dir_cand;
    
    int tag_cortical=0;//le tag utilisé pour définir si le nouvel élément sera proche de la membrane. The tag used to define if the new point will be close to the membrane
    
    //Position and direction of the previous point on the microtubule
    Shape Sdir_prev = plus->getShape();
    vector<double> dir_prev;
    dir_prev.push_back(Sdir_prev.getDirection()[0]);
    dir_prev.push_back(Sdir_prev.getDirection()[1]);
    dir_prev.push_back(Sdir_prev.getDirection()[2]);    
    Anchor *pos_prev = plus->getAnchor();
    
    //Normal and position of nearest point on the membrane. 
    Element *paroi = proche_paroi.second;
    paroi->setPropriete("contact", 1.);
    Shape n = paroi->getShape();   //for a membrane point this is the normal to the membrane surface
    Anchor *pos_mb = paroi->getAnchor(); //the position of hte nearest point on the membrane.
    
    vector<double> influence = paroi->getPropriete_v("strain_0");
    double attache = paroi->getPropriete("attachement");
    //cout << "attache :"<< attache.size() <<endl;
    //ok un peu étrange, mais je transforme "attache" en "accrochage"... 
    int accrochage=0;
    if ((attache == 1) )
    {
		accrochage=1;
	}
	//cout << "accrochage :"<< accrochage <<endl;
    //TODO : voir si ce calcul vaut la peine
    //calcul des coordonnees du vecteur dir_prev_perp, situé dans le plan de dir_prev,n
    deb2("calcul des coordonnees du vecteur dir_prev_perp, situé dans le plan de dir_prev/n et perpendiculaire à dir prev");
    deb2("cacluate the components of the vector dir_prev_perp, in the to the plane and perpendicular to the plane with normal dir_prev");
    //Calculate the coordinates of the vector dir_prev_perp a proejction of the membrane normal n onto the plane with normal dir_prev
    vector<double> dir_prev_perp;
    double scalaire = dir_prev[0] * n.getDirection()[0] + dir_prev[1] * n.getDirection()[1] + dir_prev[2] * n.getDirection()[2];
    dir_prev_perp.push_back(n.getDirection()[0] - scalaire * dir_prev[0]);
    dir_prev_perp.push_back(n.getDirection()[1] - scalaire * dir_prev[1]);
    dir_prev_perp.push_back(n.getDirection()[2] - scalaire * dir_prev[2]);
    dir_prev_perp=normalise(dir_prev_perp);
    
    deb2("calcul des coordonnees du vecteur n_perp, situé dans le plan de dir_prev/n et perpendiculaire à n");
    // Calculate the vector n_perp, the projection of MT direction dir_prev onto the membrane plane with normal n. 
    n_perp.push_back(dir_prev[0] - scalaire * n.getDirection()[0]);
    n_perp.push_back(dir_prev[1] - scalaire * n.getDirection()[1]);
    n_perp.push_back(dir_prev[2] - scalaire * n.getDirection()[2]);
    n_perp=normalise(n_perp);
    
    deb2("calcul des coordonnées du point pos_dir : point prévisionnel de la prochaine tubuline"); //Calculate the predicted point of the tubulin
    //Calculate the next expected pont of the tubulin given by its previous position plus the direction vector. 
    pos_dir.push_back(*pos_prev->getX() + dir_prev[0]);
    pos_dir.push_back(*pos_prev->getY() + dir_prev[1]);
    pos_dir.push_back(*pos_prev->getZ() + dir_prev[2]);
    
    deb2("calcul des coordonnées du vecteur pos_dir2pos_mb");
    // pos_dir2pos_mb is the difference between the nerest neightbour point on the membrane we identified before and the new MT point
    pos_dir2pos_mb.push_back(*pos_mb->getX() - pos_dir[0]);
    pos_dir2pos_mb.push_back(*pos_mb->getY() - pos_dir[1] );
    pos_dir2pos_mb.push_back(*pos_mb->getZ() - pos_dir[2] );
    deb2("calcul de la distance D_pos_dir_alea2pos_mbFn");
    //dot product of pos_dir2pos_mb and the membrane normal (gives an idea of whether the new point is inside or outside the membrane and by how much). If >0 then inside and if <0 the outside.
    double D_pos_dir2pos_mbFn = pos_dir2pos_mb[0] * n.getDirection()[0] + pos_dir2pos_mb[1] * n.getDirection()[1] + pos_dir2pos_mb[2] * n.getDirection()[2];
        //si c'est négatif il y a sans doute un souci, même si une courbure importante peut sans doute l'expliquer
        //TODO(13 05 13) ajouter la verif
        //deb2(D_pos_dir2pos_mbFn);
        //A noter : ici d_mb est donc au carré puisque la distance n'est pas en sqrt
    // Calculate D_pos_dir2lim_mbFn. Minus distance perpendicular to the membrane by constant, likely as only counts as membrane interaction if not beyond this dsitance.. 
    double D_pos_dir2lim_mbFn = D_pos_dir2pos_mbFn - m_params->getProprieteD("d_mb");
    //if ( (D_pos_dir2lim_mbFn < 0) | (m_params->getProprieteI("decision_accrochage")))
    //je rajoute la condition accrochage, qui vaut 0 par défaut si je n'ai pas de donnée d'accrochage et 1 si j'ai une donnée et qu'elle me dit de m'accrocher.
    //cond1 : la distance entre le point prévu et la membrane est inférieure à 0 == le point prévu est hors de la structure
    //OU
    //cond2 : le paramètre decision_accrochage vaut 1
    //OU
    //cond3 : le point du mesh le plus proche a 1 en "attachement" ET le point précédent est cortical
    
    // If D_pos_dir2lim_mbFn<0 then have a membrane interaction as a vector within d_mb of the membrane or beyond the membrane. Also have a membrane interaction if certain membrane interaction parameters are set that imposes a membrane interaction. In this case motion will be pushed back towards the membrane. 
    if ( (D_pos_dir2lim_mbFn < 0) | (m_params->getProprieteI("decision_accrochage")  ) | ( (accrochage==1) && (plus->getPropriete("cortical")==1) )  )
    {
		//cout << "accrochage :"<< accrochage <<endl;
		tag_cortical=1;
        deb2("on est repoussé"); //We are pushed back
        deb2("calcul des deux vecteurs maximum possibles"); //Calculate the two maximum vectors possible
        vector<double> dir_cand_extr1;
        vector<double> dir_cand_extr2;
        dir_cand_extr1.push_back(dir_prev_perp[0]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[0]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr1.push_back(dir_prev_perp[1]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[1]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr1.push_back(dir_prev_perp[2]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[2]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr2.push_back(- dir_prev_perp[0]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[0]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr2.push_back(- dir_prev_perp[1]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[1]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr2.push_back(- dir_prev_perp[2]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[2]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        deb2("calcul des deux vecteurs maximum possibles 2");

        //calcul des deux scalaires theoriques
        //TODO : ici souci ce n'est pas la taille du vecteur qui compte mais vecteur + point d'origine
        //Calculate the proportion of dir_cand and dir_can_extra1 in the normal direction. Sign > is outwards and < is inwards.
        double dir_cand_extr1_proj = dir_cand_extr1[0] * n.getDirection()[0] + dir_cand_extr1[1] * n.getDirection()[1] + dir_cand_extr1[2] * n.getDirection()[2];
        double dir_cand_extr2_proj = dir_cand_extr2[0] * n.getDirection()[0] + dir_cand_extr2[1] * n.getDirection()[1] + dir_cand_extr2[2] * n.getDirection()[2];
        //deb(dir_cand_extr2[0]);
        //conds : l'idée était de voir si le scalaire de la nouvelle direction fait passer au travers la membrane Ces conditions ne peuvent être réalisées que si on a une D_pos_dir2lim_mbFn < 1 ou > -1. This idea is to see if the scalar of the new direction passes through the membrane. These conditions can only be realised if D_pos_dir2lim_mbFn < 1 or > -1
        if (
        ( (dir_cand_extr1_proj > D_pos_dir2lim_mbFn) && (dir_cand_extr2_proj < D_pos_dir2lim_mbFn ) )
        |
        ( (dir_cand_extr1_proj < D_pos_dir2lim_mbFn) && (dir_cand_extr2_proj > D_pos_dir2lim_mbFn ) )
        )
        {
            deb2("on est dans le cas d'une intersection");
            //We are in the case of an intersection
            //cette valeur "dangereuse" ne cause pas d'erreur car les conditions font que 0<||D_pos_dir2lim_mbFn||<1
            double perp=sqrt(1-D_pos_dir2lim_mbFn*D_pos_dir2lim_mbFn);
            //ici on rapproche du point d'équilibre. 
            dir_cand.push_back(D_pos_dir2lim_mbFn*n.getDirection()[0] + perp*n_perp[0]);
            dir_cand.push_back(D_pos_dir2lim_mbFn*n.getDirection()[1] + perp*n_perp[1]);
            dir_cand.push_back(D_pos_dir2lim_mbFn*n.getDirection()[2] + perp*n_perp[2]);
            
        }
        else
        {
            deb2("on n'est pas dans le cas d'une intersection");
            double un = fabs(dir_cand_extr1_proj - D_pos_dir2lim_mbFn);
            double deux = fabs(dir_cand_extr2_proj - D_pos_dir2lim_mbFn);
            if (un < deux)
            {
                dir_cand.push_back(dir_cand_extr1[0]);
                dir_cand.push_back(dir_cand_extr1[1]);
                dir_cand.push_back(dir_cand_extr1[2]);
            }
            else
            {
                dir_cand.push_back(dir_cand_extr2[0]);
                dir_cand.push_back(dir_cand_extr2[1]);
                dir_cand.push_back(dir_cand_extr2[2]);
            }
        }
        dir_cand=normalise(dir_cand);
    }
    //cond1 : la distance à la membrane du point prévu est inférieure à 'épaisseur corticale
    //ET
    //cond2 : le point est sous la membrane
    //ET
    //cond3 : la direction du nouveau vecteur est vers l'extérieur
    //If within distance  "epaisseur_corticale"+d_mb of the membrane && inside the membrane && direction of motion is towards out of the membrane.
    // Then mark as on the cortex and set dir_cand as the component of your suugesion motion direc_prev along the membrane (removing the component out of the membrane)
    else if ( (D_pos_dir2lim_mbFn < m_params->getProprieteD("epaisseur_corticale")) && (D_pos_dir2lim_mbFn > 0) && (scalaire > 0) )
    {
		//ici je donne la propriété d'être proche de la zone corticale, condition sine qua none pour s'accrocher
		tag_cortical=1;
        //la nouvelle direction est perpendiculaire à la membrane. The new direciton is perpendicular to the membrane
        dir_cand.push_back(n_perp[0]);
        dir_cand.push_back(n_perp[1]);
        dir_cand.push_back(n_perp[2]);
        dir_cand=normalise(dir_cand);
    }
    //otherwise motion to next point is just what we set it to be at the last time step
    else
    {
        deb2("on est cytosolique");
        tag_cortical=0;
        dir_cand.push_back(dir_prev[0]);
        dir_cand.push_back(dir_prev[1]);
        dir_cand.push_back(dir_prev[2]);
        dir_cand=normalise(dir_cand);
        
        
    }

    //If nan then plane and Mt direction normal to each other so turn in a random direction along the membrane
    if (isnan(dir_cand[0]) || isnan(dir_cand[1]) || isnan(dir_cand[2])){
         if ((isnan(n_perp[0]) || isnan(n_perp[1]) || isnan(n_perp[2])) || (isnan(dir_prev_perp[0]) || isnan(dir_prev_perp[1]) || isnan(dir_prev_perp[2]))){
         dir_cand[0]=-1.+2*(float)rand()/(float)RAND_MAX - scalaire * n.getDirection()[0];
         dir_cand[1]=-1.+2*(float)rand()/(float)RAND_MAX - scalaire * n.getDirection()[1];
         dir_cand[2]=-1.+2*(float)rand()/(float)RAND_MAX - scalaire * n.getDirection()[2];    
         dir_cand=normalise(dir_cand);   }
         else {cout << "dir_cand is nan in Microtubules.cpp, grow_plus_contact_mb"<<endl; } 
        }


    deb2("ajout du facteur aleatoire"); //Adding the random factor
    double x = -1.+2*(float)rand()/(float)RAND_MAX;
    double y = -1.+2*(float)rand()/(float)RAND_MAX;
    double z = -1.+2*(float)rand()/(float)RAND_MAX;    
    int fixe=m_params->getProprieteD("part_alea_fixe");
	int alea=m_params->getProprieteD("part_alea_alea");
    dir_cand[0]=fixe*dir_cand[0]  + alea*x;
    dir_cand[1]=fixe*dir_cand[1]  + alea*y;
    dir_cand[2]=fixe*dir_cand[2]  + alea*z;
    dir_cand=normalise(dir_cand);//pour le moment on ne pondere pas pas la future definition de l'angle max
    
    //ici le cas où on a des champs de vecteur influençant la trajectoire
    //If factor for influencing the direction of the MTs is present then this factor is added here. 
    if ((influence.size() == 3) && (m_microtubulePool->getInfluence()>0) )
    {
        deb2("utilisation de l'influence");
        //influence du comportement standart
        double f1 = m_microtubulePool->getInfluence();
        //pondération de l'influence des directions venant de la donnée
        double f2 = m_params->getProprieteD("part_influence_influence");
        //cout << "utilisation de l'influence" <<endl;
        double sc_infl = influence[0]*n.getDirection()[0] + influence[1]*n.getDirection()[1] + influence[2]*n.getDirection()[2];
        vector<double > influence_v;
        influence_v.push_back(influence[0] - sc_infl * n.getDirection()[0]);
        influence_v.push_back(influence[1] - sc_infl * n.getDirection()[1]);
        influence_v.push_back(influence[2] - sc_infl * n.getDirection()[2]);
        double sens = influence_v[0]*dir_cand[0] + influence_v[1]*dir_cand[1] + influence_v[2]*dir_cand[2];
        //cout << "dir_cand 0 : "<<dir_cand[0]<< " dir_cand 1 : "<<dir_cand[1]<< " dir_cand 2 : "<<dir_cand[2] <<  endl;
        influence_v=normalise(influence_v);
        //cout << "influence 0 : "<<influence_v[0]<< " influence 1 : "<<influence_v[1]<< " influence 2 : "<<influence_v[2] <<  endl;
        if (sens >0)
        {
            dir_cand[0] = f1*dir_cand[0] + f2*influence_v[0];
            dir_cand[1] = f1*dir_cand[1] + f2*influence_v[1];
            dir_cand[2] = f1*dir_cand[2] + f2*influence_v[2];
            dir_cand=normalise(dir_cand);
        }
        else
        {
            dir_cand[0] = f1*dir_cand[0] - f2*influence_v[0];
            dir_cand[1] = f1*dir_cand[1] - f2*influence_v[1];
            dir_cand[2] = f1*dir_cand[2] - f2*influence_v[2];
            dir_cand=normalise(dir_cand);
        }
    }

    //ici je vais mettre la condition liée à l'angle max. Si l'angle prédit est trop grand, je ne rajoute pas d'élément. Adding the condition on predicted angle. If angle is too big don't add element. Note:actually does still add 1 more element but depending on "decision_rencontre" may only grow one final time. 
    if ( ( angleVec(dir_cand, dir_prev) > m_params->getProprieteD("Angle_mb_limite") ) )
    {
        //cout << "angle trop aigu" << angleVec(dir_cand, dir_prev) << endl;
        m_params->setProprieteI("stop",0);
        if (m_proprietesI["gr_st_plus"] != m_params->getProprieteI("decision_rencontre"))
        {
            m_elementPool->rencontre_mb_shrink+=1;
        }
        m_proprietesI["gr_st_plus"]=m_params->getProprieteI("decision_rencontre");
    }
    
    //Add the new element to the end of the MT and add specific information about it. 

    deb2("creation de l'element");
    Element *nouv=m_elementPool->giveElement();
    nouv->setStructure(this);
    //placement de son id dans le microtubule
    m_body.push_back(nouv);
    //positionnement à la suite de l'élément précédent
    deb2("creation de l'element Anchor");
    vector<double> pos;
    pos.push_back( *pos_prev->getX() + dir_prev[0]);
    pos.push_back( *pos_prev->getY() + dir_prev[1]);
    pos.push_back( *pos_prev->getZ() + dir_prev[2] );
    deb2(*pos_prev->getX());
    deb2(*pos_prev->getY());
    deb2(*pos_prev->getZ());
    deb2(dir_prev[0])
    deb2(dir_prev[1])
    deb2(dir_prev[2])
    Anchor a2(pos[0], pos[1],pos[2]);
    nouv->setAnchor(a2);    
    nouv->setPrevious(plus);

    deb2("creation de l'element Shape");
    //on créé la nouvelle direction et on l'assigne
    Shape s2(dir_cand[0], dir_cand[1],dir_cand[2]);            
    nouv->setShape(s2);
    //cout << c<< endl;
    //cout << *nouv << endl;
    m_elementPool->spaceMtRegister(nouv);
    nouv->setPropriete("nearest_contour",paroi->getId());
    nouv->setPropriete("nearest_contour_vtk",paroi->getId_vtk());
    nouv->setPropriete("nearest_contour_d",proche_paroi.first);
    nouv->setPropriete("D_pos_dir2lim_mbFn",D_pos_dir2lim_mbFn);
    //les conditions pour être en accrochage
    if (plus->getPropriete("cortical")==0)
    {
        m_elementPool->rencontre_mb_grow+=1;
    }
    if (tag_cortical==1){nouv->setPropriete("cortical",1);
        //cout<< "Tag number= " << paroi->getPropriete("Structure_Number")<<endl;
        nouv->setPropriete("Cortical_Structure", paroi->getPropriete("Structure_Number"));} //if said here on cortical set new element as listed on the cortical and which structure
    else {nouv->setPropriete("cortical",0);} //new microtubule not on cortical
    
    if ( (plus->getPropriete("cortical")==1) && nouv->getPropriete("cortical") == 0) //microtubule leaving the cortical
    {
        m_elementPool->quitte_mb+=1;
    }
    
    tag_tocut(nouv);
    //cout << nouv << *nouv << endl;
    deb2("FIN grow_plus_contact_mb");

}


// function grows the microtubule when it is unimpeeded by anything else in the space. It adds one element to the microtubule. Its position is dependent on the direction of the previous element. A new direction is assigned to this element, equal to the previous direction plus a randome deviation the magniture of which is dependent on the relative size of two input parameters. 
// Note this function is never called by the code as "grow_plus_contact_mb" does it instead. 
void Microtubule::grow_plus_free(Element *plus)
{
    deb("grow_plus_free");
    deb3("grow_plus_free");
    vector<double> dir;
    Anchor *a = plus->getAnchor();
    Shape s = plus->getShape();
    //ici : trajectoire sans influence, donc je rajoute juste un peu de stochasticitÃ©
    //crÃ©ation d'un vecteur alÃ©atoire
    double x = -1.+2*(float)rand()/(float)RAND_MAX;
    double y = -1.+2*(float)rand()/(float)RAND_MAX;
    double z = -1.+2*(float)rand()/(float)RAND_MAX;
    int fixe=m_params->getProprieteD("part_alea_fixe");
	  int alea=m_params->getProprieteD("part_alea_alea");
    x=fixe*s.getDirection()[0]  + alea*x;
    y=fixe*s.getDirection()[1]  + alea*y;
    z=fixe*s.getDirection()[2]  + alea*z;
    //normalisation
    double pond_dir=sqrt(pow(x,2) + pow(y,2) + pow(z,2) );
    dir.push_back( x/pond_dir );
    dir.push_back( y/pond_dir );
    dir.push_back( z/pond_dir );
    //cout << dir[0] << " face a ";
    Element *nouv=m_elementPool->giveElement();
    //TODO ici ajouter le fait de donner son previous Ã  l'Ã©lÃ©ment
    nouv->setPrevious(plus);
    
    nouv->setStructure(this);
    //placement de son id dans le microtubule
    m_body.push_back(nouv);
    //positionnement Ã  la suite de l'Ã©lÃ©ment prÃ©cÃ©dent
    vector<double> pos;
    pos.push_back( *a->getX() + s.getDirection()[0]);
    pos.push_back( *a->getY() + s.getDirection()[1]);
    pos.push_back( *a->getZ() + s.getDirection()[2] );
    Anchor a2(pos[0], pos[1],pos[2]);
    nouv->setAnchor(a2);    


    //ici ce qui gÃ¨re la nouvelle direction

    //on crÃ©Ã© la nouvelle direction et on l'assigne
    Shape s2(dir[0], dir[1],dir[2]);
    nouv->setShape(s2);   

    //deb("grow_plus_free before spaceMtRegister"); 
    m_elementPool->spaceMtRegister(nouv);    
    deb("grow_plus_free after spaceMtRegister");   
    if (plus->getPropriete("cortical")==1)
    {
        m_elementPool->quitte_mb+=1;
    }

}

// Shrinks the end of the microtubule by one element, but deleting the final element in the m_body. 
int Microtubule::shrink_plus()
{
//here the function is not void because of a hack... I do not know why but suddenly erasing the object from the unordered_map behaved as a destruction of the object. So a shrink_plus could not be followed by a shrink_moins if ever the former destroyed the microtubule. Here the shrink plus informs the evolve() function that it is not possible to go to shrink_moins
    deb("shrink plus"); 
    if ( !m_body.empty() )
    {
        deb("shrink plus !body empty"); 
		  Element *i = m_body.back();
          deb2("shrink plus !body empty setStructure(NULL)"); 
		  i->setStructure(NULL);
          deb2("shrink plus !body empty erase(m_body.begin()"); 
		  m_body.erase(m_body.end());
		  m_elementPool->erase(i);
	}
	if ( m_body.empty() )
	{
        deb("shrink plus body empty");
        //cout <<"test 1 : "<< m_proprietesI["gr_st_moins"]<<endl;
		m_microtubulePool->erase(m_id);
		deb("shrink plus OVER"); 
		return 0;
	}
	deb("shrink plus OVER"); 
	return 1;
}

//TODO : traduire Ã§a en pointeurs, ce n'est pas trop trivial...
// Shrinks the start of the microtubule by one element. It deletes the one element and updates the new start of the microtubule to indicate it is the starting point. 
void Microtubule::shrink_moins()
{
    deb("shrink moins"); 
    if ( !m_body.empty() )
    {
        deb("shrink moins !body empty"); 
        Element *i = m_body.front();
        //cout << "shrink moins" << *i << endl;
        deb("shrink moins !body empty setStructure(NULL)"); 
        i->setStructure(NULL);
        deb("shrink moins !body empty erase(m_body.begin()"); 
        m_body.erase(m_body.begin());
        Element *next = m_body.front();
        next->setPrevious(NULL);
        deb("shrink moins !body empty erase(i)"); 
        m_elementPool->erase(i); 
    }
    if ( m_body.empty() )
	{
        deb("shrink moins body empty");
		m_microtubulePool->erase(m_id);
	}
}

//TODO : commence Ã  Ãªtre pas toute jeune, Ã  vÃ©rifier d'autant que je viens de faire la traduction en pointeurs

// Cuts the microtubule "element" is a part of in two at the position "element" provided "element" is not the first point on the microtubule and the microtubule has more than three pieces. The initial half of the microtubule takes the places of the origional whole microtubule. The second microtubule becomes a new microtubule in our arrays with a new ID. Its properties are reset so it continues growing at its end but decays at its new backend. The first microtubule is now decaying from its end too.  
void Microtubule::scission(Element *element)
{
    deb("scission"); 
    
    
    deb3("scission"); 
	int s = m_body.size(); 
    //cout << "taille "<<s<<" " << element->getPrevious()<<endl;
    //if (element == m_body.front()) cout << "premier element"<<endl;
    deb3("scission 2"); 
	if ( (s >=3 ) && ( element != m_body.front() )  ) // Check not first element and microtubule longer than three 
	{
// 		int position;
		deque<Element *> first; // define what will become the first microtubule
		deque<Element *> second; // define what will become the second microtubule
		int b=0;
		int k =0;
        int rang=0;
// sum over all elements of the microtubule untill you reach the one where we are cutting. If before our cut add to "first" otherwise add to "second"
		for (deque<Element *>::iterator it = m_body.begin(); it < m_body.end(); it++) 
		{
			if (*it == element) {b=1;k=rang;(*it)->setPrevious(NULL);}
			if (b==0){first.push_back(*it);}
			if (b==1){second.push_back(*it);}
            //if (b==1){b=2;}            
            rang++;
		}
        if (verbose==2){cout << "scission pos "<< k <<endl;}
        deb3("scission 3"); 
		m_body=first; //update existing microtubule to just be "first"
        //if (m_body.size() == 0) cout << "1 vide des la scission"<<endl;
        //if (m_body.size() == 1) cout << "1 un seul element Ã  la scission"<<endl;
        //if (second.size() == 0) cout << "2 vide des la scission"<<endl;
        //if (second.size() == 1) cout << "2 un seul element Ã  la scission"<<endl;
        m_proprietesI["gr_st_plus"]=-1; // now decays from plus end
        deb3("scission 4"); 
        if (second.size() != 0) // if second microtubule non-empty
        {
            Id i=m_microtubulePool->giveMicrotubule(m_elementPool, second); // make "second" a microtubule in our array with Id "i"
// iterate over all elements and update the strucutre for each element
            for (deque<Element *>::iterator it = second.begin(); it < second.end(); it++)
            {
                (*it)->setStructure(m_microtubulePool->getMicrotubule(i));
            }
            m_microtubulePool->getMicrotubule(i)->setAge(m_age); // set age
            m_microtubulePool->getMicrotubule(i)->setPropriete("gr_st_moins", -1); // set microtubule as decaying from its new end. 
        }
        deb3("scission 5"); 
	}
}

double distance(vector<double> pos_point, vector<double> pos_ref, vector<double> vecteur_ref)
{
    vector<double> pos_point2pos_ref;
    pos_point2pos_ref.push_back(pos_ref[0] - pos_point[0]);
    pos_point2pos_ref.push_back(pos_ref[1] - pos_point[1]);
    pos_point2pos_ref.push_back(pos_ref[2] - pos_point[2]);
    double D_pos_point2pos_refFn = pos_point2pos_ref[0] * vecteur_ref[0] + pos_point2pos_ref[1] * vecteur_ref[1] + pos_point2pos_ref[2] * vecteur_ref[2];
    return D_pos_point2pos_refFn;
}



std::deque<Element *> Microtubule::getBody() const
{
    return m_body;    
}

void Microtubule::setBody(std::deque<Element *> body) 
{
    for (size_t i = 0; i<body.size();i++)
    {
        m_body.push_back(body[i]);
        body[i]->setStructure(this);
    }
}

int Microtubule::length() const
{
    if ( m_body.empty() ) { return -1; }
    else { return m_body.size();}
	
}

// Gets the element which is the last element in the microtubule (normally corresponding to the growing pluss end). 
Element *Microtubule::getPlus()
{
    return m_body.back();
}



Element * Microtubule::getPosition2ElementId(int position)
{
    return m_body[position];
}

ElementPool* Microtubule::getElementPool()
{
    return m_elementPool;
}


void Microtubule::setPropriete(string p, double k)					{m_proprietesD[p]=k;}
void Microtubule::setPropriete(string p, int k)     					{m_proprietesI[p]=k;}
double Microtubule::getProprieteD(string p)							{return m_proprietesD[p];}
double Microtubule::getProprieteI(string p)							{return m_proprietesI[p];}
void Microtubule::setAge(int age)                                     {m_age=age;}
int Microtubule::getAge() const                                       {return m_age;}
// Get the size of the microtubule (note:currently only used for testing)
int Microtubule::getSize() const                                       {return m_body.size();}




