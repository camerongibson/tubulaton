//sauve

void Microtubule::grow_plus_contact_mb(Element *plus, std::pair<double, Element*> proche_paroi)
{
        deb("grow_plus_contact_mb");
        deb2("grow_plus_contact_mb");
		int n=40;
		int m=1;
        Shape s = plus->getShape();
        Anchor *a = plus->getAnchor();
        Element *paroi = proche_paroi.second;
        paroi->setPropriete("contact", 1.);
        Shape sp = paroi->getShape();
        Anchor *ap = paroi->getAnchor();
        if ( angleVec(s.getDirection(), sp.getDirection()) > m_proprietesD["Angle_mb"] )
        {
            vector<double> dir;
            dir.push_back(0);
            dir.push_back(0);
            dir.push_back(0);
            //GESTION de l'alea + previous
            //crÃ©ation d'un vecteur alÃ©atoire
            double x = -1.+2*(float)rand()/(float)RAND_MAX;
            double y = -1.+2*(float)rand()/(float)RAND_MAX;
            double z = -1.+2*(float)rand()/(float)RAND_MAX;
            //ajout du vecteur alÃ©atoire au vecteur previous avec pondÃ©ration entre les deux
            dir[0]=n*s.getDirection()[0]  + m*x  ;
            dir[1]=n*s.getDirection()[1]  + m*y  ;
            dir[2]=n*s.getDirection()[2]  + m*z  ;
            //normalisation et pondÃ©ration par une valeur donnant un angle max de 40Â°
            dir=normalise(dir);
            
            
            
            
            dir[0]=dir[0] * m_proprietesD["poids_previous_vect"];
            dir[1]=dir[1] * m_proprietesD["poids_previous_vect"];
            dir[2]=dir[2] * m_proprietesD["poids_previous_vect"];
            
            
            //calcul de la distance c entre le depart et le point oÃ¹ il devrait se situer sur la membrane
            vector<double> vecteurPointMb;
            vecteurPointMb.push_back(*ap->getX() - *a->getX());
            vecteurPointMb.push_back(*ap->getY() - *a->getY());
            vecteurPointMb.push_back(*ap->getZ() - *a->getZ());
            //comme sp est un vecteur unitaire en theorie dptmbortho est bien la distance entre le point et la "membrane" reprÃ©sentÃ©e par le point de rÃ©fÃ©rence utilisÃ©. 
            //en thÃ©orie cette distance est positive puisque le point est cense Ãªtre en dessous, une petite vÃ©rif Ã  enlever est bienvenue
            //VERIFIER
            double dptmbortho = vecteurPointMb[0] * sp.getDirection()[0] + vecteurPointMb[1] * sp.getDirection()[1] + vecteurPointMb[2] * sp.getDirection()[2];
            if (dptmbortho < 0) {cout << "oula le point est au dessus de la membrane?" << endl;}
            //c is the distance along the normal between the point and the theoretical equilibrium.
            double c = dptmbortho - m_proprietesD["d_mb"];
            //le probleme peut survenir si jamais cette valeur est supÃ©rieure Ã  1, en effet on va aboutir Ã  une sorte d'attraction violente de la membrane... La solution la plus simple est pour le moment de se limiter dans les valeurs positives, de telle sorte que cela ne soit pas trop violent vers le haut. 
            if (c > 1) {c=0.99;}
            //evitons les blagues de distances anormales
            if ((c >= -1) & (c <= 1)) {c=0;}
            if (c < -1) {c=-0.99;}
            vector<double> vec_c;
            vec_c.push_back(c * sp.getDirection()[0]);
            vec_c.push_back(c * sp.getDirection()[1]);
            vec_c.push_back(c * sp.getDirection()[2]);
            
            double perp=sqrt(1-c*c);
        
        // A CHANGER
            double scalaire = dir[0] * sp.getDirection()[0] + dir[1] * sp.getDirection()[1] + dir[2] * sp.getDirection()[2];
            dir[0]= dir[0] - scalaire * sp.getDirection()[0] ;
            dir[1]= dir[1] - scalaire * sp.getDirection()[1] ;
            dir[2]= dir[2] - scalaire * sp.getDirection()[2] ;
            //on ramene a une taille de 1
            dir=normalise(dir);
            //on ramene a une taille de perp
            dir[0]=dir[0]*perp;
            dir[1]=dir[1]*perp;
            dir[2]=dir[2]*perp;
            
            Element *nouv=m_elementPool->giveElement();
            nouv->setStructure(this);
            //placement de son id dans le microtubule
            m_body.push_back(nouv);
            //positionnement Ã  la suite de l'Ã©lÃ©ment prÃ©cÃ©dent
            vector<double> pos;
            pos.push_back( *a->getX() + s.getDirection()[0]);
            pos.push_back( *a->getY() + s.getDirection()[1]);
            pos.push_back( *a->getZ() + s.getDirection()[2] );
            Anchor a2(pos[0], pos[1],pos[2]);
            nouv->setAnchor(a2);    
            nouv->setPrevious(plus);
            //ici ce qui gÃ¨re la nouvelle direction

            //on crÃ©Ã© la nouvelle direction et on l'assigne
            Shape s2(dir[0] + vec_c[0], dir[1] + vec_c[1],dir[2] + vec_c[2]);            
            nouv->setShape(s2);
            //cout << c<< endl;
            //cout << *nouv << endl;
            m_elementPool->spaceMtRegister(nouv);
        }
        else//si l'angle est mauvais ->shrink
        {
            m_proprietesI["gr_st_plus"]=-1;
        }
}


//
//  CETTE VERSION A LE SOUCI DE NE PAS PRODUIRE DE VECTEURS DE BONNE TAILLE ; EPIC FAIL
//
void Microtubule::grow_plus_contact_mb(Element *plus, std::pair<double, Element*> proche_paroi)
{
    //TODO (13 05 13) : vÃ©rifier peut Ãªtre la normalitÃ© de tous les vecteurs du contour?
    
    deb("grow_plus_contact_mb");
    deb2("grow_plus_contact_mb");        
    //initialisation des vecteurs, directions et positions
    vector<double> dir_alea;
    vector<double> pos_dir_alea;
    vector<double> pos_dir_alea2pos_mb;
    vector<double> n_perp;
    vector<double> dir_cand;
    
    Shape dir_prev = plus->getShape();
    Anchor *pos_prev = plus->getAnchor();
    
    Element *paroi = proche_paroi.second;
    paroi->setPropriete("contact", 1.);
    Shape n = paroi->getShape();
    Anchor *pos_mb = paroi->getAnchor();
    
    //crÃ©ation du vecteur alÃ©atoire dir_alea
    double x = -1.+2*(float)rand()/(float)RAND_MAX;
    double y = -1.+2*(float)rand()/(float)RAND_MAX;
    double z = -1.+2*(float)rand()/(float)RAND_MAX;    
    int fixe=40;
	int alea=1;
    dir_alea.push_back(fixe*dir_prev.getDirection()[0]  + alea*x);
    dir_alea.push_back(fixe*dir_prev.getDirection()[1]  + alea*y);
    dir_alea.push_back(fixe*dir_prev.getDirection()[2]  + alea*z);
    dir_alea=normalise(dir_alea);//pour le moment on ne pondere pas pas la future definition de l'angle max
    dir_alea[0] = dir_alea[0] * m_proprietesD["poids_previous_vect"];
    dir_alea[1] = dir_alea[1] * m_proprietesD["poids_previous_vect"];
    dir_alea[2] = dir_alea[1] * m_proprietesD["poids_previous_vect"];
    
    //calcul des coordonnees du vecteur n_perp, situÃ© dans le plan de dir_alea,n
    double scalaire = dir_alea[0] * n.getDirection()[0] + dir_alea[1] * n.getDirection()[1] + dir_alea[2] * n.getDirection()[2];
    n_perp.push_back(dir_alea[0] - scalaire * n.getDirection()[0]);
    n_perp.push_back(dir_alea[1] - scalaire * n.getDirection()[1]);
    n_perp.push_back(dir_alea[2] - scalaire * n.getDirection()[2]);
    n_perp=normalise(n_perp);
    
    //TODO : voir si ce calcul vaut la peine
    //calcul des coordonnees du vecteur dir_alea_perp, situÃ© dans le plan de dir_alea,n
    vector<double> dir_alea_perp;
    dir_alea_perp.push_back(n.getDirection()[0] - scalaire * dir_alea[0]);
    dir_alea_perp.push_back(n.getDirection()[1] - scalaire * dir_alea[1]);
    dir_alea_perp.push_back(n.getDirection()[2] - scalaire * dir_alea[2]);
    dir_alea_perp=normalise(dir_alea_perp);
    
    //calcul des coordonnÃ©es du point pos_dir_alea
    pos_dir_alea.push_back(dir_alea[0] + *pos_prev->getX() + dir_prev.getDirection()[0]);
    pos_dir_alea.push_back(dir_alea[1] + *pos_prev->getY() + dir_prev.getDirection()[1]);
    pos_dir_alea.push_back(dir_alea[2] + *pos_prev->getZ() + dir_prev.getDirection()[2]);
    
    //calcul des coordonnÃ©es du vecteur pos_dir_alea2pos_mb
    pos_dir_alea2pos_mb.push_back(pos_dir_alea[0] - *pos_mb->getX());
    pos_dir_alea2pos_mb.push_back(pos_dir_alea[1] - *pos_mb->getY());
    pos_dir_alea2pos_mb.push_back(pos_dir_alea[2] - *pos_mb->getZ());
    
    //calcul de la distance D_pos_dir_alea2pos_mbFn
    double D_pos_dir_alea2pos_mbFn = pos_dir_alea2pos_mb[0] * n.getDirection()[0] + pos_dir_alea2pos_mb[1] * n.getDirection()[1] + pos_dir_alea2pos_mb[2] * n.getDirection()[2];
        //si c'est nÃ©gatif il y a sans doute un souci, mÃªme si une courbure importante peut sans doute l'expliquer
        //TODO(13 05 13) ajouter la verif
    double c = D_pos_dir_alea2pos_mbFn - m_proprietesD["d_mb"];
    
    //calcul des coordonnees de dir_cand
    if ( fabs(c) >= (1-m_proprietesD["poids_previous_vect"]) )
    {
        //cout << "la zone de confort est top loin pour Ãªtre atteinte directement " << endl;
        dir_cand.push_back(c * n.getDirection()[0]);
        dir_cand.push_back(c * n.getDirection()[1]);
        dir_cand.push_back(c * n.getDirection()[2]);        
    }
    else 
    {
        //on va crÃ©er un vecteur de coordonnÃ©es  (c, perp) le long de l'axe sp et n_perp
        double perp=sqrt(1-c*c);
        dir_cand.push_back(c*dir_prev.getDirection()[0] + perp*n_perp[0]);
        dir_cand.push_back(c*dir_prev.getDirection()[1] + perp*n_perp[1]);
        dir_cand.push_back(c*dir_prev.getDirection()[2] + perp*n_perp[2]);
    }
    
    //on verifie si dir_cand est ou pas dans la direction de dir_alea :
    scalaire=( dir_cand[0] * dir_alea[0] + dir_cand[1] * dir_alea[1] + dir_cand[2] * dir_alea[2] );    
    if (scalaire <= 0)
    {
        //ici on est dans le cas oÃ¹ le vecteur produirait un angle trop aigu, l'idÃ©e est alors de l'Ã©changer avec un vecteur perpendiculaire Ã  dir_alea
        //je projette alors ce vecteur sur l'axe
        dir_cand[0] = dir_cand[0] - scalaire * dir_alea[0];
        dir_cand[1] = dir_cand[1] - scalaire * dir_alea[1];
        dir_cand[2] = dir_cand[2] - scalaire * dir_alea[2];
        dir_cand = normalise(dir_cand);
        
    }
    else
    {
        //ici on est dans le cas oÃ¹ le vecteur est ok
        dir_cand = normalise(dir_cand);
    }
    dir_cand[0] = dir_cand[0] * (1- m_proprietesD["poids_previous_vect"]);
    dir_cand[1] = dir_cand[1] * (1- m_proprietesD["poids_previous_vect"]);    
    dir_cand[2] = dir_cand[2] * (1- m_proprietesD["poids_previous_vect"]);
        
    Element *nouv=m_elementPool->giveElement();
    nouv->setStructure(this);
    //placement de son id dans le microtubule
    m_body.push_back(nouv);
    //positionnement Ã  la suite de l'Ã©lÃ©ment prÃ©cÃ©dent
    vector<double> pos;
    pos.push_back( *pos_prev->getX() + dir_prev.getDirection()[0]);
    pos.push_back( *pos_prev->getY() + dir_prev.getDirection()[1]);
    pos.push_back( *pos_prev->getZ() + dir_prev.getDirection()[2] );
    Anchor a2(pos[0], pos[1],pos[2]);
    nouv->setAnchor(a2);    
    nouv->setPrevious(plus);

    //on crÃ©Ã© la nouvelle direction et on l'assigne
    Shape s2(dir_alea[0] + dir_cand[0], dir_alea[1] + dir_cand[1],dir_alea[2] + dir_cand[2]);            
    nouv->setShape(s2);
    //cout << c<< endl;
    //cout << *nouv << endl;
    m_elementPool->spaceMtRegister(nouv);

}





    void MicrotubulePool::evolve(ElementPool *ep)
    {
        //
        // version qui prend en compte un nb maximal de tubuline, qui peuvent etre saturees
        //
        deb("MicrotubulePool::evolve(ElementPool *ep)");
        //srand(time(0)+getpid());
        int k1 = 0;
        int k2 = 0;
        int k3 = 0;
        int k4 = 0;
        //first reading of the generators
        for ( list<pair<Id,Microtubule*> >::iterator it = m_gamma_tub.begin(); it != m_gamma_tub.end(); it++)
        {
            k3++;
            if ( (*it).first > 0 )
            {
                k4++;
                double proba = (float)rand()/(float)RAND_MAX;
                if (proba < m_proba_detachement)
                {
                    (*it).second->setPropriete("gr_st_moins", -1);
                    (*it).first=-1;
                    (*it).second=0;
                    k1++;
                }
            }
            else
            {
                double pos = (float)rand()/(float)RAND_MAX;
                double proba = (float)rand()/(float)RAND_MAX;
                if (proba < m_proba_initiation)
                {   
                    //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                    vector<double> v = pointOnContour(ep->getContour());
                    //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
                    Id i = this->giveMicrotubule(ep, v);
                    Microtubule *m = this->getMicrotubule(i);
                    (*it).first=i;
                    (*it).second=m;
                    k2++;
                }                
            }            
        }
        deb("           evolve microtubulepool 2");
        //cout << "creation : " <<k2<<" separation : "<<k1<<" sur : "<<k4<<" et "<<k3<<endl;
        //if necessary generation of new free microtubules
            //double r = (float)rand()/(float)RAND_MAX;
            //if ( (r < 0.01) && (t>1000) )
            //{
                //Id p=Mpool.giveMicrotubule(&Epool);
            //}
        //reading of the plus ends and movements
        int a=0;int b=0;int c=0;int d=0;double tot=0;int lg=0;
        list<Id> lp = getListMicrotubules();
        deb("           evolve microtubulepool 2b");
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   

                Microtubule *m=getMicrotubule(*it);
                deb("                   evolve microtubulepool 3a");
                //cout << "essai " << *m <<endl;
                m->evolve();
                deb("                   evolve microtubulepool 3b");
  
        }
        deb("           evolve microtubulepool 4");
    }






void Microtubule::grow_plus_contact_mb_mode_tangent(Element *plus, std::pair<double, Element*> proche_paroi)
{
    //TODO (13 05 13) : vÃ©rifier peut Ãªtre la normalitÃ© de tous les vecteurs du contour?
    deb("grow_plus_contact_mb");
    //deb2("grow_plus_contact_mb");        
    //initialisation des vecteurs, directions et positions
    vector<double> pos_dir;
    vector<double> pos_dir2pos_mb;
    vector<double> n_perp;
    vector<double> dir_cand;
    
    Shape Sdir_prev = plus->getShape();
    vector<double> dir_prev;
    dir_prev.push_back(Sdir_prev.getDirection()[0]);
    dir_prev.push_back(Sdir_prev.getDirection()[1]);
    dir_prev.push_back(Sdir_prev.getDirection()[2]);    
    Anchor *pos_prev = plus->getAnchor();
    
    Element *paroi = proche_paroi.second;
    paroi->setPropriete("contact", 1.);
    Shape n = paroi->getShape();
    Anchor *pos_mb = paroi->getAnchor();
    
    vector<double> influence = paroi->getPropriete_v("strain_0");
    //cout << influence.size() <<endl;
    
    //calcul des coordonnees du vecteur dir_prev_perp, situÃ© dans le plan de dir_alea,n
    deb("calcul des coordonnees du vecteur dir_prev_perp, situÃ© dans le plan de dir_prev,n");
    vector<double> dir_prev_perp;
    double scalaire = dir_prev[0] * n.getDirection()[0] + dir_prev[1] * n.getDirection()[1] + dir_prev[2] * n.getDirection()[2];
    dir_prev_perp.push_back(n.getDirection()[0] - scalaire * dir_prev[0]);
    dir_prev_perp.push_back(n.getDirection()[1] - scalaire * dir_prev[1]);
    dir_prev_perp.push_back(n.getDirection()[2] - scalaire * dir_prev[2]);
    dir_prev_perp=normalise(dir_prev_perp);
    
    deb("calcul des coordonnees du vecteur n_perp, situÃ© dans le plan de dir_prev,n");
    n_perp.push_back(dir_prev[0] - scalaire * n.getDirection()[0]);
    n_perp.push_back(dir_prev[1] - scalaire * n.getDirection()[1]);
    n_perp.push_back(dir_prev[2] - scalaire * n.getDirection()[2]);
    n_perp=normalise(n_perp);
    
    deb("calcul des coordonnÃ©es du point pos_dir");
    pos_dir.push_back(*pos_prev->getX() + dir_prev[0]);
    pos_dir.push_back(*pos_prev->getY() + dir_prev[1]);
    pos_dir.push_back(*pos_prev->getZ() + dir_prev[2]);
    
    deb("calcul des coordonnÃ©es du vecteur pos_dir2pos_mb");
    pos_dir2pos_mb.push_back(*pos_mb->getX() - pos_dir[0]);
    pos_dir2pos_mb.push_back(*pos_mb->getY() - pos_dir[1] );
    pos_dir2pos_mb.push_back(*pos_mb->getZ() - pos_dir[2] );
    deb("calcul de la distance D_pos_dir_alea2pos_mbFn");
    double D_pos_dir2pos_mbFn = pos_dir2pos_mb[0] * n.getDirection()[0] + pos_dir2pos_mb[1] * n.getDirection()[1] + pos_dir2pos_mb[2] * n.getDirection()[2];
        //si c'est nÃ©gatif il y a sans doute un souci, mÃªme si une courbure importante peut sans doute l'expliquer
        //TODO(13 05 13) ajouter la verif
        deb2(D_pos_dir2pos_mbFn);

    double D_pos_dir2lim_mbFn = D_pos_dir2pos_mbFn - m_params->getProprieteD("d_mb");
    
    /*
     * 
     *   ici l'ajout de l'alÃ©atoire
     * 
     */

    dir_cand.push_back(dir_prev[0]);
    dir_cand.push_back(dir_prev[1]);
    dir_cand.push_back(dir_prev[2]);
    dir_cand=normalise(dir_cand);
    deb("ajout du facteur aleatoire");
    double x = -1.+2*(float)rand()/(float)RAND_MAX;
    double y = -1.+2*(float)rand()/(float)RAND_MAX;
    double z = -1.+2*(float)rand()/(float)RAND_MAX;    
    int fixe=m_params->getProprieteD("part_alea_fixe");
	int alea=m_params->getProprieteD("part_alea_alea");
    dir_cand[0]=fixe*dir_cand[0]  + alea*x;
    dir_cand[1]=fixe*dir_cand[1]  + alea*y;
    dir_cand[2]=fixe*dir_cand[2]  + alea*z;
    dir_cand=normalise(dir_cand);//pour le moment on ne pondere pas pas la future definition de l'angle max
    double a = angleVec(dir_prev, n_perp);
    /*
     * 
     *   ici l'influence Ã©ventuelle de la membrane
     * 
     */
    if ( (D_pos_dir2lim_mbFn <= 0) )
    {
        if (fabs(a) < m_params->getProprieteD("Angle_mb_tangent"))
        {
            double scalaire = dir_cand[0] * n.getDirection()[0] + dir_cand[1] * n.getDirection()[1] + dir_cand[2] * n.getDirection()[2];
            dir_cand[0]=dir_cand[0] - scalaire * n.getDirection()[0];
            dir_cand[1]=dir_cand[1] - scalaire * n.getDirection()[1];
            dir_cand[2]=dir_cand[2] - scalaire * n.getDirection()[2];
            dir_cand=normalise(dir_cand);
        }
        else
        {
            m_proprietesI["gr_st_plus"]=m_params->getProprieteI("decision_rencontre");
        }
    }
    /*
     * 
     *   ici l'influence Ã©ventuelle de vecteurs de direction 
     * 
     */
    if ((influence.size() == 3) & (m_microtubulePool->getInfluence()>0) & (D_pos_dir2lim_mbFn <= 0))
    {
        deb("utilisation de l'influence");
        double f1 = m_microtubulePool->getInfluence();
        double f2 = 1;
        //cout << "utilisation de l'influence" <<endl;
        double sc_infl = influence[0]*n.getDirection()[0] + influence[1]*n.getDirection()[1] + influence[2]*n.getDirection()[2];
        vector<double > influence_v;
        influence_v.push_back(influence[0] - sc_infl * n.getDirection()[0]);
        influence_v.push_back(influence[1] - sc_infl * n.getDirection()[1]);
        influence_v.push_back(influence[2] - sc_infl * n.getDirection()[2]);
        double sens = influence_v[0]*dir_cand[0] + influence_v[1]*dir_cand[1] + influence_v[2]*dir_cand[2];
        if (sens >0)
        {
            dir_cand[0] = f1*dir_cand[0] + f2*influence_v[0];
            dir_cand[1] = f1*dir_cand[1] + f2*influence_v[1];
            dir_cand[2] = f1*dir_cand[2] + f2*influence_v[2];
            dir_cand=normalise(dir_cand);
        }
        else
        {
            dir_cand[0] = f1*dir_cand[0] - f2*influence_v[0];
            dir_cand[1] = f1*dir_cand[1] - f2*influence_v[1];
            dir_cand[2] = f1*dir_cand[2] - f2*influence_v[2];
            dir_cand=normalise(dir_cand);
        }
    }


    deb("creation de l'element");
    Element *nouv=m_elementPool->giveElement();
    nouv->setStructure(this);
    //placement de son id dans le microtubule
    m_body.push_back(nouv);
    //positionnement Ã  la suite de l'Ã©lÃ©ment prÃ©cÃ©dent
    deb("creation de l'element Anchor");
    //ici la condition "tangente" ne suffisait pas, car cela conduisait dans les courbes Ã  peu Ã  peu s'Ã©loigner de l'Ã©quilibre. Je rajoute donc une condition stricte sur la distance Ã  la membrane
    //TODO : revoir si ce n'est pas trop drastique.
    if (D_pos_dir2lim_mbFn <= 0)
    {
        pos_dir[0] = pos_dir[0] - D_pos_dir2lim_mbFn * n.getDirection()[0];
        pos_dir[1] = pos_dir[1] - D_pos_dir2lim_mbFn * n.getDirection()[1];
        pos_dir[2] = pos_dir[2] - D_pos_dir2lim_mbFn * n.getDirection()[2];
    }
    
    Anchor a2(pos_dir[0], pos_dir[1],pos_dir[2]);
    nouv->setAnchor(a2);    
    nouv->setPrevious(plus);

    deb("creation de l'element Shape");
    //on crÃ©Ã© la nouvelle direction et on l'assigne
    Shape s2(dir_cand[0], dir_cand[1],dir_cand[2]);            
    nouv->setShape(s2);
    //cout << c<< endl;
    //cout << *nouv << endl;
    m_elementPool->spaceMtRegister(nouv);
    nouv->setPropriete("nearest_contour",paroi->getId());
    nouv->setPropriete("nearest_contour_d",proche_paroi.first);
    nouv->setPropriete("D_pos_dir2lim_mbFn",D_pos_dir2lim_mbFn);
    deb("FIN grow_plus_contact_mb");
}
