//classe MicrotubulePool
//non si element.h deja importe ailleurs
#ifndef __MICROTUBULEPOOL_H_INCLUDED__
#define __MICROTUBULEPOOL_H_INCLUDED__
#include <iostream>
#include "Microtubule.hpp"
#include <unordered_map>
#include <deque>
#include <list>
#include <vector>
#include <utility>

typedef int Id;
class Parametres;


class MicrotubulePool
{
    friend std::ostream& operator<<(std::ostream& os, MicrotubulePool& p);
    friend std::ofstream& operator<<(std::ofstream& os, MicrotubulePool& p);
    public:
        MicrotubulePool();
        MicrotubulePool(Parametres *params, int nb_gamma_tub, ElementPool *ep);
        //MicrotubulePool(Parametres *params, int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep);
        MicrotubulePool(Parametres *params, ElementPool *ep);
        ~MicrotubulePool();
        void initiate();
        Id giveMicrotubule(ElementPool *ep);        
        Id giveMicrotubule(ElementPool *ep, std::deque<Element *> body);
        Id giveMicrotubule(ElementPool *ep, std::vector<double> vec, int cont_num);
        Microtubule* getMicrotubule(Id id);
        int getInfluence();
        void erase(Id id);
        std::list<Id> getListMicrotubules();
        int write(std::string filename);
        std::vector<int> getMicrotubuleLengths();

        void evolve(ElementPool *ep);
        
        //a ne pas utiliser
        std::unordered_map<Id, Microtubule> *getMicrotubules();
        
        int destruction_microtubule;
        int creation_microtubule;
        int cut_microtubule;
	int override_cut_microtubule;    
        int detachements;
	int shrinkages;

        
    private:
        void InitiateMidway(ElementPool *ep, double proba_initation, int cont_num);

        std::unordered_map<Id, Microtubule> m_microtubules;
        Id maxId();
        Id m_maxId;
        int m_nb_gamma_tub;
        int m_obj_num;
        double m_proba_detachement;
	double m_proba_shrink;
	double m_proba_crossmicro_cut;

        ElementPool *m_ep;

        //double m_proba_initiation;
        double m_proba_tocut;
        int m_influence;
        std::list< std::pair<Id,Microtubule*> > m_gamma_tub;
        Parametres *m_params;
        
        
        
};
#endif
