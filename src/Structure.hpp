//classe mere des structures
#ifndef __STRUCTURE_H_INCLUDED__
#define __STRUCTURE_H_INCLUDED__
#include <iostream>

typedef int Id;
class Element;
class Structure
{    
    public:
        Structure();
        ~Structure();
        virtual std::ostream& Print( std::ostream &os) const;
        virtual Id getId() const;
        virtual int getType() const;
        virtual void setId(Id id);
        
        virtual void scission(Element *element);
        

    protected:
        Id m_id;
        int m_type;
};
#endif
