//g++ -c draw_scene.cpp -I/usr/include/vtk-5.8
//g++ *.o -lvtkCommon -lvtkHybrid -lvtkGenericFiltering -lvtkVolumeRendering -lvtksys -lvtkFiltering -lvtkRendering -lvtkIO -lvtkGraphics -lvtkexoIIc  -lvtkImaging -lvtkftgl  -lvtkDICOMParser  -o prog
#include "vtkPolyDataReader.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkPointData.h"
#include "vtkFloatArray.h"
#include "vtkLongArray.h"
#include "vtkIntArray.h"
#include "vtkPolyData.h"
#include "vtkPolyDataWriter.h"
#include "vtkSmartPointer.h"

#include "MicrotubulePool.hpp"
#include "ElementPool.hpp"
#include "Microtubule.hpp"
#include "Element.hpp"
#include "drawScene.hpp"
#include "Contour.hpp"
#include <sstream>
#include <cmath>

using namespace std;

#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

void drawScene::pool2vtk(string s_filename, ElementPool* ep, MicrotubulePool* mp, int detail, int temps)
{
	//vtkSmartPointer<vtkPolyDataReader> contour = vtkSmartPointer<vtkPolyDataReader>::New();
	//contour->SetFileName("contour.vtk");
	//contour->Update();
	//
	//vtkSmartPointer<vtkTransform> translation =	vtkSmartPointer<vtkTransform>::New();
	//translation->Translate(250.0, 250.0, 250.0);
	//translation->Scale(500.0, 500.0, 500.0);
	//
	//
	//vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	//transformFilter->SetInput(contour->GetOutput());
	//transformFilter->SetTransform(translation);
	//transformFilter->Update();
	//
	//
	//
	// crÃ©er un flux de sortie
    ostringstream oss;
    // Ã©crire un nombre dans le flux
    oss << temps;
    // rÃ©cupÃ©rer une chaÃ®ne de caractÃ¨res
    string result = oss.str();
    s_filename+=result+".vtk";
    ofstream myfile;
    char *filename = (char*)s_filename.c_str();
    myfile.open(filename);
    
    vtkPolyData *total = vtkPolyData::New();
    vtkPoints *points = vtkPoints::New();
    vtkFloatArray *directions = vtkFloatArray::New();
    directions->SetNumberOfComponents(3);
    vtkIntArray *identite = vtkIntArray::New();
    identite->SetName("identite");
    vtkIntArray *contact = vtkIntArray::New();
    contact->SetName("contact");
    //vtkLongArray *space = vtkLongArray::New();
    //space->SetName("space");
    //vtkLongArray *space2 = vtkLongArray::New();
    //space2->SetName("space2");
    vtkIntArray *type = vtkIntArray::New();
    type->SetName("type");

    vtkIntArray *cortical = vtkIntArray::New();
    cortical->SetName("cortical");
    
    vtkIntArray *id_vtk = vtkIntArray::New();
    id_vtk->SetName("id_vtk");
    
    vtkIntArray *to_cut = vtkIntArray::New();
    to_cut->SetName("to_cut");

    vtkIntArray *structure_num = vtkIntArray::New();  // number of the structure it is adjacent to if it is adjacen to any
    structure_num->SetName("structure_num");
    //
    vtkFloatArray *nearest_contour_d = vtkFloatArray::New();
    nearest_contour_d->SetName("nearest_contour_d");
    
    vtkFloatArray *nearest_contour_d2 = vtkFloatArray::New();
    nearest_contour_d2->SetName("nearest_contour_d2");
    //vtkLongArray *nearest_contour = vtkLongArray::New();
    //nearest_contour->SetName("nearest_contour");
    
    vtkFloatArray *nearest_contour_vtk = vtkFloatArray::New();
    nearest_contour_vtk->SetName("nearest_contour_vtk");
    
    int i=0;
    deb("drawScene::pool2vtk | ep->getListElements()");
    list<Element *> lm_provi= ep->getListElements();
    Element *el;
    for (list<Element *>::iterator it = lm_provi.begin();it != lm_provi.end();it++)
    {
        deb("drawScene::pool2vtk | (detail == 0)");
		if (detail == 0)
		{
            deb("drawScene::pool2vtk | el = *it;");
			el = *it;
            deb("drawScene::pool2vtk | el->isAlive()");
            if (el->isAlive()){
                //cout << *el << endl;
                deb("drawScene::pool2vtk | (detail == 0) | getAnchor()");
                Anchor* pos=el->getAnchor();
                float pos_[3] = {(float)*(pos->getX()), (float)*(pos->getY()), (float)*(pos->getZ())};
                vector<double> dir=el->getShape().getDirection();
                float dir_[3] = {(float)dir[0], (float)dir[1], (float)dir[2]};
                deb("drawScene::pool2vtk | (detail == 0) | getPositionContour(*it)");
//                 long c=ep->getPositionContour(*it);
//                 long c2=ep->getPositionMt(*it);
                //cout << *it<<"\t"<<pos_[0]<<"\t"<<pos_[1]<<"\t"<<pos_[2] <<"\t"<<spa[0]<<"\t"<<spa[1]<<"\t"<<spa[2]<<"\t"<< c <<endl;
                deb("drawScene::pool2vtk | (detail == 0) | InsertPoint");
                points->InsertPoint(i,pos_);
#if defined VTK_VERSION_7_1PLUS
		directions->InsertTypedTuple(i,dir_); //vtk 7.1 update             
#else
                directions->InsertTupleValue(i,dir_); //deprecated vtk 7.1
#endif
                deb("drawScene::pool2vtk | (detail == 0) | el->getStructureType()");
                //cout << (int)(el->getStructureType()) << endl;
                deb("drawScene::pool2vtk | (detail == 0) | el->getStructure()->getId())");
                identite->InsertValue(i,(int)(el->getStructureId()));
                deb("drawScene::pool2vtk | (detail == 0) | (int)(el->getPropriete('contact')");
                //space->InsertValue(i,c);
                //space2->InsertValue(i,c2);
                contact->InsertValue(i, (int)(el->getPropriete("contact")));
                type->InsertValue(i, (int)(el->getStructureType()));
                //nearest_contour->InsertValue(i, (int)(el->getPropriete("nearest_contour")));
                nearest_contour_d->InsertValue(i, (float)(el->getPropriete("nearest_contour_d")));
                nearest_contour_d2->InsertValue(i, (float)(el->getPropriete("D_pos_dir2lim_mbFn")));
                nearest_contour_vtk->InsertValue(i, (float)(el->getPropriete("nearest_contour_vtk")));

                to_cut->InsertValue(i, (el->getPropriete("to_cut")));
                id_vtk->InsertValue(i, (el->getId_vtk()));
                cortical->InsertValue(i, (el->getPropriete("cortical")));
                structure_num->InsertValue(i, (int)(el->getPropriete("Cortical_Structure")));
                i++;
            }
            
		}
		else
		{
            deb("drawScene::pool2vtk | else");
			el = *it;
            if (el->isAlive()){
                Anchor* pos=el->getAnchor();
                float pos_[3] = {(float)*(pos->getX()), (float)*(pos->getY()), (float)*(pos->getZ())};
                //deb("toto 1c");
                vector<double> dir=el->getShape().getDirection();
                float dir_[3] = {(float)dir[0], (float)dir[1], (float)dir[2]};
                //cout << *it <<"\t"<<el->getStructure()<<"\t"<< pos[0] <<endl;
                deb("toto 1b");
//                 long c=ep->getPositionContour(*it);
//                 long c2=ep->getPositionMt(*it);
                //cout << *it<<"\t"<<pos_[0]<<"\t"<<pos_[1]<<"\t"<<pos_[2] <<"\t"<<spa[0]<<"\t"<<spa[1]<<"\t"<<spa[2]<<"\t"<< c <<endl;
                //points->InsertPoint(i,pos_);
                //directions->InsertTupleValue(i,dir_);
                //identite->InsertValue(i,(int)(el->getStructure()));
                if ( (int)(el->getStructureType()) == 1)
                {
                    points->InsertPoint(i,pos_);
#if defined VTK_VERSION_7_1PLUS
		    directions->InsertTypedTuple(i,dir_); //vtk 7.1 update             
#else
                    directions->InsertTupleValue(i,dir_); //deprecated vtk 7.1
#endif
                    identite->InsertValue(i,(int)(el->getStructureId()));
                    //space->InsertValue(i,c);
                    //space2->InsertValue(i,c2);
                    contact->InsertValue(i, (int)(el->getPropriete("contact")));
                    type->InsertValue(i, el->getStructureType());
                    
                    //nearest_contour->InsertValue(i, (int)(el->getPropriete("nearest_contour")));
                    nearest_contour_d->InsertValue(i, (float)(el->getPropriete("nearest_contour_d")));
                    nearest_contour_d2->InsertValue(i, (float)(el->getPropriete("D_pos_dir2lim_mbFn")));
                    nearest_contour_vtk->InsertValue(i, (float)(el->getPropriete("nearest_contour_vtk")));
                    to_cut->InsertValue(i, (el->getPropriete("to_cut")));
                    id_vtk->InsertValue(i, (el->getId_vtk()));
                    cortical->InsertValue(i, (el->getPropriete("cortical")));
                    structure_num->InsertValue(i, (int)(el->getPropriete("Cortical_Structure")));
                    i++;
                }
            }
		}

    }
    deb("toto 1c");
    //cout<<"draw2"<<endl;
    
    total->SetPoints(points);
    points->Delete();    
    total->GetPointData()->SetScalars(directions);
    directions->Delete();    
    total->GetPointData()->AddArray(identite);
    identite->Delete();        
    total->GetPointData()->AddArray(type);
    type->Delete();
    
    
    //total->GetPointData()->AddArray(space);
    //space->Delete();
    //total->GetPointData()->AddArray(space2);
    //space2->Delete();
    total->GetPointData()->AddArray(contact);
    contact->Delete();
    total->GetPointData()->AddArray(to_cut);
    to_cut->Delete();
    
    total->GetPointData()->AddArray(id_vtk);
    id_vtk->Delete();
    
    //total->GetPointData()->AddArray(nearest_contour);
    //nearest_contour->Delete();
    
    total->GetPointData()->AddArray(nearest_contour_d);
    nearest_contour_d->Delete();
    
    total->GetPointData()->AddArray(nearest_contour_vtk);
    nearest_contour_vtk->Delete();
    
    
    total->GetPointData()->AddArray(nearest_contour_d2);
    nearest_contour_d2->Delete();
    
    total->GetPointData()->AddArray(cortical);
    cortical->Delete();
    
    total->GetPointData()->AddArray(structure_num);
    structure_num->Delete();   
    
    
    vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
//     writer->SetInput(total);
    writer->SetInputData(total);
    //cout<<"draw3"<<endl;
    deb("toto 2");
    writer->SetFileName(filename);
    writer->SetFileTypeToBinary();
    writer->Write();
    
    //writer = vtkSmartPointer<vtkPolyDataWriter>::New();
    //writer->SetInput(transformFilter->GetOutput());
    //cout<<"draw3"<<endl;
    //writer->SetFileName("contourAdapte.vtk");
    //writer->Write();
    
    myfile.close();
}

void drawScene::Contour2vtk(string s_filename, ElementPool* ep, MicrotubulePool* mp, Parametres* params, int temps)
{

    deb("drawScene::pool2vtk | ep->getListElements()");
    list<Element *> lm_provi= ep->getListElements();
    Element *el;
    
    // Prints out the contours at every point that the mesh is output. 
    
    //Calculate length dependent forces depending on microtubules
    list<Id> lp = mp->getListMicrotubules();
    for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
    {   
    //cout<<"Microtubule: "<<(*it)<<endl;
    Microtubule* m=mp->getMicrotubule(*it);
    std::deque<Element *> Elem=m->getBody();
    Element* OldElem;
    float CouFront=0;
    int Initial=0;
    float Num;   
    
    
    Element* OldElem_ForceDirect;
    int CortStruc=0;
    //int Warning=0;
    for (std::deque<Element *>::iterator it = Elem.begin(); it!=Elem.end(); it++){
        //cout<<"Structure Number: "<< (*it)->getPropriete("Cortical_Structure")<<endl;
        int CortStruc_Current=(*it)->getPropriete("Cortical_Structure");
        //NOTE: if (CortStruc_Current<0 && Warning==0){cout<<"Warning: Detecting MTs with negative structure value. Force_pushing point data may be inaccurate."<<endl;Warning=1;}
        if (CortStruc_Current>0){
            if (CortStruc==0){CortStruc=CortStruc_Current; OldElem_ForceDirect=(*it);}
            else {
                if (CortStruc!=CortStruc_Current){
                    (OldElem_ForceDirect)->setPropriete("Forces_Pushing",(OldElem_ForceDirect)->getPropriete("Forces_Pushing")-1);
                    (*it)->setPropriete("Forces_Pushing",(*it)->getPropriete("Forces_Pushing")+1);
                }
                OldElem_ForceDirect=(*it);
                CortStruc=CortStruc_Current; 
            }
        }
        //if((*it)->getPropriete("cortical")==1){
        if (CortStruc_Current>0){
            if (Initial==0){
                  Num=(*it)->getPropriete("Forces_Length");
                  Num=Num+CouFront;
                  (*it)->setPropriete("Forces_Length",Num);
                  Initial=1;
                  }
              else {
                  Num=(*it)->getPropriete("Forces_Length");
                  Num=Num+CouFront/2;
                  (*it)->setPropriete("Forces_Length",Num);
                  Num=OldElem->getPropriete("Forces_Length");
                  Num=Num+CouFront/2;
                  OldElem->setPropriete("Forces_Length",Num);
                  }
              CouFront=1;
              OldElem= (*it);}
        else{CouFront=CouFront+1;}
   	    }
        if (Initial!=0){
           Num=OldElem->getPropriete("Forces_Length");
           Num=Num+CouFront;
           OldElem->setPropriete("Forces_Length",Num);
           }
	  }	
    int i=0;

    pair<double, Element*> min1;
    float Temp_num;
    float Temp_num_direc_Push;
    float Temp_num_direc;
    for (list<Element *>::iterator it = lm_provi.begin();it != lm_provi.end();it++)
        {
        el = *it;
            if (el->isAlive() && ((int)(el->getStructureType()) == 1))
            {
            vector<double> dir1=el->getShape().getDirection();   //dir1 is a unit vector
            ep->NN_All_Contour(el,1);
            min1=ep->NN_Contour(el);
            if ((min1.first!=-1) && (min1.first<100)) {
                Temp_num=min1.second->getPropriete("Forces_NN")+1;
                min1.second->setPropriete("Forces_NN",Temp_num);
                vector<double> dir2=min1.second->getShape().getDirection();  //dir2 is a unit vector
                //float force=(dir1[0]*dir2[0]+dir1[1]*dir2[1]+dir1[2]*dir2[2]); //force on the contour due to the microtubule proportional to the component of the microtubule perpendicular to contour
                Temp_num_direc=min1.second->getPropriete("Forces_NN_Direction")+(dir1[0]*dir2[0]+dir1[1]*dir2[1]+dir1[2]*dir2[2]);
                min1.second->setPropriete("Forces_NN_Direction",Temp_num_direc);
                Temp_num=min1.second->getPropriete("Forces_Length_Allx")+el->getPropriete("Forces_Length")*dir1[0];
                min1.second->setPropriete("Forces_Length_Allx",Temp_num);
                Temp_num=min1.second->getPropriete("Forces_Length_Ally")+el->getPropriete("Forces_Length")*dir1[1];
                min1.second->setPropriete("Forces_Length_Ally",Temp_num);
                Temp_num=min1.second->getPropriete("Forces_Length_Allz")+el->getPropriete("Forces_Length")*dir1[2];
                min1.second->setPropriete("Forces_Length_Allz",Temp_num);  
                //if ((el->getPropriete("Forces_Length"))>0.000001){
                //cout<<"Direction: " << dir1[0] << dir1[1] << dir1[2] <<endl; 
                //cout<< "Forces Length: "<< el->getPropriete("Forces_Length") <<endl;}
                Temp_num_direc_Push=min1.second->getPropriete("Forces_PushingX")+el->getPropriete("Forces_Pushing")*dir1[0];
                min1.second->setPropriete("Forces_PushingX",Temp_num_direc_Push); 
                Temp_num_direc_Push=min1.second->getPropriete("Forces_PushingY")+el->getPropriete("Forces_Pushing")*dir1[1];
                min1.second->setPropriete("Forces_PushingY",Temp_num_direc_Push); 
                Temp_num_direc_Push=min1.second->getPropriete("Forces_PushingZ")+el->getPropriete("Forces_Pushing")*dir1[2];
                min1.second->setPropriete("Forces_PushingZ",Temp_num_direc_Push);      
                Temp_num=min1.second->getPropriete("Forces_Pushing_All")+el->getPropriete("Forces_Pushing");
                min1.second->setPropriete("Forces_Pushing_All",Temp_num);
                        
                //cout <<force<<endl;
            }            
            i++;
            }
        el->setPropriete("Forces_Length",0);  
        el->setPropriete("Forces_Pushing",0);          
        }


    cout<<"Number of alive elements: "<<i<<endl;




    int detail=params->getProprieteI("details");
    // Prints out the contours at every point that the mesh is output. 
    string nom;
    for (int j=0; j<params->getProprieteI("num_input_vtk"); j++) 
    {	if (j==0){nom="Output_Contour";}
        else {nom="Output_Contour_"+std::to_string(j+1);}
        if (params->getProprieteI(nom)!=0) 
	{
                string s_filename_full;
		Contour* con=ep->getContour();
	    	std::deque<Element *> El_All=con->GetContourElements(j);
	        ostringstream oss;
	        // Ã©crire un nombre dans le flux
	        oss << "_Cont_";
	        oss << (j+1);
	        oss << "_";
	        oss << temps;
	        // rÃ©cupÃ©rer une chaÃ®ne de caractÃ¨res
	        string result = oss.str();
	        s_filename_full=s_filename;
	        s_filename_full+=result+".vtk";
	        ofstream myfile;
	        char *filename = (char*)s_filename_full.c_str();
	        myfile.open(filename);
        
	        vtkPolyData *total = vtkPolyData::New();
        	vtkPoints *points = vtkPoints::New();
        	vtkFloatArray *directions = vtkFloatArray::New();
        	directions->SetNumberOfComponents(3);
        	vtkIntArray *identite = vtkIntArray::New();
   
	        //    vtkIntArray *to_cut = vtkIntArray::New();
	        //    to_cut->SetName("to_cut");

	        vtkIntArray *ForcesAll = vtkIntArray::New();
        	ForcesAll->SetName("Forces");
	        vtkIntArray *ForcesClose = vtkIntArray::New();
        	ForcesClose->SetName("Forces_NN");
	        vtkIntArray *ForcesCloseDirec = vtkIntArray::New();
        	ForcesCloseDirec->SetName("Forces_NN_Direction");
	        vtkFloatArray *ForcesLengthAllX = vtkFloatArray::New();
        	ForcesLengthAllX->SetName("ForcesLengthX");
	        vtkFloatArray *ForcesLengthAllY = vtkFloatArray::New();
        	ForcesLengthAllY->SetName("ForcesLengthY");
	        vtkFloatArray *ForcesLengthAllZ = vtkFloatArray::New();
        	ForcesLengthAllZ->SetName("ForcesLengthZ");
	        vtkFloatArray *Forces_PushingX_All = vtkFloatArray::New();
        	Forces_PushingX_All->SetName("Forces_PushingX_All");
	        vtkFloatArray *Forces_PushingY_All = vtkFloatArray::New();
        	Forces_PushingY_All->SetName("Forces_PushingY_All");
	        vtkFloatArray *Forces_PushingZ_All = vtkFloatArray::New();
        	Forces_PushingZ_All->SetName("Forces_PushingZ_All");
	        vtkIntArray *Forces_Pushing_AllAll = vtkIntArray::New();
        	Forces_Pushing_AllAll->SetName("Forces_Pushing_AllAll");
        	int i=0;
        	deb("drawScene::Contour2vtk | ep->getListElements()");
        	list<Element *> lm_provi= ep->getListElements();
        	Element *el;
        	for (auto it = El_All.cbegin();it != El_All.cend();it++)
        	{
        	    deb("drawScene::Contour2vtk | (detail == 0)");
        	    deb("drawScene::Contour2vtk | el = *it;");
        	    el = *it;
             if (el->isAlive()){
        	    	deb("drawScene::Contour2vtk | el->isAlive()");
        	    	//cout << *el << endl;
        	    	deb("drawScene::Contour2vtk | (detail == 0) | getAnchor()");
        	    	Anchor* pos=el->getAnchor();
        	    	float pos_[3] = {(float)*(pos->getX()), (float)*(pos->getY()), (float)*(pos->getZ())};
        	    	vector<double> dir=el->getShape().getDirection();
        	    	float dir_[3] = {(float)dir[0], (float)dir[1], (float)dir[2]};
        	    	deb("drawScene::Contour2vtk | (detail == 0) | getPositionContour(*it)");
   	 //                 long c=ep->getPositionContour(*it);
   	 //                 long c2=ep->getPositionMt(*it);
        	    	//cout << *it<<"\t"<<pos_[0]<<"\t"<<pos_[1]<<"\t"<<pos_[2] <<"\t"<<spa[0]<<"\t"<<spa[1]<<"\t"<<spa[2]<<"\t"<< c <<endl;
        	    	deb("drawScene::Contour2vtk | (detail == 0) | InsertPoint");
        	    	points->InsertPoint(i,pos_);
#if defined VTK_VERSION_7_1PLUS
		    	directions->InsertTypedTuple(i,dir_); //vtk 7.1 update             
#else
        	    	directions->InsertTupleValue(i,dir_); //deprecated vtk 7.1
#endif
        	    	deb("drawScene::Contour2vtk | (detail == 0) | el->getStructureType()");
        	    	//cout << (int)(el->getStructureType()) << endl;
        	    	deb("drawScene::Contour2vtk | (detail == 0) | el->getStructure()->getId())");
     	
        	    	deb("drawScene::Contour2vtk | (detail == 0) | (int)(el->getPropriete('contact')");
        	    	//space->InsertValue(i,c);
        	    	//space2->InsertValue(i,c2);
        	    	//to_cut->InsertValue(i, (el->getPropriete("to_cut")));
                    	//if (el->getPropriete("Forces")>0){cout<<el->getPropriete("Forces")<<endl;}
        	    	ForcesAll->InsertValue(i, (int)(el->getPropriete("Forces")));
        	    	ForcesClose->InsertValue(i, (int)(el->getPropriete("Forces_NN")));
        	    	ForcesCloseDirec->InsertValue(i, (int)(el->getPropriete("Forces_NN_Direction")));
                ForcesLengthAllX->InsertValue(i, (float)(el->getPropriete("Forces_Length_Allx")));
                ForcesLengthAllY->InsertValue(i, (float)(el->getPropriete("Forces_Length_Ally")));
                ForcesLengthAllZ->InsertValue(i, (float)(el->getPropriete("Forces_Length_Allz")));
                Forces_PushingX_All->InsertValue(i, (el->getPropriete("Forces_PushingX")));
                Forces_PushingY_All->InsertValue(i, (el->getPropriete("Forces_PushingY")));
                Forces_PushingZ_All->InsertValue(i, (el->getPropriete("Forces_PushingZ")));
                Forces_Pushing_AllAll->InsertValue(i, (int)(el->getPropriete("Forces_Pushing_All")));
                 
      
                //if ((el->getPropriete("Forces_Length_Allx")) !=0){
                //    cout << "Forces Length: " << (el->getPropriete("Forces_Length_Allx")) <<endl;
                //    cout << "Forces Pushing All: " << (el->getPropriete("Forces_Pushing_All")) <<endl;
                //    cout << "Forces Length in vtk: " << ForcesLengthAllX->GetValue(i) << endl;}
                
             	  el->setPropriete("Forces",0);
             	  el->setPropriete("Forces_NN",0);
             	  el->setPropriete("Forces_NN_Direction",0);
                el->setPropriete("Forces_Length_Allx",0);
                el->setPropriete("Forces_Length_Ally",0);
                el->setPropriete("Forces_Length_Allz",0);
                el->setPropriete("Forces_PushingX",0);
                el->setPropriete("Forces_PushingY",0);
                el->setPropriete("Forces_PushingZ",0);
                el->setPropriete("Forces_Pushing_All",0);
        	    	i++;
                    }
                    
        	}
         
 	        deb("toto 1c");
    	
        	total->SetPoints(points);
        	points->Delete();    
        	total->GetPointData()->SetScalars(directions);    
        	directions->Delete(); 
        	total->GetPointData()->AddArray(ForcesAll);
        	ForcesAll->Delete(); 
        	total->GetPointData()->AddArray(ForcesClose);
        	ForcesClose->Delete();    
        	total->GetPointData()->AddArray(ForcesCloseDirec);
        	ForcesCloseDirec->Delete();
        	total->GetPointData()->AddArray(ForcesLengthAllX);
        	ForcesLengthAllX->Delete();    
        	total->GetPointData()->AddArray(ForcesLengthAllY);
        	ForcesLengthAllY->Delete();  
        	total->GetPointData()->AddArray(ForcesLengthAllZ);
        	ForcesLengthAllZ->Delete();  
        	total->GetPointData()->AddArray(Forces_PushingX_All);
        	Forces_PushingX_All->Delete();  
        	total->GetPointData()->AddArray(Forces_PushingY_All);
        	Forces_PushingY_All->Delete();  
        	total->GetPointData()->AddArray(Forces_PushingZ_All);
        	Forces_PushingZ_All->Delete();  
        	total->GetPointData()->AddArray(Forces_Pushing_AllAll);
        	Forces_Pushing_AllAll->Delete();       
     
        	vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  	  	//writer->SetInput(total);
        	writer->SetInputData(total);
        	//cout<<"draw3"<<endl;
        	deb("toto 2");
        	writer->SetFileName(filename);
        	writer->SetFileTypeToBinary();
        	writer->Write();
    
        	//writer = vtkSmartPointer<vtkPolyDataWriter>::New();
       		//writer->SetInput(transformFilter->GetOutput());
        	//cout<<"draw3"<<endl;
	        //writer->SetFileName("contourAdapte.vtk");
        	//writer->Write();
    
        	myfile.close();
          oss.clear();
	}
    }
}
