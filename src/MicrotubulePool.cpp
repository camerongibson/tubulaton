//elementPool
#include "MicrotubulePool.hpp"
#include "Microtubule.hpp"
#include "ElementPool.hpp"
#include <vector>
#include <deque>
#include <list>
#include <utility>
#include "utility.hpp"
#include <fstream>
#define verbose 0
#define deb(x) if (verbose>=1){cout << x << endl;}
#define deb2(x) if (verbose>=2){cout << x << endl;}
#define deb3(x) if (verbose>=3){cout << x << endl;}
#define deb4(x) if (verbose>=4){cout << x << endl;}


using namespace std;


    std::ostream& operator<<(std::ostream& os, MicrotubulePool& p)
    {
        cout << p.maxId()<<endl;
        for (unordered_map<Id, Microtubule>::iterator it = p.getMicrotubules()->begin();it != p.getMicrotubules()->end();++it)
        {
            os << "id "<< it->first <<" microtubule "<<it->second<<endl;
        }
        return os;
    }
 
     std::ofstream& operator<<(std::ofstream& os, MicrotubulePool& p)
    {
        cout << p.maxId()<<endl;
        for (unordered_map<Id, Microtubule>::iterator it = p.getMicrotubules()->begin();it != p.getMicrotubules()->end();++it)
        {
            os << "id "<< it->first <<" microtubule "<<it->second<<endl;
        }
        return os;
    }
    
    MicrotubulePool::MicrotubulePool()
    {
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version () :"<<endl;}
        m_maxId=1;
    }
    
    MicrotubulePool::MicrotubulePool(Parametres *params, int nb_gamma_tub, ElementPool *ep)
    {
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, ElementPool *ep):"<<endl;}
//         int m_influence = 0;
        m_maxId=1;
        m_nb_gamma_tub=nb_gamma_tub;
        m_params = params;
        
        destruction_microtubule = 0;
        creation_microtubule = 0;
        cut_microtubule = 0;
	override_cut_microtubule=0;
        detachements=0;
	shrinkages=0;
        
        for (int i = 0; i < nb_gamma_tub;i++)
        {
            //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            vector<double> v = pointOnContour(ep->getContour());
            Id current=maxId()+1;
            m_maxId++;
            Microtubule e(m_params, ep,this, current, v,1); // Function seems unused but if used assume initating on contour 1
            m_microtubules[current]=e;
            e.setId(current);
            pair<Id,Microtubule*> p;
            p.first=current;
            p.second=&(m_microtubules[current]);
            m_gamma_tub.push_back(p);
        }
    }
    
    //MicrotubulePool::MicrotubulePool(Parametres *params, int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep)
    //{
        //if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep):"<<endl;}
        //int m_influence = 0;
        //m_maxId=1;
        //m_nb_gamma_tub=nb_gamma_tub;
        //m_proba_detachement = proba_detachement;
        //m_proba_initiation = proba_initiation;
        //m_ep=ep;
        //m_params = params;
        //
    //};
    
    
    MicrotubulePool::MicrotubulePool(Parametres *params, ElementPool *ep)
    {
        m_influence = params->getProprieteD("part_influence_normale");
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep):"<<endl;}
        m_maxId=1;
        m_nb_gamma_tub=params->getProprieteI("nb_microtubules_init");
        m_proba_detachement = params->getProprieteD("proba_detachement_par_step_par_microtubule");
        //m_proba_initiation = params->getProprieteD("proba_initialisation_par_step");
	m_proba_shrink = params->getProprieteD("proba_shrink");
	m_proba_crossmicro_cut = params->getProprieteD("proba_crossmicro_cut");
        m_proba_tocut = params->getProprieteD("proba_tocut");
        m_obj_num= params->getProprieteI("num_input_vtk");
        m_ep=ep;
        m_params = params;
        
        destruction_microtubule = 0;
        creation_microtubule = 0;
        cut_microtubule = 0;
	override_cut_microtubule=0;
        detachements=0;
        shrinkages=0;

    }
    
    MicrotubulePool::~MicrotubulePool()
    {       
           if (verbose>=1) {cout<<"destructeur de MicrotubulePool :"<<endl;}
    }
    
    void MicrotubulePool::initiate()
    {
    if (m_params->getProprieteI("Sep_Nucleations")==1)
        {   int Num_in;
            for (int j=1; j<=m_obj_num; j++)
            {
                //extract the correct value for microtubule initialisation on this surface
                if (j==1)
                { 
                 Num_in=m_params->getProprieteI("nb_microtubules_init");
                }
                else
                { 
                 string na_temp_nb_mic="nb_microtubules_init_"+std::to_string(j);
                 Num_in=m_params->getProprieteI(na_temp_nb_mic);
                }
                cout << "Num_in=" << Num_in << endl;
                for (int i=0; i<Num_in; i++)
                {
                    deb4("Separate surfaces: boucle ngamma 1")
                    //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                    vector<double> v = pointNucleateOnContour(m_ep->getContour(),j-1);
                    Id current=maxId()+1;
                    m_maxId++;
                    deb4("Separate surfaces: boucle ngamma 2")
                    Microtubule e(m_params, m_ep,this, current, v, j);
                    m_microtubules[current]=e;
                    e.setId(current);
                    deb4("Separate surfaces: boucle ngamma 3")
                    pair<Id,Microtubule*> p;
                    p.first=current;
                    p.second=&(m_microtubules[current]);
                    deb4("Separate surfaces: boucle ngamma 4")
                    m_gamma_tub.push_back(p);
                    e.getPlus()->setStructure(&m_microtubules[current]);
                }
            }
        }
    else
        {   int cont_num;
            if (m_obj_num==1){cont_num=1;}
            else {cont_num=-1;}
            for (int i = 0; i < m_nb_gamma_tub;i++)
            {   deb4("boucle ngamma 1")
                //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                vector<double> v = pointNucleateOnContour(m_ep->getContour(),0);
                Id current=maxId()+1;
                m_maxId++;
                deb4("boucle ngamma 2")
                Microtubule e(m_params, m_ep,this, current, v,cont_num); 
                m_microtubules[current]=e;
                e.setId(current);
                deb4("boucle ngamma 3")
                pair<Id,Microtubule*> p;
                p.first=current;
                p.second=&(m_microtubules[current]);
                deb4("boucle ngamma 4")
                m_gamma_tub.push_back(p);
                e.getPlus()->setStructure(&m_microtubules[current]);
            }
        }
    }
    
    
    Id MicrotubulePool::giveMicrotubule(ElementPool *ep)
    {
        
        Id current=maxId()+1;
        m_maxId++;
        Microtubule e(m_params, ep,this, current);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }

// "giveMicrotubule" Function used when you have a whole microtubule and want to add it to the microtubule pool. This happens for example after a microtubule has been cut. Then the first microtubule just takes the place of the origional microtubule and the second gets added to the "microtubule pool" using this function. The input *ep is the element pool of elements which makes up the microtubule. 
    Id MicrotubulePool::giveMicrotubule(ElementPool *ep, deque<Element *> body)
    {
        Id current=maxId()+1;
        m_maxId++;
        if (verbose==2) {cout<<"giveMicrotubule :"<<endl;}
        Microtubule e(m_params,ep,this, current, body);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]); //Slightly unclear what the e.getPlus is used for on this line. 
        //cout<<*e.getPlus()<<endl;
        return current;
    }
    
    
// Generates a new microtubule. Similar to above. Used when a new microtubule appears in initiation. Unclear whether this function and the above Microtubulue::giveMicrotubule both needed. 	
     Id MicrotubulePool::giveMicrotubule(ElementPool *ep, vector<double> vec, int cont_num)    
	{
        Id current=maxId()+1;
        m_maxId++;
        if (verbose==2) {cout<<"giveMicrotubule :"<<endl;}
        Microtubule e(m_params,ep,this, current, vec, cont_num);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }
    
       
    
        
    
    Microtubule* MicrotubulePool::getMicrotubule(Id id)
    {
        //ici ajouter des tests de coherence
        return &m_microtubules[id];
    }
    
    unordered_map<Id, Microtubule>* MicrotubulePool::getMicrotubules()
    {
        //ici ajouter des tests de coherence
        return &m_microtubules;
    }
    

// Gets key of every microtubule in the m_microtubules
    list<Id> MicrotubulePool::getListMicrotubules()
    {
        list<Id> lm;
        for (unordered_map<Id,Microtubule>::iterator it=m_microtubules.begin();it!=m_microtubules.end();it++)
        {
            lm.push_back(it->first); // adds key of the microtubule to the list lm
        }
        return lm;
    }
    
    Id MicrotubulePool::maxId()
    {
        return m_maxId;
    }
    
    int MicrotubulePool::getInfluence()
    {
        return m_influence;
    }
    
    
    
    void MicrotubulePool::erase(Id id)
    {
		deb("MicrotubulePool::erase(Id id)");
		destruction_microtubule+=1;
		//cout << "erase microtubule : "<< m_microtubules[id]  << endl;
		m_microtubules.erase(id);		
		deb("MicrotubulePool::erase(Id id) OVER");
	}

    int MicrotubulePool::write(string s_filename)
    {
        ofstream myfile;
        char *filename = (char*)s_filename.c_str();
        myfile.open(filename);
        myfile << *this;
        myfile.close();
        return 0;
    }

    std::vector<int> MicrotubulePool::getMicrotubuleLengths()
    {   std::vector<int> MicroSizes;
	list<Id> lp = getListMicrotubules();
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
        	int Temp1=getMicrotubule(*it)->getSize();
        	MicroSizes.push_back(Temp1);
	}
        return MicroSizes;
    }

// Move the microtubules on one timestep. First coupure "cut" : every microtubule in the list "getlist_tocut" is cut with a probability "m_proba_tocut". The cut produces two microtubules, one of which replaces the origional microtubule the other which is added to the MicrotubulePool as a new microtubule. The second microtubule which contains the free end continues to grow at this end but recedded from the cut. The first microtubule still attached to its base only decays away from the cut. Secondly: detachment. Every microtubule has  a probability m_proba_detachment of detaching from its base. If it does then it starts to decay from its minus end. Thirdly: initiation. the integar part of proba_initiation indicates the number of microtubules which have to be created. These are created and added to the list microtubules. The decimal part of proba_initation details the probability of a single new microtubule being created, which is created after the above number of required microtubules are produced. Finally it sends each microtubule individually to m.evolve() presumably to progress them all forward in time - note this seems to evolve microtubules initialised in this timestep too. 
//Added an additiona option of shrinkage - chance that the microtubules start to shrink based on a probability
    void MicrotubulePool::evolve(ElementPool *ep)
    {
        //version avec trois probas : initiation, separation et coupure
        deb("MicrotubulePool::evolve(ElementPool *ep)");        
        //srand(time(0)+getpid());
        //cut
        list<Element *> l_tocut = m_ep->getlist_tocut();        
        int k=0;
        for (list<Element *>::iterator it=l_tocut.begin();it!=l_tocut.end();it++)
        { 
            //deb3("test...");
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_tocut))
            {
                deb3("ici...");
                //cout << **it <<  endl;
                Structure *s=(*it)->getStructure();
                deb3("... ici"); 
                s->scission(*it);
                //cut_microtubule+=1;
                deb3("... ou lÃ "); 
            }
	//cout << "k = "<< k << endl;
        }

	//cutting microtubules which have crossed over each other
        list<Element *> m_cross_tocut = m_ep->getlist_crossover_tocut();  
        for (list<Element *>::iterator it=m_cross_tocut.begin();it!=m_cross_tocut.end();it++)
        {   double proba = (float)rand()/(float)RAND_MAX;
	    k++;
            if ((proba < m_proba_crossmicro_cut))
            {   //cout << "Probability=" << proba << " Microtubule cut=" << m_proba_crossmicro_cut << endl;  	
                Element * Cross_element=(*it)->getCrossMicro();
                if (Cross_element->isAlive()==1) //check it is still crossing a microtubule
		{ 
                	deb3("ici...");
                	//cout << **it <<  endl;
                	Structure *s=(*it)->getStructure();
                	deb3("... ici"); 
                	s->scission(*it);
			ep->unlist_crossover_tocut(*it);
                        (*it)->setCrossMicro(NULL);
                	cut_microtubule+=1;

		}
		else   //if it is not crossing a microtubule just delete it from the list. Keep and eye on how many cutting this is stopping.
		{	//ep->ErrorChecking(1);
			//ep->N_Check(*it);	
			//ep->ErrorChecking(0)
			// delete from list if decided nolonger a crossover
			ep->unlist_crossover_tocut(*it);
                        (*it)->setCrossMicro(NULL);
			override_cut_microtubule+=1;
		}
		
            }
        }
        //cout << "Total micro considered for cutting=" << k; 
        //cout <<" Overide=" <<override_cut_microtubule;
        //cout << " Cut=" << cut_microtubule << endl;

        
        //detachement - microtubule starts decaying from minus end
        list<Id> lp = getListMicrotubules();
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   

            Microtubule *m=getMicrotubule(*it);
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_detachement))
                {
                    deb3("testDetach...");
                    if (verbose==3){cout<<*m<<endl;}
                    //voir si je rajoute une condition sur l'etat du microtubule
                    m->setPropriete("gr_st_moins", -1);
                    detachements+=1;
                }
        }

	// microtubules have a probability of decaying from the positive end suddenly
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   

            Microtubule *m=getMicrotubule(*it);
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_shrink))
                {
                    deb3("testshrink...");
                    if (verbose==3){cout<<*m<<endl;}
                    m->setPropriete("gr_st_plus", -1);
                    shrinkages+=1;
                }
        }
       
        //initiation
//         double pos = (float)rand()/(float)RAND_MAX;

    if (m_params->getProprieteI("Sep_Nucleations")==1)
        {   double proba_initiation;
            for (int j=1; j<=m_obj_num; j++)
            {
                //extract the correct value for microtubule initialisation on this surface
                if (j==1)
                { 
                 proba_initiation=m_params->getProprieteD("proba_initialisation_par_step");
                }
                else
                { 
                 string na_temp_nb_mic="proba_initialisation_par_step_"+std::to_string(j);
                 proba_initiation=m_params->getProprieteD(na_temp_nb_mic);
                }
                int cont_num=j;
                //cout<< "Initiating on multiple surfaces begin" << " Cont_num=" << cont_num <<endl;
                //InitiateMidway(ep, proba_initiation, cont_num);
                double proba = (float)rand()/(float)RAND_MAX;
                int p_proba_entier = proba_initiation;
                double p_proba_decimal = proba_initiation - p_proba_entier;
                //cout << "test de la valeur entiÃ¨re/dÃ©cimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
                int nb_init=0;
                for (int step=1;step<=p_proba_entier;step++)
                {
                   deb3("testInit...");
                   vector<double> v = pointNucleateOnContour(ep->getContour(),cont_num-1);
                   //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
                   Id i = this->giveMicrotubule(ep, v,cont_num);
                   Microtubule *m = this->getMicrotubule(i);
                   creation_microtubule+=1;
                   if (verbose==3){cout<<*m<<endl;}
                   nb_init++;
               }
               if (proba < p_proba_decimal)
               {   
                  deb3("testInit decimal...");
                  //vector<double> v = pointOnSphere(150, 3000,3000,3000)
                  vector<double> v = pointNucleateOnContour(ep->getContour(),cont_num-1);
                  //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
                  
                  Id i = this->giveMicrotubule(ep, v, cont_num);
                  Microtubule *m = this->getMicrotubule(i);
                  creation_microtubule+=1;
                  if (verbose==3){cout<<*m<<endl;}
                  nb_init++;
                }
               //cout<< "Initiation on multiple surfaces end" <<endl;
            }
        }
    else
        {    
            int cout_num;
            if (m_obj_num==1){cout_num=1;}
            else {cout_num=-1;}
            double proba_initiation = m_params->getProprieteD("proba_initialisation_par_step");
             //int cont_num=-1;
             double proba = (float)rand()/(float)RAND_MAX;
             int p_proba_entier = proba_initiation;
             double p_proba_decimal = proba_initiation - p_proba_entier;
             //cout << "test de la valeur entiÃ¨re/dÃ©cimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
             int nb_init=0;
             for (int step=1;step<=p_proba_entier;step++)
             {
                 deb3("testInit...");
                 //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                 vector<double> v = pointNucleateOnContour(ep->getContour(),0);
                 //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
                 Id i = this->giveMicrotubule(ep, v, cout_num);
                 Microtubule *m = this->getMicrotubule(i);
                 creation_microtubule+=1;
                 if (verbose==3){cout<<*m<<endl;}
                 nb_init++;
             }
             if (proba < p_proba_decimal)
             {   
                 deb3("testInit decimal...");
                 //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                 vector<double> v = pointNucleateOnContour(ep->getContour(),0);
                 //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
                 Id i = this->giveMicrotubule(ep, v, cout_num);
                 Microtubule *m = this->getMicrotubule(i);
                 creation_microtubule+=1;
                 if (verbose==3){cout<<*m<<endl;}
                 nb_init++;
             }

        }
        //cout<<"nb d'initiations "<<nb_init<<endl;
        
        deb2("           evolve microtubulepool 2");
//         int a=0;int b=0;int c=0;int d=0;double tot=0;int lg=0;
        lp = getListMicrotubules();
        deb2("           evolve microtubulepool 2b");
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
                //deb3("testMicr...");
                Microtubule *m=getMicrotubule(*it);
                deb2("                   evolve microtubulepool 3a");
                if (verbose>=1){cout << "                       in evolve microtubule, microtubule : " << *m <<endl;}
                m->evolve();
                if (verbose>=1){cout << "                       evolve returned in the pool loop : " << *m <<endl;}
                deb2("                   evolve microtubulepool 3b");
  
        }
        deb("           evolve microtubulepool 4");

    }

    void MicrotubulePool::InitiateMidway(ElementPool *ep, double proba_initiation, int cont_num)
    {
         vector<double> v;
         double proba = (float)rand()/(float)RAND_MAX;
         int p_proba_entier = proba_initiation;
         double p_proba_decimal = proba_initiation - p_proba_entier;
         //cout << "test de la valeur entiÃ¨re/dÃ©cimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
         int nb_init=0;
         for (int step=1;step<=p_proba_entier;step++)
         {
             deb3("testInit...");
             //vector<double> v = pointOnSphere(150, 3000,3000,3000);
             if (cont_num<0)
             {
             v = pointOnContour(ep->getContour());
             }
             else
             {
             v = pointOnContour(ep->getContour(),cont_num-1);
             }
             //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
             Id i = this->giveMicrotubule(ep, v,cont_num);
             Microtubule *m = this->getMicrotubule(i);
             creation_microtubule+=1;
             if (verbose==3){cout<<*m<<endl;}
             nb_init++;
       }
       if (proba < p_proba_decimal)
            {   
            deb3("testInit decimal...");
            //vector<double> v = pointOnSphere(150, 3000,3000,3000)
            if (cont_num<0)
            {
            vector<double> v = pointOnContour(ep->getContour());
            }
            else
            {
            vector<double> v = pointOnContour(ep->getContour(),cont_num-1);
            //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
            }
            Id i = this->giveMicrotubule(ep, v, cont_num);
            Microtubule *m = this->getMicrotubule(i);
            creation_microtubule+=1;
            if (verbose==3){cout<<*m<<endl;}
            nb_init++;
        }

    }


