#include <vector>
class Contour;


std::vector<double> normalise(std::vector<double> vec);
std::vector<double> normalise(double x, double y, double z);
double norme(std::vector<double> vec);
double angleVec(std::vector<double> v1, std::vector<double> v2);
std::vector<double> pointOnSphere(double size, double x, double y, double z);
std::vector<double> pointOnContour(Contour *c);
std::vector<double> pointOnContour(Contour *c, int i);
std::vector<double> pointNucleateOnContour(Contour *c, int i); //find point on contour which allows nucleation