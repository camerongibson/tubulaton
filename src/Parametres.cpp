// class for reading in parameters from the command line. This class is largely self contained. Beyond the inital construction, the only reason it will probably be called is to read a parameter value out of one of the three lists of properties: m_proprietesD, m_proprietesI and m_proprietesS.

#include "Parametres.hpp"
#include <string>
#include <iostream>
#include <map>
#include <fstream>


#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}
#define deb2(x) if (verbose==2){cout << x << endl;}

using namespace std;

// Reads in parameters from the .txt file given to the executable as an input
Parametres::Parametres(string config)
{
    // Predefining all the entries in the three arrays m_proprietesD, m_proprietesI and m_proprietesS
    m_proprietesD["Kgr_sh_Plus"]=0.0;
    m_proprietesD["Ksh_gr_Plus"]=0.0;
    m_proprietesD["V_gr_Plus"]=0.0;
    m_proprietesD["V_sh_Plus"]=0.0;
    m_proprietesD["V_sh_Moins"]=0.0;
    m_proprietesD["Angle_bundle"]=0.0;
    m_proprietesD["K_nobundle_shrink"]=0.0;
    m_proprietesD["K_nobundle_cross"]=0.0;
    m_proprietesD["K_nobundle_catastrophe"]=0.0;
    m_proprietesD["D_bundle"]=0.0;
    m_proprietesD["Angle_mb_limite"]=0.0;
    m_proprietesD["Angle_mb_trajectoire"]=0.0;
    m_proprietesD["Angle_cut"]=0.0;//angle defining the category that is targeted by katanin
    m_proprietesD["tan_Angle_mb"]=0.0;
    m_proprietesD["epaisseur_corticale"]=0.0;
    m_proprietesD["poids_previous_vect"]=0.0;
    m_proprietesD["d_mb"]=0.0;
    m_proprietesD["part_alea_alea"]=0.0;
    m_proprietesD["part_alea_fixe"]=0.0;
    m_proprietesD["proba_detachement_par_step_par_microtubule"]=0.0;
    m_proprietesD["proba_initialisation_par_step"]=-1.0;
    m_proprietesD["proba_initialisation_area"]=-1.0; // unused unless updated later.
    m_proprietesD["proba_tocut"]=0.0;
    m_proprietesD["proba_rescue"]=0.02;
    m_proprietesD["proba_shrink"]=0.0; //Probability that a microtubule growing end will change to shrinking
    m_proprietesD["proba_crossmicro_cut"]=0.0; //Probability that a microtubule crossing another will be cut
    m_proprietesD["proba_crossmicro_shrink"]=1.0; //Probability that a microtubule will start shrinking when it hits another microtubule at a large angle rather than crossing over. 
    m_proprietesD["cutgroup"]=0; //At crossovers multiple adjacent elements on the microtubule can be marked for cutting so low probabilities for proba_crossmicro_shrink and proba_crossmicro_cut occur. if cutgroup=0 this is allowed. otherwise cut-group should be a positive integer and sets the continuous number of points which would otherwise be marked for cutting/induced catastrophe before another position is marked.
    m_proprietesD["part_influence_influence"]=0.0;
    m_proprietesD["part_influence_normale"]=0.0;
    m_proprietesD["taille_microtubule"]=0.;
    
    m_proprietesI["gr_st_plus"]=0;
    m_proprietesI["gr_st_moins"]=0;
    m_proprietesI["nb_microtubules_init"]=0;
    m_proprietesI["nb_max_steps"]=0;
    m_proprietesI["garbage_steps"]=0;
    m_proprietesI["vtk_steps"]=0;
    m_proprietesI["stop"]=0;
    m_proprietesI["details"]=0;
    m_proprietesI["num_input_vtk"]=1;
    m_proprietesI["Sep_Nucleations"]=0; // sets separate or same nucelation on eahc surface. if =0 same if >0 different.
    m_proprietesI["Output_Contour"]=0; // Default contour data not output
    m_proprietesI["Type_Input"]=0; // Assumes initiation per step provided as a value rather than an area initiatoni. .

    m_proprietesS["nom_output_vtk"]="";
    m_proprietesS["nom_input_vtk"]="";
    m_proprietesS["nom_rapport"]="";
    m_proprietesS["nom_input_vtk_ref"]="";
    m_proprietesS["nom_folder_input_vtk"]="";
    m_proprietesS["nom_folder_output_vtk"]="";
    m_proprietesS["nom_config"]="";
    
    m_proprietesI["decision_rencontre"]=0;
    m_proprietesI["decision_accrochage"]=0;
    m_proprietesI["decision_cut"]=0;
    m_proprietesI["Nucleation_Direction"]=0; //Sets nucleation direction. Defaut =0 so tangent to the surface of =1 for normal to the surface. 
    
    m_proprietesI["cortical"]=0;
    
    m_proprietesI["save_events"]=100;
    
    // Read in values for the arrays from the config file. Output to Load is a boolean value which is 1 if it cannot read the config file
    bool b = Load(config);
    if (!b) {exit(0);} // exits file if no config file read in 
}


// Next three functions set the property called p the value k for the three arrays m_proprietesI, m_proprietesD and m_proprietesS respectively. 
void Parametres::setProprieteI(string p, int k)					
{
    m_proprietesI[p]=k;
}
void Parametres::setProprieteD(string p, double k)					
{
    m_proprietesD[p]=k;
}
void Parametres::setProprieteS(string p,string k)
{
    m_proprietesS[p]=k;
}

// The next three functions et the property calle p from the vectors m_proprietesI, m_proprietesD and m_proprietesS respectively
int Parametres::getProprieteI(string p)							
{
    if (m_proprietesI.count(p)>0){return m_proprietesI[p];}
    else {
        cout << "erreur de getPropriete I "<<p<<endl;
        exit(0);
    }
    
}
double Parametres::getProprieteD(string p)
{
    if (m_proprietesD.count(p)>0){return m_proprietesD[p];}
    else {
        cout << "erreur de getPropriete D "<<p<<endl;
        exit(0);
    }
}
string Parametres::getProprieteS(string p)
{
    if (m_proprietesS.count(p)>0){return m_proprietesS[p];}
    else {
        cout << "erreur de getPropriete S "<<p<<endl;
        exit(0);
    }
}

// Sets all the parameter values to default ones extracted from papers. Appears unused in standard use of the code and should perhaps be made a constructor for this class anyway. If these values are desired it may be better to read them in as there own .txt file. 
void Parametres::initialise_default()
{
    //Kawamura et al 2008
    //allard et al 2010 Mechanisms of Self-Organization of Cortical Microtubules in Plants Revealed by Computational Simulations
    //Hawkins et al 2010
    //Murata et al 2005
    //Dixit et al 2004 Encounters between Dynamic Cortical Microtubules Promote Ordering of the Cortical Array through Angle-Dependent Modifications of Microtubule Behavior
    //Chan et al 2003
    
    m_proprietesD["Kgr_sh_Plus"]=0.01;// /s                              allard et al 2010
    m_proprietesD["Ksh_gr_Plus"]=0.04;// /s                              allard et al 2010
    m_proprietesD["V_gr_Plus"]=0.08;// um/s
    m_proprietesD["V_sh_Plus"]=0.16;// um/s                              Kawamura et al 2008
    m_proprietesD["V_sh_Moins"]=0.09;// um/s                             Kawamura et al 2008
    m_proprietesD["Angle_bundle"]=0.69813170079773179;//                                  Dixit et al 2004
    m_proprietesD["K_nobundle_shrink"]=0.4;// um/s
    m_proprietesD["K_nobundle_cross"]=1-m_proprietesD["K_nobundle_shrink"];// um/s
    m_proprietesD["K_nobundle_catastrophe"]=0.4;//                       Dixit et al 2004
    m_proprietesD["D_bundle"]=49;// nm                                 Chan et al 2003
    m_proprietesD["Angle_mb"]=0.17453292519943295;//( = 10 deg)          personnal estimate
    //m_proprietesD["Angle_mb"]=0.5;//( = 40 deg)          personnal estimate
    m_proprietesD["Angle_cut"]=0.6;
    m_proprietesD["tan_Angle_mb"]=0.83909963117727993;//( = 40 deg)          personnal estimate
    //m_proprietesD["poids_previous_vect"]=0.83909963117727993;//( = 40 deg) concerns the relative weight between the previous element and environment
    m_proprietesD["poids_previous_vect"]=0.5;//( = 40 deg) concerns the relative weight between the previous element and environment
    m_proprietesD["d_mb"]=10;//( = distance to the membrane)          personnal estimate    
    
    m_proprietesI["gr_st_plus"]=1; //state of growth of the plus side
    m_proprietesI["gr_st_moins"]=0; //state of growth of the moins side
    
    m_proprietesD["part_alea_alea"]=1;//dans Microtubule, la ponderation de la part aleatoire
    m_proprietesD["part_alea_fixe"]=40;//dans Microtubule, la ponderation de la part fixe
    
    m_proprietesI["nb_microtubules_init"]=100;
    
    m_proprietesI["nb_max_steps"]=1000;
    
    m_proprietesI["garbage_steps"]=500;
    m_proprietesI["vtk_steps"]=500;
    m_proprietesI["stop"]=0;
    m_proprietesI["details"]=0;
    m_proprietesI["Sep_Nucleations"]=0;
    m_proprietesI["Type_Input"]=0; 

    m_proprietesD["part_influence_influence"]=0;//dans Microtubule, la ponderation de la part liee aux forces
    m_proprietesD["part_influence_normale"]=1;//dans Microtubule, la ponderation de la part liees au mouvement normal
    
    m_proprietesS["nom_output_vtk"]="sortie_standart.vtk";
    m_proprietesS["nom_input_vtk"]="ellipsoid_3_final_convert.vtk";

    m_proprietesI["num_input_vtk"]=1;
    m_proprietesI["Output_Contour"]=0;
    m_proprietesS["nom_input_vtk"]="ellipsoid_3_final_convert.vtk";
    m_proprietesS["nom_input_vtk_ref"]="contourPave.vtk";
    m_proprietesS["nom_rapport"]="rapport.txt";
    m_proprietesS["nom_folder_output_vtk"]="./";
    m_proprietesS["nom_folder_input_vtk"]="./";
    m_proprietesS["nom_config"]="config.ini";
    
    m_proprietesD["proba_detachement_par_step_par_microtubule"]=0.001;
    m_proprietesD["proba_initialisation_par_step"]=0.1;
    m_proprietesD["proba_tocut"]=0.01;

    
    m_proprietesD["taille_microtubule"]=8.;//nm
    
    //a rajouter
    //"vitesse_relative_plus_moins"
    //"proba_shrink_si_rencontre"
    //"proba_stop_si_rencontre"
    //"proba_scission"
    //"taille_microtubule"
    m_proprietesI["decision_rencontre"]=0;
    m_proprietesI["decision_accrochage"]=0;
    m_proprietesI["decision_cut"]=0;//se rÃ©fÃ¨re Ã  si oui ou non on coupe les microtubules, c'est dans to_cut que ce point est testÃ©
    m_proprietesI["cortical"]=0;
    
    m_proprietesI["save_events"]=100;
    
    
}


// Reads in values from the input file (called at the end of the constructor)
bool Parametres::Load(string file)
{
    ifstream inFile(file.c_str());
	// Checks if it can read the file and returns if not	
    if (!inFile.good())
    {
        cout << "Cannot read configuration file " << file << endl;
        return false;
    }
    // Reads through each line of the input .txt file untill it gets to the end of the file. On each line looks for comments and removes them. Looks for the = sign as the break between the parameter name and its value. Then calls remplir to replace the value in the three arrays m_proprietesI, m_proprietesD and m_proprietesS respectively with the desired value. 
    while (inFile.good() && ! inFile.eof())
    {
        string line;
        getline(inFile, line);

        // filter out comments
        if (!line.empty())
        {
            size_t pos = line.find('#');

            if (pos != string::npos)
            {
                line = line.substr(0, pos);
            }
        }

        // split line into key and value
        if (!line.empty())
        {
            size_t pos = line.find('=');

            if (pos != string::npos)
            {
                string key     = Trim(line.substr(0, pos));
                string value   = Trim(line.substr(pos + 1));
		// if num_input_vtk indicates multiple surfaces input extend m_proprietesS to expect the additional parameters. num_input_vtk must be in a line before the surface names. 
                if (key.compare("num_input_vtk")==0)
                {
			if  (stoi(value)>1)
			{ 
                		for (int i=2; i<=stoi(value); i++){
					string na_temp="nom_input_vtk_"+std::to_string(i);
    					m_proprietesS[na_temp]="";
					string na_temp_ref="nom_input_vtk_ref_"+std::to_string(i);
    					m_proprietesS[na_temp_ref]="";
					string na_temp_nb_mic="nb_microtubules_init_"+std::to_string(i);
   					m_proprietesI[na_temp_nb_mic]=-1;
					string na_temp_nb_prob_area="proba_initialisation_area_"+std::to_string(i);
   					m_proprietesD[na_temp_nb_prob_area]=-1.0;
					string na_temp_nb_prob_area_ind="Type_Input_"+std::to_string(i);
   					m_proprietesI[na_temp_nb_prob_area_ind]=0;
					string na_temp_nb_prob="proba_initialisation_par_step_"+std::to_string(i);
    					m_proprietesD[na_temp_nb_prob]=-1.0;
					string na_temp_cont_out="Output_Contour_"+std::to_string(i);
    					m_proprietesI[na_temp_cont_out]=0; //Assume not output unless stated otherwise
					string na_temp_nuc_direc="Nucleation_Direction_"+std::to_string(i);
    					m_proprietesI[na_temp_nuc_direc]=0; //Assume tangent unless stated otherwise                                                 
				}
			}
                }
                if (key.compare("proba_initialisation_area")==0){   m_proprietesI["Type_Input"]=1;}
                for (int i=2; i<=m_proprietesI["num_input_vtk"];i++){
                string key_name="proba_initialisation_area_"+std::to_string(i);
                if (key.compare(key_name)==0) {
                string key_name_Type="Type_Input_"+std::to_string(i);
                m_proprietesI[key_name_Type]=1;}
                }

                if (!key.empty() && !value.empty())
                {
                    remplir(key, value);
                }
            }
        }
    }
    
    //Tests to make sure that coherent values were in the init file. Will produce error messages if not
    int Cou_mb=0;
    for (int i=2; i<=m_proprietesI["num_input_vtk"]; i++){
        string na_temp_nb_mic="nb_microtubules_init_"+std::to_string(i);
        if (m_proprietesI[na_temp_nb_mic]>=0){
              Cou_mb=Cou_mb+1; 
        }
        string na_temp_nb_prob="proba_initialisation_par_step_"+std::to_string(i);
        if (m_proprietesD[na_temp_nb_prob]>=0){
              Cou_mb=Cou_mb+1;
        }
        string na_temp_nb_prob_area="proba_initialisation_area_"+std::to_string(i);
        if (m_proprietesD[na_temp_nb_prob_area]>=0){ 
              Cou_mb=Cou_mb+1;
        }
        if (m_proprietesD[na_temp_nb_prob_area]>0 && m_proprietesD[na_temp_nb_prob]>=0){cout << "Initiation probability of the microtubules must be provided only once EITHER in terms of per area or per step (i.e. proba_initialisation_par_step_ or proba_initialisation_area_) with surface number. Both formats must not be provided as done for surface number " << i <<endl;
        exit(0);}
    }
    
    //Check only area or par_step provided
    if ((m_proprietesD["proba_initialisation_par_step"]>=0 && m_proprietesD["proba_initialisation_area"]>=0) || (m_proprietesD["proba_initialisation_par_step"]<0 && m_proprietesD["proba_initialisation_area"]<0)) {
    cout << "Initiation on the first surface (which can refer to the average over all surfaces) must be provided. One and only one of proba_initialisation_par_step or proba_initialisation_area must be provided in the init file" <<endl;     exit(0);} 
    
    //cout << "Counted input: " << Cou_mb << endl;
    if (Cou_mb>0){
        int Temp=2*(m_proprietesI["num_input_vtk"]-1);
        if (Cou_mb==Temp){
	      m_proprietesI["Sep_Nucleations"]=1;
        }
        else {cout << "nb_microtubules_init and proba_initialisation_par_step must be set on all surfaces individually or only one set given for the combined surfaces" <<endl;
        exit(0);
        }       
    }
    
    
    
    //cout << "Area Input: " <<  m_proprietesI["Type_Input"] << endl;
    //cout << "Area Input 2: " <<  m_proprietesI["Type_Input_2"] << endl;

    return true;
}


// Replaces the property in m_proprietesI, m_proprietesD or m_proprietesS called "key" with the new "value"
void Parametres::remplir(string key, string value)
{
    if (m_proprietesD.count(key)>0)
    {m_proprietesD[key]=atof(value.c_str());
    //cout << "type : "<<" double "<<" key : "<<key<<" value : "<<value <<endl;
    }
    else if (m_proprietesI.count(key)>0)
    {m_proprietesI[key]=atoi(value.c_str());
    //cout << "type : "<<" integer "<<" key : "<<key<<" value : "<<value <<endl;
    }
    else if (m_proprietesS.count(key)>0)
    {m_proprietesS[key]=value.c_str();
    //cout << "type : "<<" string "<<" key : "<<key<<" value : "<<value <<endl;
    }
    else
    {cout << "unknown parameter: " << key <<endl;
    exit(0);
    }
}

// Reduces the string called "str" down to a subset of its length presumably be remove the white spaces around it. 
string Parametres::Trim(const string& str)
{
    size_t first = str.find_first_not_of(" \t");

    if (first != string::npos)
    {
        int last = str.find_last_not_of(" \t");

        return str.substr(first, last - first + 1);
    }
    else
    {
        return "";
    }
}

