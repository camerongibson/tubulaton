//shape of an element
// Class to hold a single vector "m_direction" which gives the direction that an element (maybe microtubule base) ponts in. 
#include <vector>
#include "Shape.hpp"

using namespace std;

//Empty constructor
Shape::Shape(){}
//Constuctor to 
Shape::Shape(double x, double y, double z)
{
    std::vector<double>direction;
    direction.push_back(x);
    direction.push_back(y);
    direction.push_back(z);
    m_direction=direction;
}

std::vector<double> Shape::getDirection() const
{
    return m_direction;
}
        
void Shape::setDirection(std::vector<double> direction)
{
    m_direction=direction;    
}        




