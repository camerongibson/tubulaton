#ifndef __MICROTUBULE_H_INCLUDED__
#define __MICROTUBULE_H_INCLUDED__
#include <deque>
#include "Structure.hpp"
#include "Element.hpp"


class ElementPool;
class Parametres;
class MicrotubulePool;

class Microtubule:public Structure
{
friend std::ostream& operator<<(std::ostream& os, Microtubule& m);
public:
    Microtubule();
    Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r);
    Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r, std::deque<Element *> body);
    Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r, std::vector<double> vec, int cont_num);
    ~Microtubule();    
    
    void std_properties();
    
    virtual std::ostream& Print( std::ostream &os) const;
    
    //general function to evolve the microtubule
    void evolve();
    void evolve(int i);
    int length() const;
    double distance(std::vector<double> pos_point, std::vector<double> pos_ref, std::vector<double> vecteur_ref);
    Element * getPosition2ElementId(int position);
    
    void setPropriete(std::string p, double k);
    void setPropriete(std::string p, int k);
	double getProprieteD(std::string p);
	double getProprieteI(std::string p);
    
    void setAge(int age);
    int getAge() const;
    int getSize() const;
    
    void tag_tocut(Element *e);
    void grow_plus();
        void grow_plus_contact_voisin(Element * previous, std::pair<double, Element *> proche, std::pair<double, Element *> proche_paroi_imp);
        void grow_plus_contact_mb(Element * previous, std::pair<double, Element *> paroi);
        void grow_plus_free(Element * previous);
    int shrink_plus();
    void shrink_moins();
    void scission(Element *element);
    std::deque<Element *> getBody() const;  
    
    //fonctions utiles surtout pour le debug - useful functions especially for debugging
    Element * getPlus();
    ElementPool* getElementPool();
        
private:

 
    void setBody(std::deque<Element *> body) ;
    std::deque<Element *> m_body;
    ElementPool *m_elementPool;
    MicrotubulePool *m_microtubulePool;
    Parametres *m_params;
    std::unordered_map<std::string, double> m_proprietesD;
    std::unordered_map<std::string, int> m_proprietesI;
    int m_age;

    
};
#endif
