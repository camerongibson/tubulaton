//Utility
#include <vector>
#include "utility.hpp"
#include "Contour.hpp"
#include "math.h"
#include <fstream>


using namespace std;

#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}


vector<double> normalise(vector<double> vec)
{
	//fonction qui redonne une taille de 1 au vecteur
	//ici verifier la taille de vector
	vector<double> vec2;
	double x = vec[0];
	double y = vec[1];
	double z = vec[2];
	double pond=sqrt(pow(x,2) + pow(y,2) + pow(z,2) );
	vec2.push_back( x/pond );
	vec2.push_back( y/pond );
	vec2.push_back( z/pond );
	return vec2;
}

vector<double> normalise(double x, double y, double z)
{
	//fonction qui redonne une taille de 1 au vecteur
	//ici verifier la taille de vector
	vector<double> vec2;
	double pond=sqrt(pow(x,2) + pow(y,2) + pow(z,2) );
	vec2.push_back( x/pond );
	vec2.push_back( y/pond );
	vec2.push_back( z/pond );
	return vec2;
}


double norme(vector<double> vec)
{
	//fonction qui donne la taille du vecteur
	//ici verifier la taille de vector
	vector<double> vec2;
	double x = vec[0];
	double y = vec[1];
	double z = vec[2];
	double pond=sqrt(pow(x,2) + pow(y,2) + pow(z,2) );
	return pond;
}

// Angle between the two input vectors using the trignometric rule. 
double angleVec(vector<double> v1, vector<double> v2)
{
    //cos a = (XaXb+YaYb+ZaZb) / sqrt((XaÂ²+YaÂ²+ZaÂ²)(XbÂ²+YbÂ²+ZbÂ² ))    
    double Xa = v1[0];
    double Ya = v1[1];
    double Za = v1[2];
    double Xb = v2[0];
    double Yb = v2[1];
    double Zb = v2[2];
    double a = acos( ( Xa*Xb + Ya*Yb + Za*Zb ) / sqrt( ( Xa*Xa + Ya*Ya + Za*Za )*( Xb*Xb + Yb*Yb + Zb*Zb ) ) );
    return a;
    
}

std::vector<double> pointOnSphere(double size, double x, double y, double z)
{
    //srand(time(0)+getpid());
    double x1= -1+2*(float)rand()/(float)RAND_MAX;
    double y1= -1+2*(float)rand()/(float)RAND_MAX;
    double z1= -1+2*(float)rand()/(float)RAND_MAX;
    std::vector<double> vec;
    vec.push_back(x1);
    vec.push_back(y1);
    vec.push_back(z1);
    vec=normalise(vec);
    vec.push_back(vec[0]);
    vec.push_back(vec[1]);
    vec.push_back(vec[2]);
    vec[0]=vec[0] * size + x;
    vec[1]=vec[1] * size + y;
    vec[2]=vec[2] * size + z;   
    return vec;
}


std::vector<double> pointOnContour(Contour *c)
{
    return c->getElementRandom();
}

std::vector<double> pointOnContour(Contour *c, int i)
{
    return c->getElementRandom(i);
}

std::vector<double> pointNucleateOnContour(Contour *c, int i)
{
    return c->getElementRandomNucleation(i);
}

