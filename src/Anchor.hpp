//anchor of an element (in space)
#ifndef __ANCHOR_H_INCLUDED__
#define __ANCHOR_H_INCLUDED__
#include <vector>
class Anchor
{
    public:
        Anchor();
        Anchor(double x, double y, double z);
        double* getX() ;
        double* getY() ;
        double* getZ() ;
        void setPosition(std::vector<double> position);
    
    private:
    //par defaut au moins un vecteur position synthetique
        std::vector<double> m_position;


};
#endif
