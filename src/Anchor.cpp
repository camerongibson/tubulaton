//anchor of an element (in space)
// This records the position in space of the base of the microtubule (and possibly every element). 
#include <vector>
#include "Anchor.hpp"


Anchor::Anchor(){}
Anchor::Anchor(double x, double y, double z)
{
   std::vector<double>position;
   position.push_back(x);
   position.push_back(y);
   position.push_back(z);
   m_position=position;
}

double* Anchor::getX()
{
    return &m_position[0];
}

double* Anchor::getY()
{
    return &m_position[1];
}

double* Anchor::getZ()
{
    return &m_position[2];
}

void Anchor::setPosition(std::vector<double> position)
{
    m_position=position;
}
