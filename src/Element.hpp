//classe Element
//non si element.h deja importe ailleurs
#ifndef __ELEMENT_H_INCLUDED__
#define __ELEMENT_H_INCLUDED__
#include <iostream>
#include <vector>
#include <unordered_map>
#include "Structure.hpp"
#include "Shape.hpp"
#include "Anchor.hpp"
#include "Parametres.hpp"
#include <string>


//definition necessaire pour le pointeur
class ElementPool;
class Space;
class Element;
class Structure;
typedef int Id;

class Element
{
public:
    friend std::ostream& operator<<(std::ostream& os, Element p);
    Element();
    Element(Parametres *params, ElementPool* ep, Id id);

    ~Element();

    Shape getShape() const;
    void setShape(Shape shape);

    Anchor* getAnchor() ;
    void setAnchor(Anchor anchor);

    Id getId() const;
    void setId(Id id);
    
    Id getId_vtk() const;
    void setId_vtk(Id id);
    
    Id getAge() const;
    void setAge(Id age);
    
    
    
    void setPropriete(std::string p, double k);
    void setPropriete(std::string p, std::vector<double> k);
	double getPropriete(std::string p);
    std::vector<double> getPropriete_v(std::string p);
    void setDead();
    int isAlive();

    
    void setStructure(Structure* id);
    Structure* getStructure();
    int getStructureType() const;
    int getStructureId() const;
    int isMember() const;
    Element* getPrevious();
    void setPrevious(Element* previous);
    

/*
 * deux comportements de base :
 * - decay : ne disparait pas mais un parametre decroit (non utilise pour le moment)
 * - increase : accroit un parametre (non utilise pour le moment)
 */

    void decay();
    void increase();    
    
    Element *m_cross_micro; // If this element is marked for cutting as crossing another microtubule this lists the nearest neighbour element that it is crossing.
    Element* getCrossMicro();
    int m_cross_num;// Used to check how many successive elements have been added to the cut_list for cutting at a cross-over. 
    void setCrossNum(int i); //Set m_cross_num
    int getCrossNum(); //Get m_cross_num
    void setCrossMicro(Element* previous);

private:
    //on peut imaginer y ajouter des trucs genre "forme"    
    Shape m_shape;
    Anchor m_anchor;
    //l'id - ID
    Id m_id;
    //l'id vtk - VTK ID
    Id m_id_vtk;
    //age
    int m_age;
    //present dans l'espace - present in space (probably number of time steps it has been present - not sure if only updated when something happens to the elements in the microtubule or at every time step)
    int m_alive;
    //le pool createur
    ElementPool *m_ep; // elements in the pool 
    Parametres *m_params; // parameters read into from input file
    //la structure container
    Structure* m_structure; 
    int m_structure_id;
    int m_structure_type;
    Element *m_previous;
    //un dictionnaire de caractÃ©ristiques - a dictionary of features
    std::unordered_map<std::string, double> m_proprietes;
    std::unordered_map<std::string, std::vector<double> > m_proprietes_v;
    
    
};
#endif

