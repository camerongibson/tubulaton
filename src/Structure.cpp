//Structure
#include "Structure.hpp"


#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

using namespace std;

class Element;

Structure::Structure()
{
    //cout<<"on instancie d'abord Structure"<<endl;
    m_type=0;
}

Structure::~Structure()
{
    if (verbose){cout<<"on supprime une Structure :"<< m_type<<" "<< m_id <<endl;}
}


std::ostream& Structure::Print( std::ostream &os) const
{
    os << "Structure "<<endl;
    os << "structure de base"<<endl;
    return os;
}

Id Structure::getId() const
{
    return m_id;
}

int Structure::getType() const
{
    return m_type;
}

void Structure::setId(Id id)
{
    m_id = id;
}

void Structure::scission(Element *element)
{
    deb("on est dans la scission de la mere, Structure, ce qui n'est pas trop le but...");
}
