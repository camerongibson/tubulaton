//classe Contour

#include "Contour.hpp"
#include <iostream>

//#include "vtkPolyDataReader.h"
//#include "vtkTransformPolyDataFilter.h"
//#include "vtkTransform.h"
//#include "vtkPolyData.h"
//#include "vtkSmartPointer.h"
//#include "vtkPolyDataWriter.h"
#include "vtkDoubleArray.h"
#include "vtkFloatArray.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkMassProperties.h"
#include "vtkPolyDataNormals.h"

#include <map>
#include <string>
#include <vector>
#include <sstream>
#include "ElementPool.hpp"
#include "Element.hpp"
#include "Anchor.hpp"
#include "Shape.hpp"
#include "utility.hpp"


#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

using namespace std;

std::ostream& Contour::Print( std::ostream &os) const
{
    os << "Contour "<<this->getId()<<"/"<<this->getType()<<"/"<<this<<endl;
    
return os;
}



ostream& operator<<(ostream& os, Contour& m)
{
    m.Print(os);
    return os;
}

Contour::Contour(Parametres *params, ElementPool *ep, Id r):Structure()
{
        m_id=r;
        m_type=2;
        m_elementPool=ep;
        m_params=params;
        ep->setContour(this);
}



// Reads in information from two files. One is the external geometry of the cell which the microtubules cannot leave, and the other appears to be relate the initial positions of the microtubules
void Contour::open()
{   
   //Find the bounding box of all surfaces
   double bounds[6]; //[xmin,xmax,ymin,ymax,zmin,zmax] Give bounding area around all cells
   string complete_name_temp;
   float TotalArea=0;
   for (int obj = 0; obj< m_params->getProprieteI("num_input_vtk"); obj++){ //repeat for all surfaces
 	    //File name of the obj surface
      string s_filename = m_params->getProprieteS("nom_input_vtk");
	  	string r_ref = m_params->getProprieteS("nom_folder_input_vtk");    
	    if (obj>0.5) {
	  	    string nam="nom_input_vtk_"+std::to_string(obj+1);
   	      s_filename = m_params->getProprieteS(nam); 
      }  
	    string complete_name_temp=r_ref+s_filename;
  	  char *filename_temp = (char*)complete_name_temp.c_str();
       
      // Read in data 
	    vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();	
      reader->SetFileName(filename_temp);
      reader->Update();
      vtkSmartPointer<vtkPolyData> polyData = reader->GetOutput();
      
      //Get bounds of this surface and compare to see if larger than for other surfaces
      double bounds_temp[6];
      polyData->GetBounds(bounds_temp);
      if (obj<0.5){
          for (int i = 0; i<6; i++){
              bounds[i]=bounds_temp[i];
              }
      }
      else {
        for (int i = 0; i<3; i++){
            bounds[2*i]=std::min(bounds[2*i],bounds_temp[2*i]);
            bounds[2*i+1]=std::max(bounds[2*i+1],bounds_temp[2*i+1]);
        }
      }
   }
   m_elementPool->spaceContourRegisterLimits(bounds); 
   std::cout << "xmin: " << bounds[0] << " "
            << "xmax: " << bounds[1] << std::endl
            << "ymin: " << bounds[2] << " "
            << "ymax: " << bounds[3] << std::endl
            << "zmin: " << bounds[4] << " "
            << "zmax: " << bounds[5] << std::endl;
            
    //Read in data from the surfaces 
    std::deque<Element *> vec;
    std::deque<Element *> vec_nuc;
    for (int obj = 0; obj< m_params->getProprieteI("num_input_vtk"); obj++){ //repeat for all surfaces
          vec.clear();
           if (m_params->getProprieteI("Sep_Nucleations")==1)
              {vec_nuc.clear();}
 	    string s_filename = m_params->getProprieteS("nom_input_vtk");
  	  string s_ref = m_params->getProprieteS("nom_input_vtk_ref");
  	  string r_ref = m_params->getProprieteS("nom_folder_input_vtk");
      string nam_proba="proba_initialisation_par_step";
      double prob_init=m_params->getProprieteD("proba_initialisation_par_step");
      double InitType=m_params->getProprieteI("Type_Input");
      string Area_Nam="proba_initialisation_area";
 	  // if multiple surfaces then obj>0.5. Work out correct file name of the surface and read it in 
	  if (obj>0.5) {
	  	   string nam="nom_input_vtk_"+std::to_string(obj+1);
      	 //cout << "nam=" << nam << endl;
   	  	 s_filename = m_params->getProprieteS(nam);
       	 nam="nom_input_vtk_ref_"+std::to_string(obj+1);
       	 //cout << "nam=" << nam << endl;
  	  	 s_ref = m_params->getProprieteS(nam);
  	  	 r_ref = m_params->getProprieteS("nom_folder_input_vtk"); 
         nam_proba="proba_initialisation_par_step_"+std::to_string(obj+1);
         prob_init=m_params->getProprieteD(nam_proba);
         string InitType_nam="Type_Input_"+std::to_string(obj+1);
         InitType=m_params->getProprieteI(InitType_nam);
         Area_Nam="proba_initialisation_area_"+std::to_string(obj+1);
	  }
	  string complete_name=r_ref+s_filename;
	  string complete_ref_name=r_ref+s_ref;
  	char *filename = (char*)complete_name.c_str();
		vtkSmartPointer<vtkPolyDataReader> contour = vtkSmartPointer<vtkPolyDataReader>::New();	
		contour->SetFileName(filename);
		contour->Update();
  	  
  	  char *refname = (char*)complete_ref_name.c_str();
		vtkSmartPointer<vtkPolyDataReader> contour_ref = vtkSmartPointer<vtkPolyDataReader>::New();	
		contour_ref->SetFileName(refname);
		contour_ref->Update();
		
  	  vtkSmartPointer<vtkMassProperties> properties =	vtkSmartPointer<vtkMassProperties>::New();
//     properties->SetInput(contour->GetOutput());
  	  properties->SetInputConnection(contour->GetOutputPort());
  	  cout<<"surface "<<properties->GetSurfaceArea()<<endl;
	
  	  vtkSmartPointer<vtkMassProperties> properties_ref =	vtkSmartPointer<vtkMassProperties>::New();
//     properties_ref->SetInput(contour_ref->GetOutput());
  	  properties_ref->SetInputConnection(contour_ref->GetOutputPort());
  	  cout<<"surface_ref "<<properties_ref->GetSurfaceArea()<<endl;
	
//////////////////////////////////////////////////////////////////////////
// The following few lines multiply the probability of initialisation by the ratio of surface areas to reference surface area to allow nucleation rate normalisation.
      if (InitType==0){
      double result=prob_init*properties->GetSurfaceArea()/properties_ref->GetSurfaceArea();
      //cout << prob_init << "  " << properties->GetSurfaceArea() << "  " << properties_ref->GetSurfaceArea() <<endl;
  	  m_params->setProprieteD(nam_proba,result); 
      //cout << prob_init << "  " << properties->GetSurfaceArea() << "  " << properties_ref->GetSurfaceArea() << m_params->getProprieteD("proba_initialisation_par_step")  <<endl;
      }
      else if (InitType=1){
      //cout << prob_init << "  " << properties->GetSurfaceArea() << "  " << properties_ref->GetSurfaceArea() << m_params->getProprieteD("proba_initialisation_par_step")  <<endl;
      float result= m_params->getProprieteD(Area_Nam)*properties->GetSurfaceArea()*8*8/1000/1000/5; //init_per_area * surace area * (8/1000)**2 to move to micromemters from computaitonal units and /5 as about 5 times steps per secont (5-10 depending on growth speed but assumes at the lower end). 
      //cout << "Probability of initialisation: " << result << "   " << m_params->getProprieteD(Area_Nam) << "   "<< 8*properties->GetSurfaceArea()/1000  << endl;
      m_params->setProprieteD(nam_proba,result); 
      prob_init=m_params->getProprieteD(nam_proba);
      //cout << prob_init << "  " << properties->GetSurfaceArea() << "  " << properties_ref->GetSurfaceArea() << m_params->getProprieteD("proba_initialisation_par_step")  <<endl;
      }
      else {cout << "Should have area or per step initialisation not both. Check Parametres.cpp for errors. " <<endl;
        exit(0);
      }
////////////////////////////////////////////////////////////////
		vtkSmartPointer<vtkTransform> translation =	vtkSmartPointer<vtkTransform>::New();
		//translation->RotateX(90);
		//translation->Translate(600.0, 600.0, 600.0);
		//translation->Scale(1200.0, 1200.0, 1200.0);
		//translation->Scale(1./4., 1./4., 1./4.);
	
	
		vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
//	 	transformFilter->SetInput(contour->GetOutput());
  	  transformFilter->SetInputConnection(contour->GetOutputPort());
		transformFilter->SetTransform(translation);
		transformFilter->Update();
		
		//ici j'emploierai des anchors et des Shape
		vtkSmartPointer<vtkPolyData> contourData = transformFilter->GetOutput();
		vtkIdType numPoints = contourData->GetNumberOfPoints();
		//cout << "nb of points : "<<numPoints<<endl;
  	  vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
  	  normalGenerator->SetInputData(contourData);
  	  normalGenerator->ComputePointNormalsOn();
  	  normalGenerator->ComputeCellNormalsOff();
  	  normalGenerator->Update();

	// vtkFloatArray* normalDataFloat = vtkFloatArray::SafeDownCast(normalGenerator->GetOutput()->GetPointData()->GetArray("Normals"));
  	  vtkFloatArray* normalDataFloat = vtkFloatArray::SafeDownCast(contour->GetOutput()->GetPointData()->GetArray("Normals"));

		//vtkDoubleArray* normalDataDouble = vtkDoubleArray::SafeDownCast(polydata->GetPointData()->GetArray("Normals"));
		//if(normalDataFloat)
  	  //{
  	  int nc = normalDataFloat->GetNumberOfTuples();
  	  std::cout << "There are " << nc
  	          << " components in normalDataFloat" << std::endl;
  	  // return true;
  	  
  	  if (!normalDataFloat)
  	  {
//    	cout << "In coutour.cpp, computing normal"<<endl;
//    	vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
//    	normalGenerator->SetInputData(contourData);
//    	normalGenerator->ComputePointNormalsOn();
//    	normalGenerator->ComputeCellNormalsOff();
//    	normalGenerator->ConsistencyOn();
 //   	normalGenerator->FlipNormalsOn();
//    	normalGenerator->Update();
//    	normalDataFloat = vtkFloatArray::SafeDownCast(normalGenerator->GetOutput()->GetPointData()->GetArray("Normals"));
//    	cout << "In contour.cpp, print normal data: " << normalDataFloat <<endl;
		cout << "In contour.cpp, no normal data" <<endl;
  	  }

  	  vtkIntArray* NucleationPoints = vtkIntArray::SafeDownCast(contour->GetOutput()->GetPointData()->GetArray("Nucleation"));	
  	     
      if (!NucleationPoints) 
      {
      cout <<"Nucleation equally on all points"<<endl;
      cout << "Contour number: " << obj <<endl;
      }
      else {cout<<"Nucleation has spatial dependence"<<endl;
          cout << "Contour number: " << obj <<endl;}
      
          
  	  for (int i = 0; i<numPoints; i++)
  	  {
			//cout << "point " << i << " : " << *(contourData->GetPoint(i))<< " / " << *(contourData->GetPoint(i)+1)<< " / " << *(contourData->GetPoint(i)+2) <<endl;

			Element* current = m_elementPool->giveElement();
  	                current->setId_vtk(i);
			vector<double> pos;
			double* posvtk = contourData->GetPoint(i);
			pos.push_back(*posvtk);
			pos.push_back(*(posvtk+1));
			pos.push_back(*(posvtk+2));

  	      for(auto const& component: pos) {
  	          if(component < 0){
  	              std::cerr << "At least one of your vertices has a negative component" << std::endl <<
  	              "expect a segfault!" <<std::endl;
  	          }
  	      }

  	      vector<double> dir;
			double* dirvtk = normalDataFloat->GetTuple(i);
			dir.push_back(*dirvtk);
			dir.push_back(*(dirvtk+1));
			dir.push_back(*(dirvtk+2));
  	      dir=normalise(dir);
			//deb("tata 1");
			Anchor a;
			Shape s;
			a.setPosition(pos);
			s.setDirection(dir);
  	      if (current)
  	      {
                  
        	        int nb = contour->GetOutput()->GetPointData()->GetNumberOfArrays();
        	        for (int n = 0; n<nb;n++)
        	        {
        	            //cout << "array : " << n << " nom : " << contour->GetOutput()->GetPointData()->GetArrayName(n) << endl;
        	            string nom = contour->GetOutput()->GetPointData()->GetArrayName(n);
        	            char *array = (char*)contour->GetOutput()->GetPointData()->GetArrayName(n);
        	            int nbc=contour->GetOutput()->GetPointData()->GetArray(array)->GetNumberOfComponents();
        	            //cout << "array : " << n << " nb : " << nbc << endl;
        	            if (nbc == 1)
        	            {
        	                double *d = contour->GetOutput()->GetPointData()->GetScalars(array)->GetTuple(i);
        	                //cout << "element : "<< *d<<endl;
        	                current->setPropriete(nom, *d);
        	            }
        	            else if (nbc==3)
        	            {
        	                double *d = contour->GetOutput()->GetPointData()->GetVectors(array)->GetTuple(i);
        	                //cout << "elements : "<< *d << " " << *(d+1) << " " << *(d+2) <<endl;
        	                vector<double> v;
        	                v.push_back(*d);
        	                v.push_back(*(d+1));
        	                v.push_back(*(d+2));
        	                current->setPropriete(nom, v);
        	            }
        	            else
        	            {
        	                //cout << "number of components not understandable (by me) in "<< nom <<endl;
        	                deb("number of components not understandable (by me)");
        	            }
        	        }

        	         
                        current->setPropriete("Forces",0);
                        current->setPropriete("Forces_NN",0);
                        current->setPropriete("Forces_NN_Direction",0);
                        current->setPropriete("Forces_Length_Allx",0);
                        current->setPropriete("Forces_Length_Ally",0);
                        current->setPropriete("Forces_Length_Allz",0);
                        current->setPropriete("Forces_PushingX",0);
                        current->setPropriete("Forces_PushingY",0);
                        current->setPropriete("Forces_PushingZ",0);
                        current->setPropriete("Forces_Pushing_All",0);
                        current->setPropriete("Structure_Number",obj+1);
			            current->setAnchor(a);
        	        current->setShape(s);
        	        current->setStructure(this);
        	        //deb("tata 2");
              	  m_elementPool->spaceContourRegister(current);                                   
        	        m_body.push_back(current);
			vec.push_back(current);
      if (NucleationPoints){
          int NucCha=NucleationPoints->GetValue(i);
          if (NucCha==1){vec_nuc.push_back(current);}}
      else {vec_nuc.push_back(current);}
     	}
	}
	m_surf.push_back(vec);
  if (m_params->getProprieteI("Sep_Nucleations")==1) {m_nucleation.push_back(vec_nuc);}
  if(InitType=1){
      float Temp_propD=m_params->getProprieteD(nam_proba)*vec_nuc.size()/vec.size(); 
      m_params->setProprieteD(nam_proba,Temp_propD);
      cout<<m_params->getProprieteD(nam_proba) <<endl;}
     }

  if (m_params->getProprieteI("Sep_Nucleations")!=1) {m_nucleation.push_back(vec_nuc);}
    deb("fin de Contour::open");

}

vector<double> Contour::getElementRandom()
{
    int taille = m_body.size();
    int choix = (int) ((float)rand() * taille / (float)RAND_MAX);
    //cout << "choix "<<choix<<" / "<<taille <<endl;
    Element *e = m_body.at(choix);
    vector<double> pos=AdaptElement(e,0);
    return pos;
}

vector<double> Contour::getElementRandom(int i)
{
    std::deque<Element *> Temp_body=m_surf[i];
    int taille = Temp_body.size();
    int choix = (int) ((float)rand() * taille / (float)RAND_MAX);
    //cout << "choix "<<choix<<" / "<<taille <<endl;
    Element *e = Temp_body.at(choix);
    //cout << *e<<endl;
    vector<double> pos=AdaptElement(e,i);
    return pos;
}

vector<double> Contour::getElementRandomNucleation(int i)
{
    std::deque<Element *> Temp_body=m_nucleation[i];
    int taille = Temp_body.size();
    int choix = (int) ((float)rand() * taille / (float)RAND_MAX);
    //cout << "choix "<<choix<<" / "<<taille <<endl;
    Element *e = Temp_body.at(choix);
    //cout << *e<<endl;
    vector<double> pos=AdaptElement(e,i);
    return pos;
}

vector<double> Contour::AdaptElement(Element *e, int ContNum)
{    
    vector<double> pos;
    Anchor *a=e->getAnchor();
    pos.push_back(*a->getX() - 10 * e->getShape().getDirection()[0]);
    pos.push_back(*a->getY() - 10 * e->getShape().getDirection()[1]);
    pos.push_back(*a->getZ() - 10 * e->getShape().getDirection()[2]);
    
    vector<double> dir;
    dir.push_back(-1+2*(float)rand()/(float)RAND_MAX);
    dir.push_back(-1+2*(float)rand()/(float)RAND_MAX);
    dir.push_back(-1+2*(float)rand()/(float)RAND_MAX);
    dir=normalise(dir);
    
    string na_temp_nuc_direc;
    if (ContNum==0){na_temp_nuc_direc="Nucleation_Direction";}
    else{na_temp_nuc_direc="Nucleation_Direction_"+std::to_string(ContNum+1);}
    int NucDirec=m_params->getProprieteI(na_temp_nuc_direc);

    if (NucDirec==0){
        //here we make sure the direction is tangent to the the membrane by projecting dir onto the plane with normal *e->getShape().getDirection()
        double scalaire =  dir[0]*e->getShape().getDirection()[0] + dir[1]*e->getShape().getDirection()[1] + dir[2]*e->getShape().getDirection()[2];
        dir[0]=dir[0] - scalaire * e->getShape().getDirection()[0];
        dir[1]=dir[1] - scalaire * e->getShape().getDirection()[1];
        dir[2]=dir[2] - scalaire * e->getShape().getDirection()[2];}
    else if (NucDirec==1) {
        // Nucleate normal to the surface
        dir[0]= -e->getShape().getDirection()[0];   
        dir[1]= -e->getShape().getDirection()[1];   
        dir[2]= -e->getShape().getDirection()[2]; }
    else {
        cout << "Nucleation_Direction should be 0 or 1 for tangential or normal nucleation. Recieved " << na_temp_nuc_direc<< " = " <<NucDirec <<endl;
        exit(0);
        }  
        
    dir=normalise(dir);
    pos.push_back(dir[0]);
    pos.push_back(dir[1]);
    pos.push_back(dir[2]);
    return pos;
}

//extracts elements where you can nucleate in each surface individually 
std::deque<Element *> Contour::GetContourNucleationElements(int i)     
{
return (m_nucleation[i]);
}

//extracts elements in each surface individually 
std::deque<Element *> Contour::GetContourElements(int i)     
{
return (m_surf[i]);
}

//extracts all contour elements in any contour
std::deque<Element *> Contour::GetAllContourElements()     
{
return (m_body);
}


